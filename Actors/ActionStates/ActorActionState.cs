using Godot;
using System;

public class ActorActionState : Reference
{
	public Actor Actor {get; set;}

	public virtual void Update(float delta)
	{
		
		// We should be able to "run" whilst falling (it feels better)
		CalculateDirection();
		MovePhysicsBody(delta);
	}
	// We have this method available to all states because it can be called during multiple states, e.g. flying, jumping, falling, running
	public void CalculateDirection()
	{
		

		Actor.MoveDir = new Vector2(Actor.TryRunLeft ? -1 : Actor.TryRunRight ? 1 : 0, Actor.TryMoveUp ? -1 : Actor.TryMoveDown ? 1 : 0);

		if (Actor.FacingRight && Actor.MoveDir.x < 0 || !Actor.FacingRight && Actor.MoveDir.x > 0)
		{
			Actor.Flip();
		}
		
		if (Actor.CurrentActionState == Actor.ActionState.Dying)
		{
			// GD.Print("dying so shouldn't move");
			Actor.MoveDir= new Vector2(0,0);
		}
	}

	// the physics body will be moved in most states that are not idle
	public void MovePhysicsBody(float delta)
	{
		Actor.Velocity = new Vector2(Mathf.Lerp(Actor.Velocity.x, Actor.Speed * Actor.MoveDir.x, 0.15f), Actor.YVel + (Actor.MoveDir.y * Actor.Speed));
		// Actor.Velocity = Actor.GetNode<KinematicBody2D>("Body").MoveAndSlideWithSnap(Actor.Velocity, Vector2.Down*32, Vector2.Up);
		// if (Actor.GetNode<RayCast2D>("Body/CollideLeftRay").IsColliding())
		// {
		// 	Actor.Velocity = new Vector2(100, Actor.Velocity.y);
		// 	// return;
		// }
		if (Actor.MoveDir.x == 0)
		{
			// Actor.Velocity = new Vector2(0, Actor.MoveDir.y);
		}

		Actor.Velocity = Actor.GetNode<KinematicBody2D>("Body").MoveAndSlide(Actor.Velocity, new Vector2(0, -1));

		
		Actor.Position = Actor.GetNode<KinematicBody2D>("Body").Position;
		
		// Coyote timer: start whenever we're on the floor
		if (Actor.GetNode<KinematicBody2D>("Body").IsOnFloor()) //!Actor.IsJumping && 
		{
			Actor.GetNode<Timer>("CoyoteTimer").Start();
		}
	}

	public virtual void EndState()
	{
		// QueueFree();
	}
}
