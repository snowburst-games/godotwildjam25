using Godot;
using System;
using System.Collections.Generic;

public class CastingActorActionState : ActorActionState
{
	private ActorSpellManager _spellManager;
	public CastingActorActionState()
	{

	}

	public CastingActorActionState(Actor actor)
	{
		this.Actor = actor;
		_spellManager =  Actor.GetNode<ActorSpellManager>("SpellManager");
		
	}

	private Dictionary<ActorSpellManager.Spell, float> _spellCostPowerMultiplier = new Dictionary<ActorSpellManager.Spell, float>()
	{
		{ActorSpellManager.Spell.Berserk, 0},
		{ActorSpellManager.Spell.Blink, 1},
		{ActorSpellManager.Spell.Fireball, 0},
		{ActorSpellManager.Spell.FireBolt, 0},
		{ActorSpellManager.Spell.Heal, 0},
		{ActorSpellManager.Spell.SpiritArtefact, 0}
	};

	// private int GetSpellLevel(ActorSpellManager.Spell spell)
	// {
	// 	return _spellManager.SpellLevels[spell];
	// }
	private bool _casted = false;
	public override void Update(float delta)
	{
		base.Update(delta);

		if (_casted)
		{
			return;
		}

		// if the cost is adjustable by power, adjust it
		// int levelCostModifier = 2; // higher makes spell levels more powerful at reducing costs
		// float costReductionDivisor = 5f; // higher makes overall impact of power and spelllevel less on the spell cost
		// int minimumCost = 1;
		// float cost = _spellManager.GetSpellCost(_spellManager.CurrentSpell);
		// Math.Max(_spellManager.SpellCosts[_spellManager.CurrentSpell] - (_spellCostPowerMultiplier[_spellManager.CurrentSpell]
		//  * (Actor.ActorStats["Power"] + levelCostModifier*_spellManager.SpellLevels[_spellManager.CurrentSpell]) /costReductionDivisor), minimumCost);

		if (Actor.GetCurrentAnim() != "Die")
			Actor.PlayAnim("Cast");
		 // can we transfer the cost outside so that we can easily display it?
		// Actor.GetNode<ActorSpellManager>("SpellManager").CastSelectedSpell(_spellManager.CurrentSpell);

		if (_spellManager.ArtefactCastSpell == ActorSpellManager.Spell.SpiritArtefact)
		{
			_spellManager.CastSelectedSpell(ActorSpellManager.Spell.SpiritArtefact);
			_spellManager.ArtefactCastSpell = ActorSpellManager.Spell.Empty;
		}
		else if (_spellManager.HotKeyCastSpell != ActorSpellManager.Spell.Empty)
		{
			_spellManager.CastSelectedSpell(_spellManager.HotKeyCastSpell);
			_spellManager.HotKeyCastSpell = ActorSpellManager.Spell.Empty;
		}
		else
		{
			// GD.Print("THAT");
			_spellManager.CastSelectedSpell(_spellManager.CurrentSpell);
		}


		// if (Actor.GetInputManager().CanAffordToCastSpell() && Actor.GetNode<Timer>("SpellCooldownTimer").TimeLeft == 0 && Actor.GetInputManager().CanCastSpirit())
		// {
		// 	Actor.GetNode<ActorSpellManager>("SpellManager").Cast(Actor.ActorStats["Power"], Actor.GetInputManager().Target);
		// 	Actor.GetNode<ActorDamageHandler>("DamageHandler").ModMana(-cost);
		// 	// Actor.ActorStats["Mana"] -= _spellManager.SpellCosts[_spellManager.CurrentSpell];
		// 	GD.Print(Actor.ActorStats["Mana"], " mana remains.");
		// 	Actor.GetNode<Timer>("SpellCooldownTimer").Start();
		// }
		// else
		// {
		// 	// GD.Print("Insufficient mana or time or spirit!");
		// }



		// face wherever we cast
		if ( Actor.GetInputManager().Target.x/2 > Actor.Position.x && !Actor.FacingRight)
		{
			Actor.Flip();
		}
		if ( Actor.GetInputManager().Target.x/2 < Actor.Position.x && Actor.FacingRight)
		{
			Actor.Flip();
		}

		/// ****experimental
		if (Actor.TryJump && Actor.CoyoteOnFloor() && Actor.Gravity > 0)
		{
			if (Actor.ActorStats["Health"] > 0)
			Actor.SetActionState(Actor.ActionState.Jumping);
		}
		//// ****experimental
		
		EndCast();
	}

	public async void EndCast()
	{
		_casted = true;
		await ToSignal (Actor.GetNode<AnimationPlayer>("Body/Anim"), "animation_finished");


		Actor.SetActionState(Actor.ActionState.Idle);
	}

	public override void EndState()
	{
		OnEndState();
		// QueueFree();
	}

	public async void OnEndState()
	{
		await ToSignal(Actor.GetNode<AnimationPlayer>("Body/Anim"), "animation_finished");
		// QueueFree();
	}

}
