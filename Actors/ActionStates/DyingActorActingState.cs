using Godot;
using System;

public class DyingActorActingState : ActorActionState
{
	public DyingActorActingState()
	{

	}

	public DyingActorActingState(Actor actor)
	{
		this.Actor = actor;
		if (Actor.DiedOnce)
		{
			return;
		}
		Actor.PlayAnimDeath();//("Die");
		Die();
	}

	public override void Update(float delta)
	{
		base.Update(delta);
		// if (Actor.GetNode<AnimationPlayer>("Body/Anim").CurrentAnimation != "Die")
		// {Actor.PlayAnim("Die");}
		
	}


	public async void Die()
	{
		// GD.Print("DIE");
		if (Actor.GetNode<IActorBody>("Body") is MonsterBody monster)
		{
			monster.PlaySound(monster.GhostSounds[AudioData.Ghost.Die], AudioData.SoundBus.Voice, 4);
			monster.OnDeath();
		}
		else if (Actor.GetNode<IActorBody>("Body") is PlayerBody player)
		{
			player.PlaySound(player.PlayerSounds[AudioData.Player.Die], AudioData.SoundBus.Voice, 5);
		}

		
		await ToSignal(Actor.GetNode<AnimationPlayer>("Body/Anim"), "animation_finished");

		
		if (Actor.GetNode<KinematicBody2D>("Body") is PlayerBody p) // don't disappear if player
		{
			p.OnDeath();
			// Actor.GetNode<AnimationPlayer>("Body/Anim").Stop();
			// Actor.SetPhysicsProcess(false);
			// Actor.Hide();
			// Actor.GetNode<CollisionShape2D>("Body/Shape").Disabled = true;
			Actor.DiedOnce = true;
			return;
		}
		if (Actor.GetNode<KinematicBody2D>("Body") is MonsterBody m)
		{
			// Actor.DiedOnce = true;
			m.OnDeathPostAnim();
		}
		
		Actor.DisposeActor();
		// Actor.DiedOnce = true;
	}

	public override void EndState()
	{
		// OnEndState();
		// QueueFree();
	}

	// public async void OnEndState()
	// {
	// 	// await ToSignal(Actor.GetNode<AnimationPlayer>("Body/Anim"), "animation_finished");
	// 	// QueueFree();
	// }
}
