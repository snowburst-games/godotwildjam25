using Godot;
using System;

public class FallingActorActionState : ActorActionState
{
	public FallingActorActionState()
	{

	}

	public FallingActorActionState(Actor actor)
	{
		this.Actor = actor;


		Actor.IsJumping = false;

	}

	public override void Update(float delta)
	{
		base.Update(delta);
		// GD.Print("LETS JUMP", Actor.TryJump && Actor.GetNode<Timer>("CoyoteTimer").TimeLeft > 0);


		Actor.SetActionState(
			Actor.TryJump && Actor.CoyoteOnFloor() && (Actor.ActorStats["Health"] > 0)? Actor.ActionState.Jumping :
			Actor.GetNode<KinematicBody2D>("Body").IsOnFloor() ? Actor.ActionState.Idle :
			Actor.TryCast ? Actor.ActionState.Casting :
			Actor.ActionState.Falling
		);

		
		// Actor.PlayAnim("Fall");
	}
}
