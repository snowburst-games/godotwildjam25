using Godot;
using System;

public class FlyingActorActionState : ActorActionState
{
	public FlyingActorActionState()
	{

	}

	public FlyingActorActionState(Actor actor)
	{
		this.Actor = actor;
	}
	public override void Update(float delta)
	{

		base.Update(delta);
		Actor.SetActionState(
			! Actor.TryFly() ? Actor.ActionState.Idle :  
			Actor.ActionState.Flying);
		
		// CalculateDirection();
		// MovePhysicsBody(delta);

		if (Actor.GetCurrentAnim() != "Die")
			Actor.PlayAnim("Fly");
	}
}
