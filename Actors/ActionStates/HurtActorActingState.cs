using Godot;
using System;

public class HurtActorActingState : ActorActionState
{
	public HurtActorActingState()
	{

	}

	public HurtActorActingState(Actor actor)
	{
		this.Actor = actor;
		Actor.GetNode<Timer>("HurtModTimer").Start();
		Actor.GetNode<Sprite>("Body/Sprite").Modulate = new Color(10,10,10,10);

		if (Actor.GetNode<IActorBody>("Body") is MonsterBody monster)
		{
			monster.PlaySound(monster.GetRandomHurtSound(), AudioData.SoundBus.Voice, 2);
		}
		else if (Actor.GetNode<IActorBody>("Body") is PlayerBody player)
		{
			player.PlaySound(player.GetRandomHurtSound(), AudioData.SoundBus.Voice, 4);
		}
		if (Actor.GetCurrentAnim() != "Die")
			Actor.PlayAnim("Hurt");
	}

	public override void Update(float delta)
	{
		base.Update(delta);

		if (Actor.GetNode<Timer>("HurtModTimer").TimeLeft == 0)
		{
			Actor.GetNode<Sprite>("Body/Sprite").Modulate = Actor.OriginalColor;
			Actor.SetActionState(Actor.ActionState.Idle);
		}
		
		

	}
}
