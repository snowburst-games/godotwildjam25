using Godot;
using System;

public class IdleActorActionState : ActorActionState
{
	public IdleActorActionState()
	{

	}

	public IdleActorActionState(Actor actor)
	{
		this.Actor = actor;
		Actor.GetNode<Sprite>("Body/Sprite").Modulate = Actor.OriginalColor;
		if (Actor.GetNode<IActorBody>("Body") is PlayerBody player)
		{
			Actor.GetNode<Sprite>("Body/Sprite").Scale = new Vector2(0.3f,0.3f);
			player.StopSound(player.PlayerSounds[AudioData.Player.Run]);
		}
		if (Actor.GetNode<IActorBody>("Body") is MonsterBody monster)
		{
			Actor.GetNode<Sprite>("Body/Sprite").Scale = new Vector2(0.2f,0.2f);
			
		}
	}

	public override void Update(float delta)
	{
		base.Update(delta);

		Actor.SetActionState(
			Actor.TryCast ? Actor.ActionState.Casting : 
			Actor.TryRun() ? Actor.ActionState.Running :  
			Actor.TryFly() ? Actor.ActionState.Flying : 
			Actor.TryJump && Actor.CoyoteOnFloor() && Actor.Gravity > 0 && (Actor.ActorStats["Health"] > 0) ? Actor.ActionState.Jumping : 
			!Actor.GetNode<KinematicBody2D>("Body").IsOnFloor() && Actor.Gravity > 0 ? Actor.ActionState.Falling : 
			Actor.ActionState.Idle);

		if (Actor.GetCurrentAnim() != "Die")
			Actor.PlayAnim("Idle");
	}
}
