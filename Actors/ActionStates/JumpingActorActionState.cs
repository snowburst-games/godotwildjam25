using Godot;
using System;

public class JumpingActorActionState : ActorActionState
{
	public JumpingActorActionState()
	{

	}

	public JumpingActorActionState(Actor actor)
	{
		this.Actor = actor;
		// Actor.PlayAnim("Jump"); // we may potentially have to put this in update
		Actor.IsJumping = true;
		// Actor.YVel -= Actor.JumpForce;
		if (Actor.GetNode<IActorBody>("Body") is PlayerBody player)
		{
			player.PlaySound(player.PlayerSounds[AudioData.Player.Jump], AudioData.SoundBus.Effects, 4);
		}
	}

	private int _jumpHeight = 0;

	public override void Update(float delta)
	{
		base.Update(delta);

		// if we're holding the jump key and haven't hit our max jump height, adjust our y velocity
		if (Actor.TryJump && _jumpHeight < Actor.MaxJumpForce)
		{
			Actor.YVel -= Actor.JumpForce;
			Actor.YVel = -Math.Max(Actor.YVel, Actor.MaxJumpForce/4f);
			_jumpHeight += Actor.JumpForce;
		}

		// if we stop holding the jump key, limit the height we can jump (stop jumping)
		if (!Actor.TryJump)
		{
			_jumpHeight = Actor.MaxJumpForce;
		}

		// if we hit ceiling, set yVel to 5
		if (Actor.GetNode<KinematicBody2D>("Body").IsOnCeiling())
		{
			Actor.YVel = 5;
		}
		

		Actor.SetActionState(Actor.YVel >= 0  ? Actor.ActionState.Falling : Actor.TryCast ? Actor.ActionState.Casting : 
			(Actor.ActorStats["Health"] > 0) ? Actor.ActionState.Jumping : Actor.ActionState.Dying);
		
		// We should be able to "run" whilst jumping (it feels better)
		// CalculateDirection();
		// MovePhysicsBody(delta);


	}
}
