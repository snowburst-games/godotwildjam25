using Godot;
using System;

public class RunningActorActionState : ActorActionState
{
	public RunningActorActionState()
	{

	}

	public RunningActorActionState(Actor actor)
	{
		this.Actor = actor;
		// if (Actor.GetNode<IActorBody>("Body") is PlayerBody player)
		// {
		// 	player.PlaySound(player.PlayerSounds[AudioData.Player.Run], AudioData.SoundBus.Effects, 1);
		// }
	}
	
	public override void Update(float delta)
	{
		base.Update(delta);

		Actor.SetActionState(
			Actor.TryJump && Actor.CoyoteOnFloor() && Actor.Gravity > 0  && (Actor.ActorStats["Health"] > 0) ? Actor.ActionState.Jumping :
			Actor.TryCast ? Actor.ActionState.Casting :
			! Actor.TryRun() ? Actor.ActionState.Idle :  
			Actor.ActionState.Running);
		
		// CalculateDirection();
		// MovePhysicsBody(delta);

		// play the fly animation if there is no gravity...
		if (Actor.GetCurrentAnim() != "Die")
			Actor.PlayAnim(Actor.Gravity == 0 ? "Fly" : "Run");
		
		if (Actor.GetNode<IActorBody>("Body") is PlayerBody player)
		{
			if (Math.Abs(Actor.Velocity.x)>55)
			{
				// GD.Print(Math.Abs(Actor.Velocity.x));
				player.PlaySound(player.PlayerSounds[AudioData.Player.Run], AudioData.SoundBus.Effects, 1);
			}
		}
	}

	public override void EndState()
	{
		base.EndState();
		// if (Actor.GetNode<IActorBody>("Body") is PlayerBody player)
		// {
		// 	player.StopSound(player.PlayerSounds[AudioData.Player.Run]);
		// }
	}

}
