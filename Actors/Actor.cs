using Godot;
using System;
using System.Collections.Generic;

public class Actor : Node2D
{
	public delegate void TriedToBlinkDelegate(Actor actor, int ID, Vector2 target, float distance, bool throughwalls);
	public event TriedToBlinkDelegate TriedToBlink;
	private ActorActionState _actionState;
	private IActorInputManager _inputManager;
	public delegate void DisposedDelegate(Actor actor);
	public event DisposedDelegate Disposed;
	public bool DiedOnce {get; set;} = false;
	private bool _active = true; // for performance
	
	// public 
	public Dictionary<string, float> ActorStats = new Dictionary<string, float>()
	{
		{"MaxHealth", 30},
		{"Health", 30},
		{"Mana", 60},
		{"MaxMana", 60},
		{"Power", 1}
	};

	// public override void _UnhandledInput(InputEvent ev)
	// {
	// 	base._UnhandledInput(ev);
	// 	if (ev.IsActionPressed("Cast"))
	// 		GD.Print(ev);
	// }

	

	public int ID = -1;
	public int StartingSpeed {get; set;} = 200;
	public int Speed {get; set;} = 200;
	public int JumpForce {get; set;} = 150;
	public int MaxJumpForce {get; set;} = 2000;
	public Color OriginalColor {get; set;} = new Color(1,1,1,1);
	public float YVel {get; set;} = 0;
	public Vector2 MoveDir = new Vector2(0,0);
	public Vector2 Velocity {get; set;}
	public float Gravity {get; set;} = 50;
	public float MaxFallSpeed {get; set;} = 750;
	public bool TryRunLeft {get; set;} = false;
	public bool TryRunRight {get; set;} = false;
	public bool TryMoveDown {get; set;} = false;
	public bool TryMoveUp {get; set;} = false;
	public bool TryJump {get; set;} = false;
	public bool TryCast {get; set;} = false;
	public bool FacingRight {get; set;} = false;
	public ActionState CurrentActionState {get; set;} = ActionState.None;
	public bool IsJumping {get; set;} = false;
	public int ManaRegenMultiplier {get; set;} = 1;

	public new Vector2 Position {
		set{
			base.Position = value;
			if (HasNode("Body"))
			{
				GetNode<KinematicBody2D>("Body").Position = value;
			}
		}
		get{
			return base.Position;
		}
	}

	public Vector2 OriginalPos {get; set;}

	public enum ActionState {
		Idle, Running, Jumping, Falling, Casting, Flying, Hurt, Dying, None
	}

	public enum InputManager { AI, Player}

	public ActorExperienceManager ExperienceManager {get; set;}
	public void SetActive(bool active)
	{
		if (_active == active)
		{
			return;
		}
		_active = active;
		if (_inputManager is MonsterInputManager monster)
		{
			monster.SetAIState(MonsterInputManager.AIState.Idle);
		}
	}
	public override void _Ready()
	{
		if (!HasNode("Body"))
		{
			GD.Print("No kinematicbody child - terminating");
			QueueFree();
			SetPhysicsProcess(false);
		} 
		ExperienceManager = new ActorExperienceManager(this);
		SetActionState(ActionState.Idle);
		// SetInput(InputManager.Player); // temp - should be set by the actorfactory - for testing only
	}

	public IActorInputManager GetInputManager()
	{
		return _inputManager;
	}

	public void DisposeActor()
	{
		GetNode<IActorBody>("Body").UnsubscribeEvents();
		Disposed?.Invoke(this);
		Disposed = null;
		TriedToBlink = null;
	}

	public void SetActionState(ActionState state)
	{
		// if (DiedOnce)
		// {
		// 	return;
		// }
		if (CurrentActionState == state)
		{
			// GD.Print("Already in that state! returning..");
			return;
		}
		// if (CurrentActionState == ActionState.Dying)
		// {
		// 	if (state != ActionState.Idle)
		// 	{
		// 		return;
		// 	}
		// }
		OnEndState();
		switch (state)
		{
			case ActionState.Idle:
				_actionState = new IdleActorActionState(this);
				break;
			case ActionState.Falling:
				_actionState = new FallingActorActionState(this);
				break;
			case ActionState.Jumping:
				_actionState = new JumpingActorActionState(this);
				break;
			case ActionState.Running:
				_actionState = new RunningActorActionState(this);
				break;
			case ActionState.Casting:
				_actionState = new CastingActorActionState(this);
				break;
			case ActionState.Flying:
				_actionState = new FlyingActorActionState(this);
				break;
			case ActionState.Hurt:
				_actionState = new HurtActorActingState(this);
				break;
			case ActionState.Dying:
				_actionState = new DyingActorActingState(this);
				break;
		}
		CurrentActionState = state;
	}

	public void OnEndState()
	{
		// await ToSignal(GetNode<AnimationPlayer>("Body/Anim"), "animation_finished");
		if (_actionState != null)
		{
			_actionState.EndState();
		}

	}

	public void SetInput(InputManager inputManager)
	{
		switch (inputManager)
		{
			case InputManager.Player:
				_inputManager = new PlayerInputManager(this);
				break;
			case InputManager.AI:
				_inputManager = new MonsterInputManager(this);
				break;
		}
	}

	public override void _PhysicsProcess(float delta)
	{
		if (!_active)
		{
			return;
		}

		// if (DiedOnce)
		// {
		// 	return;
		// }

		DoGravity();

		_actionState.Update(delta);
		_inputManager.Update(delta);

		// mana regen
		GetNode<ActorDamageHandler>("DamageHandler").ModMana(ActorStats["MaxMana"]/100 * delta * ManaRegenMultiplier);
	}


	public bool TryRun()
	{
		return TryRunLeft || TryRunRight ? true : false;
	}
	public bool TryFly()
	{
		return TryMoveUp || TryMoveDown ? true : false;
	}

	private bool _playingDeath = false;
	public async void PlayAnimDeath()
	{
		if (GetNode<AnimationPlayer>("Body/Anim").IsPlaying() && GetNode<AnimationPlayer>("Body/Anim").CurrentAnimation == "Die")
		{
			return;
		}
		_playingDeath = true;
		GetNode<AnimationPlayer>("Body/Anim").Play("Die");
		await ToSignal(GetNode<AnimationPlayer>("Body/Anim"), "animation_finished");
		_playingDeath = false;

	}

	public void PlayAnim(string animName)
	{
		if (_playingDeath)
		{
			return;
		}
		// if (animName != "Jump")
		// {

		// if (CurrentActionState == ActionState.Dying)
		// {
		// 	if (animName != "Die")
		// 	{
		// 		return;
		// 	}
		// }

		if (GetNode<AnimationPlayer>("Body/Anim").IsPlaying() && GetNode<AnimationPlayer>("Body/Anim").CurrentAnimation == "Fall")
			{
				return;
			}
		// }
		if (GetNode<AnimationPlayer>("Body/Anim").IsPlaying() && GetNode<AnimationPlayer>("Body/Anim").CurrentAnimation == "Cast")
		{
			return;
		}
		// if (GetNode<AnimationPlayer>("Body/Anim").IsPlaying() && GetNode<AnimationPlayer>("Body/Anim").CurrentAnimation == "Cast")
		// {
		// 	return;
		// }
		if (animName != "Fall" && animName != "Cast")
		{
			if (GetNode<AnimationPlayer>("Body/Anim").IsPlaying() && GetNode<AnimationPlayer>("Body/Anim").CurrentAnimation == "Jump")
			{
				return;
			}
		}
		if (GetNode<AnimationPlayer>("Body/Anim").IsPlaying() && GetNode<AnimationPlayer>("Body/Anim").CurrentAnimation == animName)
		{
			return;
		}
		// if (GetNode<IActorBody>("Body") is PlayerBody)
		// 	GD.Print("playing ", animName);
		GetNode<AnimationPlayer>("Body/Anim").Play(animName);

		if (GetNode<IActorBody>("Body") is PlayerBody player)
		{
			if (animName != "Idle")
			{
				GetNode<CPUParticles2D>("Body/DustParticles").Emitting = false;
			}
			else
			{
				GetNode<CPUParticles2D>("Body/DustParticles").Emitting = true;
			}
			if (animName == "Jump")
			{
				GetNode<CPUParticles2D>("Body/DustParticles").Visible = false;
			}
			else
			{
				GetNode<CPUParticles2D>("Body/DustParticles").Visible = true;
			}
		}
	}

	public bool CoyoteOnFloor()
	{
		return (GetNode<KinematicBody2D>("Body").IsOnFloor() || GetNode<Timer>("CoyoteTimer").TimeLeft > 0);
	}

	public void Flip()
	{
		FacingRight = !FacingRight;
		GetNode<Sprite>("Body/Sprite").FlipH = ! GetNode<Sprite>("Body/Sprite").FlipH;


		GetNode<IActorBody>("Body").Flip();
		// _sprite.FlipH = !_sprite.FlipH;
		// if (FacingRight)
		// {
		// 	_sprite.Position = new Vector2(14, _sprite.Position.y);
		// }
		// else
		// {
		// 	_sprite.Position = new Vector2(0, _sprite.Position.y);
		// }
	}

	private bool _landAtStart = true;

	public string GetCurrentAnim()
	{
		return GetNode<AnimationPlayer>("Body/Anim").CurrentAnimation;
	}

	public void DoGravity()
	{
		// no need to run if there is no gravity
		if (Gravity == 0)
		{
			return;
		}
		// if (HasNode("Body/CollideDownRay"))
		// {
		// 	if (GetNode<RayCast2D>("Body/CollideDownRay").IsColliding())
		// 	{
		// 		return;
		// 	}
		// }
		// if (HasNode("Body/CollideLeftRay"))
		// {
		// 	if (GetNode<RayCast2D>("Body/CollideLeftRay").IsColliding())
		// 	{
		// 		GD.Print("colide left working");
		// 	}
		// }
		// we keep accumulate positive y velocity - so eventually what comes up must go down
		YVel = Math.Min(YVel+Gravity, MaxFallSpeed);
		// If we are on the floor, we need to stop accumulate positive y velocity due to gravity

		if (GetNode<KinematicBody2D>("Body").IsOnFloor() && YVel > 255)
		{
			if (GetNode<IActorBody>("Body") is PlayerBody player)
			{
				player.PlaySound(player.PlayerSounds[AudioData.Player.Land], AudioData.SoundBus.Effects, 3);
				if (GetCurrentAnim() != "Die")
					PlayAnim("Fall");				
			}
		}

		if (YVel < 5)
		{
			// GD.Print("high yvel");
			if (GetCurrentAnim() != "Die")
				PlayAnim("Jump");
		}

		if (GetNode<KinematicBody2D>("Body").IsOnFloor() && YVel >= 5)
		{
			
			YVel = 5;
		}

		// GD.Print(Position);
		// GD.Print("global", GlobalPosition);
		// GD.Print("bnody ", GetNode<KinematicBody2D>("Body").Position);
	}

	public void Blink(Vector2 target, float distance=200f, bool throughwalls = false)
	{
		TriedToBlink?.Invoke(this, this.ID, target, distance, throughwalls);
	}

	public void Blinked()
	{
		GetNode<AnimationPlayer>("Body/BlinkAnim").Play("Blink");
	}
}
