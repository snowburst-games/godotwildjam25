using Godot;
using System;

public class ActorDamageHandler : Node
{
	public Actor Actor {get; set;}
	public int HealPots {get; set;} = 0;
	public int ManaPots {get; set;} = 0;
	// public delegate void ActorStatChangedDelegate(string stat, int magnitude);
	// public event ActorStatChangedDelegate ActorStatChanged;

	public override void _Ready()
	{
		
	}
	public void TakeDamage(float damage)
	{
		if (Actor == null)
		{
			GD.Print("NULL ");
			return;
		}
		
		Actor.ActorStats["Health"] = Mathf.Clamp(Actor.ActorStats["Health"] - damage, 0, Actor.ActorStats["MaxHealth"]);
		if (Actor.ActorStats["Health"] <= 0)
		{
			if (Actor.CurrentActionState != Actor.ActionState.Dying)
			{
				// GD.Print("Actor took ", damage, " damage! Actor health is now DEAD");
				Actor.SetActionState(Actor.ActionState.Dying);
			}
			return;			
		}
		Actor.SetActionState(Actor.ActionState.Hurt);
		// ActorStatChanged?.Invoke("Health", -damage);
		// GD.Print("Actor took ", damage, " damage! Actor health is now: ", Actor.ActorStats["Health"]);

		Actor.ExperienceManager.OnStatsChanged();// StatsUpdated?.Invoke(GetUpdatedStats());

		if (Actor.GetNode<IActorBody>("Body") is MonsterBody monster)
		{
			
				monster.OnTookDamage();
			
		}
	}

	public void ModHealth(float amount)
	{
		Actor.ActorStats["Health"] = Mathf.Clamp(Actor.ActorStats["Health"] + amount, 0, Actor.ActorStats["MaxHealth"]);
	}

	public void ModMana(float mana)
	{
		if (Actor == null)
		{
			GD.Print("NULL ");
			return;
		}
		Actor.ActorStats["Mana"] = Mathf.Clamp(Actor.ActorStats["Mana"] + mana, 0, Actor.ActorStats["MaxMana"]);
		Actor.ExperienceManager.OnStatsChanged();
	}

	public void SetHealth(float newHealth)
	{
		Actor.ActorStats["MaxHealth"] = newHealth;
		Actor.ActorStats["Health"] = newHealth;
		Actor.ExperienceManager.OnStatsChanged();
	}
	public void SetMana(float newMana)
	{
		Actor.ActorStats["MaxMana"] = newMana;
		Actor.ActorStats["Mana"] = newMana;
		Actor.ExperienceManager.OnStatsChanged();
	}
	public void SetPower(float newPower)
	{
		Actor.ActorStats["Power"] = newPower;
		Actor.ActorStats["Power"] = newPower;
		Actor.ExperienceManager.OnStatsChanged();
	}

	public void ModMaxHealth(float amount)
	{
		Actor.ActorStats["MaxHealth"] += amount;
		// Actor.ActorStats["Health"] = Actor.ActorStats["MaxHealth"];
		Actor.ExperienceManager.OnStatsChanged();
	}
	public void ModMaxMana(float amount)
	{
		Actor.ActorStats["MaxMana"] += amount;
		// Actor.ActorStats["Mana"] = Actor.ActorStats["MaxMana"];
		Actor.ExperienceManager.OnStatsChanged();
	}
	public void ModPower(float amount)
	{
		Actor.ActorStats["Power"] += amount;
		// Actor.ActorStats["Mana"] = Actor.ActorStats["MaxMana"];
		Actor.ExperienceManager.OnStatsChanged();
	}

	public void ModHealthPots(int num)
	{
		HealPots += num;
		Actor.ExperienceManager.OnStatsChanged();
	}

	public void ModManaPots(int num)
	{
		ManaPots += num;
		Actor.ExperienceManager.OnStatsChanged();
	}

	public void HealthPotUsed()
	{
		ModHealth(Actor.ActorStats["MaxHealth"]/3f);
		HealPots -= 1;
	}
	public void ManaPotUsed()
	{
		ModMana(Actor.ActorStats["MaxMana"]/3f);
		ManaPots -= 1;
	}

	public void OnItemUsed(PickupBehaviour.Item item)
	{
		if (Actor.GetNode<IActorBody>("Body") is PlayerBody p)
		{
			p.GetNode<AnimationPlayer>("ItemUsedAnim").Play("Start");
			p.PlaySpellSound(p.GetRandomGulpSound());
		}
		switch (item)
		{
			case PickupBehaviour.Item.HealthPot:
				HealthPotUsed();
				break;
			case PickupBehaviour.Item.ManaPot:
				ManaPotUsed();
				break;
		}
	}
}
