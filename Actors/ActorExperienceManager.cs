using Godot;
using System;
using System.Collections.Generic;

public class ActorExperienceManager
{
	public delegate void LevelledUpDelegate();
	public event LevelledUpDelegate LevelledUp;

	public delegate void StatsUpdatedDelegate(Dictionary<StatThatUpdates, float> statsThatUpdate );//Dictionary<string, float> stats, int level, float experience, float experienceNeeded, int skillPoints);
	public event StatsUpdatedDelegate StatsUpdated;
	private float _experience {get; set;} = 0;
	public int Level {get; set;} = 1;
	public int Skillpoints {get; set;} = 0;
	public Actor Actor {get; set;}

	public enum StatThatUpdates { Health, MaxHealth, Mana, MaxMana, Power, Level, Experience, ExperienceNeeded, SkillPoints,
		FireBolt, Fireball, Heal, Blink, Berserk,
		FireBoltCost, FireballCost, HealCost, BlinkCost, BerserkCost,
		FireBoltDamage, FireballDamage, HealHealing, BerserkTime,
		PercentXPToNextLevel,
		HealthPots, ManaPots}
		// FireBoltLevel, FireballLevel, HealLevel, BlinkLevel, BerserkLevel}

	public ActorExperienceManager()
	{

	}
	public ActorExperienceManager(Actor actor)
	{
		Actor = actor;
		OnStatsChanged();
	}

	private float GetExperienceNeeded(int level)
	{
		return (float) Math.Round((5* Math.Pow(level, 3)) / 4);
	}

	public void OnSpellPlusPressed(PnlCharacter.SpellPowerLabel spellPower)
	{
		switch (spellPower)
		{
			case PnlCharacter.SpellPowerLabel.Berserk:
				SpendPoints(Skill.Berserk);
				break;
			case PnlCharacter.SpellPowerLabel.Blink:
				SpendPoints(Skill.Blink);
				break;
			case PnlCharacter.SpellPowerLabel.Fireball:
				SpendPoints(Skill.Fireball);
				break;
			case PnlCharacter.SpellPowerLabel.FireBolt:
				SpendPoints(Skill.FireBolt);
				break;
			case PnlCharacter.SpellPowerLabel.Heal:
				SpendPoints(Skill.Heal);
				break;
		}
	}

	public Dictionary<StatThatUpdates, float> GetUpdatedStats()
	{
		return new Dictionary<StatThatUpdates, float>()
		{
			{StatThatUpdates.Health, (float)Math.Ceiling(Actor.ActorStats["Health"])},
			{StatThatUpdates.MaxHealth, (float)Math.Ceiling(Actor.ActorStats["MaxHealth"])},
			{StatThatUpdates.Mana, (float)Math.Ceiling(Actor.ActorStats["Mana"])},
			{StatThatUpdates.MaxMana, (float)Math.Ceiling(Actor.ActorStats["MaxMana"])},
			{StatThatUpdates.Power, Actor.ActorStats["Power"]},
			{StatThatUpdates.Level, Level},
			{StatThatUpdates.Experience, _experience},
			{StatThatUpdates.ExperienceNeeded, GetExperienceNeeded(Level+1)-_experience},
			{StatThatUpdates.SkillPoints, Skillpoints},
			{StatThatUpdates.Fireball, GetSpellLevel(ActorSpellManager.Spell.Fireball)},
			{StatThatUpdates.FireBolt, GetSpellLevel(ActorSpellManager.Spell.FireBolt)},
			{StatThatUpdates.Berserk, GetSpellLevel(ActorSpellManager.Spell.Berserk)},
			{StatThatUpdates.Blink, GetSpellLevel(ActorSpellManager.Spell.Blink)},
			{StatThatUpdates.Heal, GetSpellLevel(ActorSpellManager.Spell.Heal)},
			{StatThatUpdates.FireballCost, (float)Math.Ceiling(Actor.GetNode<ActorSpellManager>("SpellManager").GetSpellCost(ActorSpellManager.Spell.Fireball))},
			{StatThatUpdates.FireBoltCost, (float)Math.Ceiling(Actor.GetNode<ActorSpellManager>("SpellManager").GetSpellCost(ActorSpellManager.Spell.FireBolt))},
			{StatThatUpdates.BerserkCost, (float)Math.Ceiling(Actor.GetNode<ActorSpellManager>("SpellManager").GetSpellCost(ActorSpellManager.Spell.Berserk))},
			{StatThatUpdates.BlinkCost, (float)Math.Ceiling(Actor.GetNode<ActorSpellManager>("SpellManager").GetSpellCost(ActorSpellManager.Spell.Blink))},
			{StatThatUpdates.HealCost, (float)Math.Ceiling(Actor.GetNode<ActorSpellManager>("SpellManager").GetSpellCost(ActorSpellManager.Spell.Heal))},
			{StatThatUpdates.FireBoltDamage, (float)Math.Ceiling(Actor.GetNode<ActorSpellManager>("SpellManager").GetOverallPower(ActorSpellManager.Spell.FireBolt))},
			{StatThatUpdates.FireballDamage, (float)Math.Ceiling(Actor.GetNode<ActorSpellManager>("SpellManager").GetOverallPower(ActorSpellManager.Spell.Fireball))}	,
			{StatThatUpdates.HealHealing, (float)Math.Ceiling(Actor.GetNode<ActorSpellManager>("SpellManager").GetOverallPower(ActorSpellManager.Spell.Heal))}	,
			{StatThatUpdates.BerserkTime , (float)Math.Ceiling(Actor.GetNode<ActorSpellManager>("SpellManager").GetBerserkDuration())},
			{StatThatUpdates.PercentXPToNextLevel , PercentageToNextLvl()}	,
			{StatThatUpdates.HealthPots, Actor.GetNode<ActorDamageHandler>("DamageHandler").HealPots},
			{StatThatUpdates.ManaPots, Actor.GetNode<ActorDamageHandler>("DamageHandler").ManaPots},
			// {StatThatUpdates.FireBoltLevel, }
		};
	}

	public void SetExperience(float exp)
	{
		_experience = exp;
		OnStatsChanged();
	}

	public void UpdateExperience(float exp)
	{
		_experience += exp;

		// GD.Print("")

		GD.Print("Experience gained: ", exp);
		GD.Print("Total experience: ", _experience);
		GD.Print("Required experience for ", Level+1, ": ", GetExperienceNeeded(Level+1));

		if (_experience >= GetExperienceNeeded(Level+1))
		{
			// GD.Print("Can level");
			LevelUp();
			UpdateExperience(0); // keep running until we can no longer level (levelling automatically based on our xp)
		}
		StatsUpdated?.Invoke(GetUpdatedStats());
	}

	public void OnStatsChanged()
	{
		StatsUpdated?.Invoke(GetUpdatedStats());
	}

	private void LevelUp()
	{
		Level += 1;
		// GD.Print("Health before: ",_ship.MaxHealth);
		// Actor.GetNode<ActorDamageHandler>("DamageHandler").SetHealth(Actor.ActorStats["MaxHealth"] +(1/Actor.ActorStats["MaxHealth"])*1000f);
		Actor.GetNode<ActorDamageHandler>("DamageHandler").SetHealth(Actor.ActorStats["MaxHealth"] + (float) Math.Round(1.5f*Math.Pow(Level,2)/10f+0.5f));
		Actor.GetNode<ActorDamageHandler>("DamageHandler").SetMana(Actor.ActorStats["MaxMana"] + (float) Math.Round(1.5f*Math.Pow(Level,2)/10f+0.5f));
		Actor.GetNode<ActorDamageHandler>("DamageHandler").ModPower((float) Math.Round(1.5f*Math.Pow(Level,2)/10f+0.5f));
		GD.Print("Health after: ",Actor.ActorStats["MaxHealth"]);
		Skillpoints += (int) (Math.Ceiling(Level/4f));
		GD.Print("Skill points: ", Skillpoints);
		Actor.GetNode<PlayerBody>("Body").OnLevelUp();

		// LevelledUp?.Invoke(_ship.UID, Skillpoints, Level);
		// _ship.UpdateUI();


	}

	public enum Skill { Health, Mana, Power, FireBolt, Fireball, Blink, Berserk, Heal};

	// private float _healthLvl = 1;
	// private float _manaLvl = 1;

	public void SpendPoints(Skill s)
	{
		Skillpoints -= 1;
		switch (s)
		{
			// case Skill.Health:
			// 	// Actor.GetNode<ActorDamageHandler>("DamageHandler").ModMaxHealth((float) Math.Round(1.5f*Math.Pow(_healthLvl,2)/6f+0.5f)); // curve
			// 	// Actor.GetNode<ActorDamageHandler>("DamageHandler").ModMaxHealth(Actor.ActorStats["MaxHealth"]/10f); // exponential increase
			// 	Actor.GetNode<ActorDamageHandler>("DamageHandler").ModMaxHealth((1/Actor.ActorStats["MaxHealth"])*1000f); // diminishing returns
			// 	// _healthLvl += 1;
			// 	break;
			// case Skill.Mana:
			// 	// Actor.GetNode<ActorDamageHandler>("DamageHandler").ModMaxMana((float) Math.Round(1.5f*Math.Pow(_manaLvl,2)/6f+0.5f));
			// 	// Actor.GetNode<ActorDamageHandler>("DamageHandler").ModMaxMana(Actor.ActorStats["MaxMana"]/10f);
			// 	Actor.GetNode<ActorDamageHandler>("DamageHandler").ModMaxMana((1/Actor.ActorStats["MaxMana"])*1000f); // diminishing returns
			// 	break;
			// case Skill.Power:
			// 	// _ship.ShipState.Speed += 1;// (float) Math.Round(1.5f*Math.Pow(_level,2)/10f+0.5f);
			// 	Actor.GetNode<ActorDamageHandler>("DamageHandler").ModPower((1/Actor.ActorStats["Power"])*50f); // diminishing returns
			// 	// _ship.ShipState.SetSpeed();
			// 	break;
			case Skill.FireBolt:
				// Actor.GetNode<ActorSpellManager>("SpellManager").SpellLevels[ActorSpellManager.Spell.FireBolt] += 1;
				IncreaseSpellSkill(ActorSpellManager.Spell.FireBolt);
				break;
			case Skill.Fireball:
				IncreaseSpellSkill(ActorSpellManager.Spell.Fireball);
				break;
			case Skill.Berserk:
				IncreaseSpellSkill(ActorSpellManager.Spell.Berserk);
				break;
			case Skill.Blink:
				IncreaseSpellSkill(ActorSpellManager.Spell.Blink);
				Actor.GetNode<ActorSpellManager>("SpellManager").SetBlinkCost();
				// Actor.GetNode<ActorSpellManager>("SpellManager").SpellCosts[ActorSpellManager.Spell.Blink] =
				// 	Math.Max(1,Actor.GetNode<ActorSpellManager>("SpellManager").SpellCosts[ActorSpellManager.Spell.Blink]/1.5f);
				
				break;
			case Skill.Heal:
				IncreaseSpellSkill(ActorSpellManager.Spell.Heal);
				break;
		}
		StatsUpdated?.Invoke(GetUpdatedStats());

		// _ship.UpdateUI();
		// LevelledUp?.Invoke(_ship.UID, Skillpoints, _level);
	}

	public void IncreaseSpellSkill(ActorSpellManager.Spell spell)
	{
		Actor.GetNode<ActorSpellManager>("SpellManager").SpellLevels[spell] += 1;
	}

	public int GetSpellLevel(ActorSpellManager.Spell spell)
	{
		return Actor.GetNode<ActorSpellManager>("SpellManager").SpellLevels[spell];
	}

	public float PercentageToNextLvl()
	{
		float totalNeeded = GetExperienceNeeded(Level+1) - GetExperienceNeeded(Level);
		// GD.Print(totalNeeded);
		
		float haveToNextLevel = Level == 1 ? _experience: _experience - GetExperienceNeeded(Level);
		// GD.Print(".. ", haveToNextLevel);
		return haveToNextLevel / totalNeeded;
	}
}
