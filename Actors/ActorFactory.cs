using Godot;
using System;
using System.Collections.Generic;

public class ActorFactory : Node
{

	// private Dictionary<ActorBody, string> _actorBodyScenePaths = new Dictionary<ActorBody, string> {
	// 	{ ActorBody.Player, "res://Actors/Player/PlayerBody.tscn"},
	// 	{ ActorBody.Monster, "res://Actors/Monster/MonsterBody.tscn"}
	// };
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";
	private int _IDs = 0;

	private enum ActorBody {
		Player,
		Monster
	}

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		
	}

	public Actor CreatePlayer()
	{
		Actor actor = NewActor();
		actor.ID = _IDs;
		_IDs += 1;
		// actor.Gravity = 0;
		PlayerBody body = NewPlayerActorBody();
		body.Actor = actor;
		actor.SetInput(Actor.InputManager.Player);
		body.Name = "Body";
		actor.AddChild(body);
		body.Owner = actor;
		return actor;
	}

	public Actor CreateMonster(Vector2 position)
	{
		Actor actor = CreateActorCommon();
		actor.Gravity = 0;
		actor.Speed = actor.StartingSpeed = 25;
		MonsterBody body = NewMonsterActorBody();
		body.Actor = actor;
		body.Name = "Body";
		actor.AddChild(body);
		body.Owner = actor;
		actor.Position = position;
		actor.OriginalPos = position;
		actor.SetInput(Actor.InputManager.AI);
		return actor;
	}

	public Actor CreateActorCommon()
	{
		Actor actor = NewActor();
		actor.ID = _IDs;
		_IDs += 1;;

		return actor;
	}

	private PlayerBody NewPlayerActorBody()
	{
		return (PlayerBody)GD.Load<PackedScene>("res://Actors/Player/PlayerBody.tscn").Instance();
	}
	private MonsterBody NewMonsterActorBody()
	{
		return (MonsterBody)GD.Load<PackedScene>("res://Actors/Monster/MonsterBody.tscn").Instance();
	}

	private Actor NewActor()
	{
		return (Actor)GD.Load<PackedScene>("res://Actors/Actor.tscn").Instance();
	}

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}
