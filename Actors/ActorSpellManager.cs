using Godot;
using System;
using System.Collections.Generic;

public class ActorSpellManager : Node2D
{

	public Actor Actor {get; set;}

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		
	}

	public delegate void SpellCastDelegate(Spell spell, Vector2 pos, float power, Vector2 target, bool sourcePlayer=true);
	public event SpellCastDelegate SpellCast;

	public delegate void SpellAddedDelegate(Spell spell);
	public event SpellAddedDelegate SpellAdded;

	public delegate void SpellFizzledDelegate(bool notSpirit, bool artefact = false);
	public event SpellFizzledDelegate SpellFizzled;
	// public delegate void CheckEngagedDelegate(ActorSpellManager spellManager);
	// public event CheckEngagedDelegate CheckEngaged;

	// public delegate void SpellActiveDelegate(Spell spell);
	// public event SpellActiveDelegate SpellActive;

	public Dictionary<ActorSpellManager.Spell, float> SpellCosts {get; set;} = new Dictionary<ActorSpellManager.Spell, float>()
	{
		{Spell.FireBolt, 1f},
		{Spell.Fireball, 4f},
		{Spell.Heal, 4},
		{Spell.Berserk, 12},
		{Spell.Blink, 15},
		{Spell.SpiritArtefact, 15}

	};
	public Dictionary<ActorSpellManager.Spell, int> SpellLevels {get; set;} = new Dictionary<ActorSpellManager.Spell, int>()
	{
		{Spell.FireBolt, 0},
		{Spell.Fireball, 0},
		{Spell.Heal, 0},
		{Spell.Berserk, 0},
		{Spell.Blink, 0},
		{Spell.SpiritArtefact, 0}

	};

	public void SetBlinkCost()
	{
		SpellCosts[Spell.Blink] = Math.Max(1, 15 - (float)Math.Pow(SpellLevels[Spell.Blink], 1.5f));
	}

	private List<Spell> _spellsKnown = new List<Spell>();

	public void SetSpellLevels(Dictionary<ActorSpellManager.Spell, int> playerSpellLevels)
	{
		foreach (KeyValuePair<ActorSpellManager.Spell, int> kv in playerSpellLevels)
		{
			SpellLevels[kv.Key] = kv.Value;
		}
	}

	public List<Spell> GetKnownSpells()
	{
		return _spellsKnown;
	}

	public void SetKnownSpells(List<Spell> knownSpells)
	{
		_spellsKnown = knownSpells;
	}

	// in future best to store spell info in json
	public enum Spell
	{
		FireBolt,
		Fireball,
		Heal,
		Berserk,
		Blink,
		SpiritArtefact,
		Empty
	}

	public void AddSpell(Spell spell)
	{
		if (_spellsKnown.Contains(spell))
		{
			return;
		}
		_spellsKnown.Add(spell);
		SpellAdded?.Invoke(spell);
		SpellLevels[spell] = 1;
	}

	public Spell CurrentSpell {get; set;} = Spell.Empty;
	// public Spell PrevSpell {get; set;} = Spell.Empty;

	// private bool _singleCast = false;

	public void SetActiveSpell(Spell spell)
	{
		// PrevSpell = CurrentSpell;
		// if (PrevSpell != CurrentSpell)
		// {
		// 	PrevSpell = CurrentSpell;
		// }
		CurrentSpell = spell;
		// _singleCast = singleCast;
		// SpellActive?.Invoke(_currentSpell);
	}

	public void Cast(float power, Vector2 target, Spell spell)
	{
		// GD.Print("PEWPEW! ", _currentSpell.ToString());

		SpellCast?.Invoke(spell, GlobalPosition*2, GetOverallPower(spell), target);
		// if (_singleCast)
		// {
		// 	CurrentSpell = PrevSpell;
		// }

		// return _spellCosts[_currentSpell];
		// GD.Print(GlobalPosition);
	}

	public override void _PhysicsProcess(float delta)
	{
	   	if (Actor == null)
		{
			return;
		}
		if (! (Actor.GetNode<IActorBody>("Body") is PlayerBody))
		{
			return;
		}

		// Actor.TryCast = Input.IsActionPressed("Cast");
		// if (!MouseEnabled && Actor.TryCast)
		// {
		// 	Actor.TryCast = false;
		// 	GD.Print("Mouse disabled?");
		// }
		// Actor.TryCast = Input.IsActionPressed("Cast");
		// if (!Actor.GetInputManager().MouseEnabled && Actor.TryCast)
		// {
		// 	Actor.TryCast = false;
		// 	GD.Print("Mouse disabled?");
		// }
		
		// if (Input.IsActionPressed("FireBoltSpell"))
		// {
		// 	CastSelectedSpell(Spell.FireBolt);
		// }
		// if (Input.IsActionPressed("FireballSpell"))
		// {
		// 	CastSelectedSpell(Spell.Fireball);
		// }
		// if (Input.IsActionPressed("HealSpell"))
		// {
		// 	CastSelectedSpell(Spell.Heal);
		// }
		// if (Input.IsActionPressed("BerserkSpell"))
		// {
		// 	CastSelectedSpell(Spell.Berserk);
		// }
		// if (Input.IsActionPressed("BlinkSpell"))
		// {
		// 	CastSelectedSpell(Spell.Blink);
		// }
		// if (Input.IsActionPressed("ArtefactSpell"))
		// {
		// 	CastSelectedSpell(Spell.SpiritArtefact);
		// }
	}

	// public void OnArtefactUsed()
	// {
	// 	// CastSelectedSpell(Spell.SpiritArtefact);

	// }

	public Spell HotKeyCastSpell {get; set;} = Spell.Empty;
	public Spell ArtefactCastSpell {get; set;} = Spell.Empty;

	public bool EngagedByMonsters {get; set;} = false;
	// public Spell ClickCastSpell {get; set;} = Spell.Empty;
	

	public void CastSelectedSpell(Spell spell)
	{
		if (!_spellsKnown.Contains(spell))
		{
			return;
		}

		if (Actor.GetNode<Timer>("SpellCooldownTimer").TimeLeft > 0)
		{
			// GD.Print(Actor.GetNode<Timer>("SpellCooldownTimer").TimeLeft);
			return;
		}
		
		// Spell prevSpell = CurrentSpell;
		// CurrentSpell = spell;

		if (SpellLevels[spell] == 0)
		{
			return; // dont know the spell
		}

		if (spell == Spell.SpiritArtefact && EngagedByMonsters) // dont allow using artefact when fighting
		{
			SpellFizzled?.Invoke(false, true);
			return;
		}
		
		if (Actor.GetInputManager().CanAffordToCastSpell(spell) && Actor.GetInputManager().CanCastSpirit(spell))
		{
			Actor.GetNode<ActorSpellManager>("SpellManager").Cast(Actor.ActorStats["Power"], Actor.GetInputManager().Target, spell);
			Actor.GetNode<ActorDamageHandler>("DamageHandler").ModMana(-GetSpellCost(spell));
			// Actor.ActorStats["Mana"] -= _spellManager.SpellCosts[_spellManager.CurrentSpell];
			// GD.Print(Actor.ActorStats["Mana"], " mana remains.");
			Actor.GetNode<Timer>("SpellCooldownTimer").Start();


			if (Actor.GetNode<IActorBody>("Body") is PlayerBody player)
			{
				player.PlaySpellSound(player.SpellSounds[_spellCastSoundDict[spell]]);
			}
		}
		else
		{
			if (!Actor.GetInputManager().CanAffordToCastSpell(spell))
			{
				// GD.Print("SPELL FIZZLED! OUT OF MANA");
				SpellFizzled?.Invoke(false);
			}
			if (!Actor.GetInputManager().CanCastSpirit(spell))
			{
				// GD.Print("SPELL FIZZLED! MAY ONLY CAST IN SPIRIT FORM");
				SpellFizzled?.Invoke(true);
			}
			if (Actor.GetNode<IActorBody>("Body") is PlayerBody player)
			{
				player.PlaySpellSound(player.SpellSounds[AudioData.Spell.Fizzle]);
			}		
		}
		// CurrentSpell = prevSpell;	
	}

	private Dictionary<Spell, AudioData.Spell> _spellCastSoundDict = new Dictionary<Spell, AudioData.Spell>()
	{
		{Spell.Berserk, AudioData.Spell.BerserkCast},
		{Spell.Blink, AudioData.Spell.BlinkCast},
		{Spell.Fireball, AudioData.Spell.FireballCast},
		{Spell.FireBolt, AudioData.Spell.FireboltCast},
		{Spell.Heal, AudioData.Spell.HealCast},
		{Spell.SpiritArtefact, AudioData.Spell.SpiritArtefact}
	};

	public Dictionary<ActorSpellManager.Spell, Dictionary<WorldSpellManager.SpellStat, float>> SpellData {get; set;}
	private Dictionary<ActorSpellManager.Spell, float> _spellCostPowerMultiplier = new Dictionary<ActorSpellManager.Spell, float>()
	{
		{ActorSpellManager.Spell.Berserk, 0},
		{ActorSpellManager.Spell.Blink, 1},
		{ActorSpellManager.Spell.Fireball, 0},
		{ActorSpellManager.Spell.FireBolt, 0},
		{ActorSpellManager.Spell.Heal, 0},
		{ActorSpellManager.Spell.SpiritArtefact, 0}
	};

	// private int GetSpellLevel(ActorSpellManager.Spell spell)
	// {
	// 	return _spellManager.SpellLevels[spell];
	// }

	public float GetSpellCost(ActorSpellManager.Spell spell)
	{
		// if (spell == Spell.Empty)
		// {
		// 	GD.Print("oh no");
		// 	return 0;
		// }
		// if the cost is adjustable by power, adjust it
		// int levelCostModifier = 2; // higher makes spell levels more powerful at reducing costs
		// float costReductionDivisor = 10f; // higher makes overall impact of power and spelllevel less on the spell cost
		int minimumCost = 1;
		float cost = Math.Max(SpellCosts[spell], minimumCost);
		
		// float cost = Math.Max(SpellCosts[spell] - (_spellCostPowerMultiplier[spell]
		//  * (Actor.ActorStats["Power"] + levelCostModifier*SpellLevels[spell]) /costReductionDivisor), minimumCost);

		 return cost;
	}

	public float GetBerserkDuration()
	{
		return Math.Max(1, GetOverallPower(Spell.Berserk)/2f - 5);
	}

	public float GetOverallPower(ActorSpellManager.Spell spell)
	{
		return SpellData[spell][WorldSpellManager.SpellStat.BasePower]*(1+((float)Math.Pow(SpellLevels[spell], 2)/5f)) + SpellLevels[spell] + (Actor.ActorStats["Power"]/5f);
	}

	// private Dictionary<ActorSpellManager.Spell, float> spellPowerMultipliers = new Dictionary<Spell, float>()
	// {
	// 	{Spell.FireBolt, 1},
	// 	{Spell.Fireball, 2},
	// 	{Spell.Berserk, 0.2f},
	// 	{Spell.Blink, 1f},
	// 	{Spell.Heal, 2f}
	// };
	// public override void Update(float delta)
	// {
	// 	base.Update(delta);

	// 	// if the cost is adjustable by power, adjust it
	// 	int levelCostModifier = 2; // higher makes spell levels more powerful at reducing costs
	// 	float costReductionDivisor = 5f; // higher makes overall impact of power and spelllevel less on the spell cost
	// 	int minimumCost = 1;
	// 	float cost = Math.Max(_spellManager.SpellCosts[_spellManager.CurrentSpell] - (_spellCostPowerMultiplier[_spellManager.CurrentSpell]
	// 	 * (Actor.ActorStats["Power"] + levelCostModifier*_spellManager.SpellLevels[_spellManager.CurrentSpell]) /costReductionDivisor), minimumCost);


}
