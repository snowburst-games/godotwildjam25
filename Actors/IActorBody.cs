using Godot;
using System;
using System.Collections.Generic;

public interface IActorBody
{
	void Flip();
	void UnsubscribeEvents();

	void OnDeath();

	List<RayCast2D> CollisionRays {get; set;}
	Actor Actor {get; set;}
	void StartBerserk(float power);
}
