using Godot;
using System;

public interface IActorInputManager
{

	Actor Actor{
		get;
		set;
	}

	Vector2 Target{
		get;
		set;
	}
	Vector2 PlayerTarget{
		get;
		set;
	}
	bool MouseEnabled {get; set;}

	void Update(float delta);

	bool CanAffordToCastSpell(ActorSpellManager.Spell spell);
	bool CanCastSpirit(ActorSpellManager.Spell spell);
}
