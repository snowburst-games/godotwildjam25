using Godot;
using System;

public class AttackMonsterAIState : MonsterAIState
{
	public AttackMonsterAIState()
	{

	}
	private bool _casted = false;
	public AttackMonsterAIState(MonsterInputManager inputManager)
	{
		InputManager = inputManager;

		if (InputManager.Actor.CurrentActionState == Actor.ActionState.Dying)
		{
			InputManager.SetAIState(MonsterInputManager.AIState.Pursue);
		}

		if (InputManager.Actor.GetNode<MonsterBody>("Body").Level == MonsterBody.PowerLevel.Boss)
		{
			if (InputManager.BossPhase == 1)
			{
				if (InputManager.Actor.GetNode<MonsterBody>("Body").InAttackRange)
				{
					// GD.Print("TEST1");
					InputManager.MeleeBullet();
				}
			}
			if (InputManager.BossPhase == 2)
			{
				InputManager.Actor.GetNode<MonsterBody>("Body").OnSpellCast(ActorSpellManager.Spell.FireBolt, InputManager.Actor.Position*2, 
					InputManager.Actor.ActorStats["Power"], InputManager.Actor.Position*2 + new Vector2(new Random().Next(-100, 100), new Random().Next(-100, 100)) );// InputManager.PlayerTarget*2);
				InputManager.Actor.GetNode<MonsterBody>("Body").OnSpellCast(ActorSpellManager.Spell.FireBolt, InputManager.Actor.Position*2, 
					InputManager.Actor.ActorStats["Power"], InputManager.Actor.Position*2 + new Vector2(new Random().Next(-100, 100), new Random().Next(-100, 100)) );// InputManager.PlayerTarget*2);
			}
		}
		else
		{
			if (InputManager.Actor.GetNode<MonsterBody>("Body").InAttackRange)
			{
				// GD.Print("TEST1");
				InputManager.MeleeBullet();
			}
			else
			{
				if (InputManager.Actor.GetNode<MonsterBody>("Body").Level == MonsterBody.PowerLevel.Three)
				{
					// GD.Print("TEST2");
					// do fire bolt!
					InputManager.Actor.GetNode<MonsterBody>("Body").OnSpellCast(ActorSpellManager.Spell.FireBolt, InputManager.Actor.Position*2, 
						InputManager.Actor.ActorStats["Power"], InputManager.PlayerTarget*2);
					InputManager.Actor.GetNode<MonsterBody>("Body").OnSpellCast(ActorSpellManager.Spell.FireBolt, InputManager.Actor.Position*2, 
						InputManager.Actor.ActorStats["Power"], InputManager.PlayerTarget*2);
				}
			}
		}

		InputManager.Actor.GetNode<Timer>("Body/AttackCooldown").Start();
		// InputManager.Actor.PlayAnim("Cast");
		InputManager.Actor.GetNode<AnimationPlayer>("Body/AIAnim").Play("Cast");

		InputManager.Actor.GetNode<MonsterBody>("Body").PlaySound(GetRandomAttackSound(new Random().Next(0,2)), AudioData.SoundBus.Voice, 3);
	}

	public AudioStream GetRandomAttackSound(int num)
	{
		return num <= 0 ? InputManager.Actor.GetNode<MonsterBody>("Body").GhostSounds[AudioData.Ghost.Attack1]
			: InputManager.Actor.GetNode<MonsterBody>("Body").GhostSounds[AudioData.Ghost.Attack1];
	}

	public override void Update(float delta)
	{
		base.Update(delta);
		if (_casted)
		{
			return;
		}
		EndCast();
	}

	public async void EndCast()
	{
		_casted = true;
		await ToSignal(InputManager.Actor.GetNode<AnimationPlayer>("Body/AIAnim"), "animation_finished");

		InputManager.SetAIState(MonsterInputManager.AIState.Pursue);
	}
}
