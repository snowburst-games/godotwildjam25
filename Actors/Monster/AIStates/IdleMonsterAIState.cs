using Godot;
using System;

public class IdleMonsterAIState : MonsterAIState
{
	public IdleMonsterAIState()
	{

	}
	public IdleMonsterAIState(MonsterInputManager inputManager)
	{
		InputManager = inputManager;
		InputManager.Origin = InputManager.Actor.Position;
		InputManager.Actor.Speed = InputManager.Actor.StartingSpeed;
	}
	public override void Update(float delta)
	{
		base.Update(delta);

		if (InputManager.Actor.GetNode<MonsterBody>("Body").PlayerFound && InputManager.Actor.GetNode<MonsterBody>("Body").InSpiritWorld)
		{
			// go to pursuit mode
			InputManager.SetAIState(MonsterInputManager.AIState.Pursue);
		}

		if (InputManager.Actor.GetNode<MonsterBody>("Body").Level == MonsterBody.PowerLevel.Two) // level 2 monsters can blink
		{
			if (new Random().Next(0,100) > 98 && InputManager.Actor.GetNode<Timer>("Body/BlinkCooldown").TimeLeft == 0)
			{
				InputManager.Actor.Blink(InputManager.Actor.Position*2 + new Vector2(new Random().Next(-50,50),new Random().Next(-50,50)), 50);
				InputManager.Actor.GetNode<Timer>("Body/BlinkCooldown").Start();
			}
		}

		// don't stray too far from where we started
		if (InputManager.Actor.Position.DistanceTo(InputManager.Origin) > 300)
		{
			// GD.Print("TOO FAR! ", InputManager.Origin);
			DirectTowardsTarget(InputManager.Origin);
			return;
		}
		// Just keep moving in random directions when idle
		if (InputManager.Actor.GetNode<Timer>("Body/AITimer").TimeLeft == 0)
		{
			RandomDirection();
			// SwitchXDirection(InputManager.Actor.TryRunLeft ? false : true);
			// SwitchYDirection(InputManager.Actor.TryMoveUp ? false : true);
			InputManager.Actor.GetNode<Timer>("Body/AITimer").Start();
		}

	}
	
	public void RandomDirection()
	{
		InputManager.Actor.TryMoveUp = new Random().Next(0,2) == 0;
		InputManager.Actor.TryMoveDown = ! InputManager.Actor.TryMoveUp;
		InputManager.Actor.TryRunLeft = new Random().Next(0,2) == 0;
		InputManager.Actor.TryRunRight = ! InputManager.Actor.TryRunLeft;
	}

}
