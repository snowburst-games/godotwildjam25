using Godot;
using System;

public class MonsterAIState : Reference
{
	public MonsterInputManager InputManager {get; set;}

	public MonsterAIState()
	{

	}
	public MonsterAIState(MonsterInputManager inputManager)
	{
		InputManager = inputManager;
	}

	public virtual void Update(float delta)
	{
		
	}

	public void DirectTowardsTarget(Vector2 targetPos)
	{
		if (InputManager.Actor.Position.DistanceTo(targetPos) <= 10)
		{
			InputManager.Actor.TryRunLeft = false;
			InputManager.Actor.TryRunRight = false;
			InputManager.Actor.TryMoveUp = false;
			InputManager.Actor.TryMoveDown = false;
			return;
		}
		TryRunLeftEnabled( // if we don't want the ghost to swing around, can make it a series of if statements
			InputManager.Actor.Position.x < targetPos.x-20 && InputManager.Actor.TryRunLeft ? false :
			InputManager.Actor.Position.x > targetPos.x+20 && InputManager.Actor.TryRunRight ? true :
			InputManager.Actor.TryRunLeft
		);
		TryMoveUpEnabled(
			InputManager.Actor.Position.y < targetPos.y-20 && InputManager.Actor.TryMoveUp ? false :
			InputManager.Actor.Position.y > targetPos.y+20 && InputManager.Actor.TryMoveDown ? true :
			InputManager.Actor.TryMoveUp
		);
	}

	public void TryRunLeftEnabled(bool enabled)
	{
		InputManager.Actor.TryRunLeft = enabled;
		InputManager.Actor.TryRunRight = ! InputManager.Actor.TryRunLeft;
	}
	public void TryMoveUpEnabled(bool enabled)
	{
		InputManager.Actor.TryMoveUp = enabled;
		InputManager.Actor.TryMoveDown = ! InputManager.Actor.TryMoveUp;
	}

	public virtual void EndState()
	{
		foreach (RayCast2D ray in InputManager.Actor.GetNode<IActorBody>("Body").CollisionRays)
		{
			ray.Enabled = false;
		}
	}
}
