using Godot;
using System;
using System.Collections.Generic;

public class PursueMonsterAIState : MonsterAIState
{
	public PursueMonsterAIState()
	{

	}
	public PursueMonsterAIState(MonsterInputManager inputManager)
	{
		InputManager = inputManager;
		InputManager.Actor.Speed = InputManager.Actor.StartingSpeed * 2;
		InputManager.Actor.GetNode<Timer>("Body/AITimer").WaitTime = 3;
		InputManager.Actor.GetNode<Timer>("Body/AITimer2").WaitTime = 0.5f;
		if (InputManager.Actor.GetNode<MonsterBody>("Body").Level != MonsterBody.PowerLevel.One &&
			InputManager.Actor.GetNode<MonsterBody>("Body").Level != MonsterBody.PowerLevel.Two)
		{
			foreach (RayCast2D ray in InputManager.Actor.GetNode<IActorBody>("Body").CollisionRays)
			{
				ray.Enabled = true;
			}
		}

		// foreach (KeyValuePair<string, float> kv in InputManager.Actor.ActorStats)
		// {
		// 	// GD.Print(kv.Key, ": ", kv.Value);
		// }
		if (InputManager.Actor.GetNode<MonsterBody>("Body").Level == MonsterBody.PowerLevel.Boss)
		{
			InputManager.Actor.GetNode<MonsterBody>("Body").OnBossEngaged();
		}

		InputManager.Actor.GetNode<MonsterBody>("Body").PlaySound(
				InputManager.Actor.GetNode<MonsterBody>("Body").GhostSounds[AudioData.Ghost.Pursue], AudioData.SoundBus.Voice, 1);
		// InputManager.Actor.GetNode<Timer>("Body/AITimer").Start();
		// InputManager.LookForPlayer();
	}
	public override void Update(float delta)
	{
		base.Update(delta);
		InputManager.LookForPlayer(true);

		if (!InputManager.Actor.GetNode<MonsterBody>("Body").InSpiritWorld)
		{
			InputManager.SetAIState(MonsterInputManager.AIState.Idle);
		}
		// GD.Print(InputManager.Actor.GetNode<MonsterBody>("Body").IsColliding());

		if (InputManager.Actor.GetNode<MonsterBody>("Body").Level != MonsterBody.PowerLevel.Three && 
			InputManager.Actor.GetNode<MonsterBody>("Body").Level != MonsterBody.PowerLevel.Boss)
		{
			InputManager.LookForPlayer();
			// InputManager.PlayerTarget = InputManager.Target;
		}
		if (InputManager.Actor.GetNode<MonsterBody>("Body").Level == MonsterBody.PowerLevel.Three && InputManager.Actor.GetNode<MonsterBody>("Body").Berserk)
		{
			InputManager.LookForPlayer();
		}
		else if (InputManager.Actor.GetNode<MonsterBody>("Body").Level == MonsterBody.PowerLevel.Three)
		{
			// if (InputManager.Actor.GetNode<MonsterBody>("Body").IsColliding())
			// {
			// 	InputManager.Actor.GetNode<Timer>("Body/AITimer").WaitTime = 1;
			// }
			// else
			// {
			// 	InputManager.Actor.GetNode<Timer>("Body/AITimer").WaitTime = 3;
			// }
			
			if (InputManager.Actor.GetNode<Timer>("Body/AITimer").TimeLeft == 0)
			{
				InputManager.LookForPlayer();
				// InputManager.PlayerTarget = InputManager.Target;
				Vector2 direction = (InputManager.Actor.Position - InputManager.Target).Normalized();
				InputManager.Target = InputManager.Target + direction*( new Random().Next(100,200));
				InputManager.Actor.GetNode<Timer>("Body/AITimer").Start();
			}
			else
			{
				if (InputManager.Actor.GetNode<Timer>("Body/AITimer2").TimeLeft == 0)
				{
					InputManager.Target = InputManager.Target + new Vector2(new Random().Next(-50,50), new Random().Next(-50,50));
					InputManager.Actor.GetNode<Timer>("Body/AITimer2").Start();
				}
			}
			// this prevents monster from colliding! goes towards player if it is colliding.. good for shooting
			if (InputManager.Actor.GetNode<MonsterBody>("Body").CollisionRaysColliding())
			{
				// InputManager.Actor.GetNode<Timer>("Body/AITimer").WaitTime = 5;
				InputManager.LookForPlayer();
				// InputManager.PlayerTarget = InputManager.Target;
				InputManager.Actor.GetNode<Timer>("Body/AITimer").Start();
			}

			// next is firebolt firing!
			if (InputManager.Actor.GetNode<Timer>("Body/AttackCooldown").TimeLeft == 0)
			{
				InputManager.SetAIState(MonsterInputManager.AIState.Attack);
			}
			// fire a firebolt at the player ever
		}
		if (InputManager.Actor.GetNode<MonsterBody>("Body").Level == MonsterBody.PowerLevel.Boss)
		{
			
			if (InputManager.BossPhase == 1)
			{
				InputManager.LookForPlayer();

				if (new Random().Next(0,100) > 95 && InputManager.Actor.GetNode<Timer>("Body/BlinkCooldown").TimeLeft == 0)
				{
					InputManager.Actor.Blink(InputManager.Target*2, 75, true);
					InputManager.Actor.GetNode<Timer>("Body/BlinkCooldown").Start();
				}
				if (InputManager.Actor.GetNode<Timer>("Body/PhaseTimer").TimeLeft == 0)
				{
					NextPhase();
				}
			}
			if (InputManager.BossPhase == 2)
			{

				// if (InputManager.Actor.GetNode<Timer>("Body/BossFireTimer").TimeLeft == 0)
				{
					InputManager.SetAIState(MonsterInputManager.AIState.Attack);
					// InputManager.Actor.GetNode<Timer>("Body/BossFireTimer").Start();
				}

				if (InputManager.Actor.GetNode<Timer>("Body/BossRegenTimer").TimeLeft == 0)
				{
					InputManager.Actor.GetNode<ActorDamageHandler>("DamageHandler").ModHealth(InputManager.Actor.ActorStats["MaxHealth"]*0.05f);
					InputManager.Actor.GetNode<MonsterBody>("Body").OnTookDamage();
					InputManager.Actor.GetNode<Timer>("Body/BossRegenTimer").Start();
				}

				
				if (InputManager.Actor.GetNode<Timer>("Body/PhaseTimer").TimeLeft == 0)
				{
					NextPhase();
				}
			}
			if (InputManager.BossPhase == 3)
			{
				// InputManager.Actor.GetNode<Timer>("Body/PhaseTimer").Start();

				if (InputManager.Actor.GetNode<Timer>("Body/BossGhostTimer").TimeLeft == 0)
				{
					InputManager.SpawnGhost();
					InputManager.Actor.GetNode<Timer>("Body/BossGhostTimer").Start();
				}

				if (InputManager.Actor.GetNode<Timer>("Body/PhaseTimer").TimeLeft == 0)
				{
					NextPhase();
				}
			}
		}
		// if (InputManager.Actor.Position.DistanceTo(InputManager.Target) < 25 && InputManager.Actor.GetNode<MonsterBody>("Body").IsColliding())
		// {

		// }

		DirectTowardsTarget(InputManager.Target);

		if (InputManager.Actor.GetNode<MonsterBody>("Body").Level == MonsterBody.PowerLevel.Two)
		{
			if (new Random().Next(0,100) > 98 && InputManager.Actor.GetNode<Timer>("Body/BlinkCooldown").TimeLeft == 0)
			{
				InputManager.Actor.Blink(InputManager.Target*2, 75, true);
				InputManager.Actor.GetNode<Timer>("Body/BlinkCooldown").Start();
			}
		}

		if (InputManager.Actor.GetNode<MonsterBody>("Body").InAttackRange && InputManager.Actor.GetNode<Timer>("Body/AttackCooldown").TimeLeft == 0)
		{
			InputManager.SetAIState(MonsterInputManager.AIState.Attack);
		}

		if (InputManager.Target == new Vector2(-10000, -10000))
		{
			InputManager.SetAIState(MonsterInputManager.AIState.Idle);
		}
	}

	// public int BossPhase {get; set;} = 1;

	public void NextPhase()
	{
		InputManager.Actor.GetNode<Timer>("Body/PhaseTimer").Start();
		InputManager.BossPhase += 1;
		if (InputManager.BossPhase == 4)
		{
			InputManager.BossPhase = 1;
		}
	}
}
