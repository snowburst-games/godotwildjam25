using Godot;
using System;
using System.Collections.Generic;

public class MonsterBody : KinematicBody2D, IActorBody
{

	public delegate void DiedDelegate(float experienceDropped, Vector2 origin, PowerLevel level, float power);
	public event DiedDelegate Died;
	public delegate void DiedAnimCompletedDelegate(PowerLevel level);
	public event DiedAnimCompletedDelegate DiedAnimCompleted;

	public delegate void SpellCastDelegate(ActorSpellManager.Spell spell, Vector2 pos, float power, Vector2 target, bool sourcePlayer=false);
	public event SpellCastDelegate SpellCast;

	public delegate void TookDamageDelegate(float currentHealth, float maxHealth);
	public event TookDamageDelegate TookDamage;
	public delegate void BossEngagedDelegate();
	public event BossEngagedDelegate BossEngaged;

	public Actor Actor {get; set;}
	public bool PlayerFound {get; set;} = false;
	public bool InAttackRange {get; set;} = false;
	public List<RayCast2D> CollisionRays {get; set;}
	
	public bool Berserk {get; set;} = false;

	private float _experienceValue = 1;
	private bool _died = false;
	// public int BossPhase {get; set;} = 1;

	public enum PowerLevel { One, Two, Three, Boss};

	public PowerLevel Level {get; set;} = PowerLevel.One;
	

	private AudioStreamPlayer2D _soundPlayer;
	public Dictionary<AudioData.Ghost, AudioStream> GhostSounds {get; set;} = AudioData.LoadedSounds<AudioData.Ghost>(AudioData.GhostSoundPaths);

	public bool CollisionRaysColliding()
	{
		foreach(RayCast2D ray in CollisionRays)
		{
			if (ray.IsColliding())
			{
				return true;
			}
		}
		return false;
	}

	public void SetPower(PowerLevel level, float gameDifficulty)
	{

		GetNode<CPUParticles2D>("SpiritParticles").Visible = false;
		GetNode<CPUParticles2D>("PowerOrbParticles").Visible = false;
		GetNode<CPUParticles2D>("Fire").Visible = false;
		GetNode<CPUParticles2D>("Fire2").Visible = false;
		switch (level)
		{
			case PowerLevel.One:
				Level = PowerLevel.One;
				Actor.GetNode<ActorDamageHandler>("DamageHandler").SetHealth(15*gameDifficulty);
				// Actor.GetNode<ActorDamageHandler>("DamageHandler").SetMana(20);
				Actor.GetNode<ActorDamageHandler>("DamageHandler").SetPower(8*gameDifficulty);
				Actor.Speed = Actor.StartingSpeed = 25;
				Actor.Scale = new Vector2(1,1);
				Actor.GetNode<Sprite>("Body/Sprite").Modulate = Actor.OriginalColor = new Color(1,1,1);
				_experienceValue = 2;
				_soundPlayer.PitchScale = 1.5f;
				break;
			case PowerLevel.Two:
				GetNode<CPUParticles2D>("SpiritParticles").Visible = true;
				Level = PowerLevel.Two;
				Actor.GetNode<ActorDamageHandler>("DamageHandler").SetHealth(60*gameDifficulty);
				// Actor.GetNode<ActorDamageHandler>("DamageHandler").SetMana(20);
				Actor.GetNode<ActorDamageHandler>("DamageHandler").SetPower(15*gameDifficulty);
				Actor.Speed = Actor.StartingSpeed = 35;
				Scale = new Vector2(1.3f,1.3f);
				Actor.GetNode<Sprite>("Body/Sprite").Modulate = Actor.OriginalColor = new Color(1,1,1);
				_experienceValue = 10;
				_soundPlayer.PitchScale = 1.2f;
				break;
			case PowerLevel.Three:
				GetNode<CPUParticles2D>("SpiritParticles").Visible = true;
				GetNode<CPUParticles2D>("PowerOrbParticles").Visible = true;
				Level = PowerLevel.Three;
				Actor.GetNode<ActorDamageHandler>("DamageHandler").SetHealth(60*gameDifficulty);
				// Actor.GetNode<ActorDamageHandler>("DamageHandler").SetMana(20);
				Actor.GetNode<ActorDamageHandler>("DamageHandler").SetPower(8*gameDifficulty);
				Actor.Speed = Actor.StartingSpeed = 60;
				Scale = new Vector2(1.6f,1.6f);
				Actor.GetNode<Sprite>("Body/Sprite").Modulate = Actor.OriginalColor = new Color(1,1,1);
				_experienceValue = 22;
				_soundPlayer.PitchScale = 1f;
				break;
			case PowerLevel.Boss:
				GetNode<CPUParticles2D>("SpiritParticles").Visible = true;
				GetNode<CPUParticles2D>("PowerOrbParticles").Visible = true;
				GetNode<CPUParticles2D>("Fire").Visible = true;
				GetNode<CPUParticles2D>("Fire2").Visible = true;
				Level = PowerLevel.Boss;
				Actor.GetNode<ActorDamageHandler>("DamageHandler").SetHealth(600*gameDifficulty);
				// Actor.GetNode<ActorDamageHandler>("DamageHandler").SetMana(20);
				Actor.GetNode<ActorDamageHandler>("DamageHandler").SetPower(30*gameDifficulty);
				Actor.Speed = Actor.StartingSpeed = 75;
				Scale = new Vector2(2,2);
				Actor.GetNode<Sprite>("Body/Sprite").Modulate = Actor.OriginalColor = new Color(1,1,1);
				_experienceValue = 250;
				_berserkImmune = true;
				_soundPlayer.PitchScale = 0.8f;
				break;
		}
	}
	// public Dictionary<string, float> ActorStats = new Dictionary<string, float>()
	// {
	// 	{"MaxHealth", 20},
	// 	{"Health", 20},
	// 	{"Mana", 60},
	// 	{"MaxMana", 60},
	// 	{"Power", 5}
	// };

	public AudioStream GetRandomHurtSound()
	{
		return (new AudioStream[3] {GhostSounds[AudioData.Ghost.Hurt1], GhostSounds[AudioData.Ghost.Hurt2], GhostSounds[AudioData.Ghost.Hurt3]})
			[new Random().Next(0,3)];
	}

	public void OnTookDamage()
	{
		if (Level == MonsterBody.PowerLevel.Boss)
		{
			TookDamage?.Invoke(Actor.ActorStats["Health"], Actor.ActorStats["MaxHealth"]);
		}
		if (Actor.GetInputManager() is MonsterInputManager mInput)
		{
			if (mInput.GetAIState() == MonsterInputManager.AIState.Idle || mInput.GetAIState() == MonsterInputManager.AIState.None)
			{
				mInput.SetAIState(MonsterInputManager.AIState.Pursue);
			}
		}
	}

	public void OnBossEngaged()
	{
		BossEngaged?.Invoke();
		BossEngaged = null;
	}

	public void OnSpellCast(ActorSpellManager.Spell spell, Vector2 pos, float power, Vector2 target)
	{
		SpellCast?.Invoke(spell, pos, power, target);
	}

	public override void _Ready()
	{
		base._Ready();
		CollisionRays = new List<RayCast2D>() {
		GetNode<RayCast2D>("CollideDownRay"),
		GetNode<RayCast2D>("CollideUpRay"),
		GetNode<RayCast2D>("CollideLeftRay"),
		GetNode<RayCast2D>("CollideRightRay")
		};
		
		_soundPlayer = GetNode<AudioStreamPlayer2D>("SoundPlayer");
	}

	private int _currentSoundPriority = 0;

	public void PlaySound(AudioStream stream, AudioData.SoundBus bus, int priority)
	{
		// if (_soundPlayer.Playing && !interrupt)
		// {
		// 	return;
		// }
		if (!_soundPlayer.Playing)
		{
			_currentSoundPriority = 0;
		}
		if (priority < _currentSoundPriority)
		{
			return;
		}

		if (stream == _soundPlayer.Stream)
		{
			return;
		}
		_currentSoundPriority = priority;
		AudioHandler.PlaySound(_soundPlayer, stream, bus);
	}

	public bool IsColliding()
	{
		foreach(RayCast2D ray in CollisionRays)
		{
			if (ray.IsColliding())
			{
				return true;
			}
		}
		return false;
	}

	public bool InSpiritWorld {get; set;} = false;
	
	public void LeaveSpiritWorld()
	{
		InSpiritWorld = false;
		if (Actor.GetInputManager() is MonsterInputManager monsterInputManager)
		{
			monsterInputManager.SetAIState(MonsterInputManager.AIState.Idle);
		}
	}

	public void EnterSpiritWorld()
	{
		InSpiritWorld = true;
	}

	

	private void OnDetectionAreaBodyEntered(Godot.Object body)
	{
		// GD.Print(body.ToString());
		if (Berserk)
		{
				// GD.Print("In detection range berserk");
			if (body is MonsterBody monster)
			{
				// GD.Print("In detection range");
				PlayerFound = true;
			}
			return;
		}
		if (body is PlayerBody p)
		{
			PlayerFound = true;
		}
	}

	private void OnDetectionAreaBodyExited(object body)
	{
		if (Berserk)
		{
			if (body is MonsterBody monster)
			{
				PlayerFound = false;
			}
			return;
		}
		if (body is PlayerBody p)
		{
			PlayerFound = false;
		}
	}

	public void FlipTowardsPos(Vector2 pos)
	{
		if (Position.x < pos.x/2 && !Actor.FacingRight)
		{
			Actor.Flip();
		}
		if (Position.x > pos.x/2 && Actor.FacingRight)
		{
			Actor.Flip();
		}
	}

	private void OnAttackAreaBodyEntered(object body)
	{
		if (Berserk)
		{
			if (body is MonsterBody monster)
			{
				// GD.Print("In attack range");
				InAttackRange = true;
			}
			return;
		}
		if (body is PlayerBody p)
		{
			InAttackRange = true;
		}
	}
	private void OnAttackAreaBodyExited(object body)
	{
		if (Berserk)
		{
			if (body is MonsterBody monster)
			{
				InAttackRange = false;
			}
			return;
		}
		if (body is PlayerBody p)
		{
			InAttackRange = false;
		}
	}


	public void UnsubscribeEvents()
	{
		if (Actor.GetInputManager() is MonsterInputManager monsterInputManager)
		{
			monsterInputManager.UnsubscribeEvents();

		}
		Died = null;
		SpellCast = null;
		TookDamage = null;
		BossEngaged = null;
		DiedAnimCompleted = null;
	}

	private bool _berserkImmune = false;

	public void StartBerserk(float power)
	{
		if (_berserkImmune)
		{
			return;
		}
		GetNode<Timer>("BerserkTimer").WaitTime = Math.Max(1,power);
		GetNode<Timer>("BerserkTimer").Start();
		// if (Actor.GetInputManager() is MonsterInputManager monsterInputManager)
		// {
		// 	monsterInputManager.SetAIState(MonsterInputManager.AIState.Idle);
		// }
		Berserk = true;
	}

	private void OnBerserkTimerTimeout()
	{
		Berserk = false;
	}


	public void Flip()
	{
		var leftEye = GetNode<CPUParticles2D>("Fire");
		var rightEye = GetNode<CPUParticles2D>("Fire2");

		leftEye.Position = new Vector2(-16.5f, -11);
		rightEye.Position = new Vector2(-4.5f, -11.5f);

		if (Actor.FacingRight)
		{
			leftEye.Position = new Vector2(17f, -11);
			rightEye.Position = new Vector2(5.2f, -11.5f);
		}
	}

	public void OnDeath()
	{
		if (!_died)
		{
			Died?.Invoke(_experienceValue, Actor.Position, Level, Actor.ActorStats["Power"]);
		}
		_died = true;
	}
	public void OnDeathPostAnim()
	{
		DiedAnimCompleted?.Invoke(Level);
	}
}
