using Godot;
using System;

public class MonsterInputManager : IActorInputManager
{

	public delegate void LookedForPlayerDelegate(int actorID, bool berserk, bool playerOnly);
	public event LookedForPlayerDelegate LookedForPlayer;

	public delegate void FiredBulletDelegate(int actorID, Vector2 pos, bool berserk);
	public event FiredBulletDelegate FiredBullet;

	public delegate void GhostSpawnedDelegate(int actorID);
	public event GhostSpawnedDelegate GhostSpawned;

	public Actor Actor {get; set;}
	private MonsterAIState _aiState;

	public Vector2 Origin {get; set;}
	public Vector2 Target {get; set;} = new Vector2(-10000,-10000);
	public Vector2 PlayerTarget {get; set;}
	public int BossPhase {get; set;} = 1;

	public bool MouseEnabled {get; set;}

	public enum AIState {
		Idle, Pursue, Attack, None
	}
	
	public MonsterInputManager()
	{

	}

	public MonsterInputManager(Actor actor)
	{
		this.Actor = actor;
		// Origin = Actor.Position;
		SetAIState(AIState.Idle);
	}

	public bool CanAffordToCastSpell(ActorSpellManager.Spell spell)
	{
		return false;
	}
	public bool CanCastSpirit(ActorSpellManager.Spell spell)
	{
		return false;
	}
	public void LookForPlayer(bool playerOnly = false)
	{
		LookedForPlayer?.Invoke(Actor.ID, Actor.GetNode<MonsterBody>("Body").Berserk, playerOnly);
	}

	public void SpawnGhost()
	{
		GhostSpawned?.Invoke(Actor.ID);
	}

	public void MeleeBullet()
	{
		FiredBullet?.Invoke(Actor.ID, Target, Actor.GetNode<MonsterBody>("Body").Berserk);
	}

	public void UnsubscribeEvents()
	{
		LookedForPlayer = null;
		FiredBullet = null;
	}

	public AIState GetAIState()
	{
		if (_aiState is IdleMonsterAIState)
		{
			return AIState.Idle;
		}
		else if (_aiState is PursueMonsterAIState)
		{
			return AIState.Pursue;
		}
		else if (_aiState is AttackMonsterAIState)
		{
			return AIState.Attack;
		}
		return AIState.None;
	}

	public void SetAIState(AIState state)
	{
		if (_aiState != null)
		{
			_aiState.EndState();
		}
		switch (state)
		{
			case AIState.Idle:
				_aiState = new IdleMonsterAIState(this);
				break;
			case AIState.Pursue:
				_aiState = new PursueMonsterAIState(this);
				break;
			case AIState.Attack:
				_aiState = new AttackMonsterAIState(this);
				break;
		}
	}
	
	public void Update(float delta)
	{
		if (_aiState == null)
		{
			return;
		}
		// if (Actor.DiedOnce)
		// {
		// 	return;
		// }
		if (Actor.CurrentActionState == Actor.ActionState.Dying)
		{
			return;
		}
		_aiState.Update(delta);
	}
	
}
