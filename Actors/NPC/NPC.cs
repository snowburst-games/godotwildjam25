using Godot;
using System;
using System.Collections.Generic;

public class NPC : Area2D
{
	public delegate void DialogueStartedDelegate(DialogueControl.Dialogue dialogue);
	public event DialogueStartedDelegate DialogueStarted;

	[Export]
	private DialogueControl.Dialogue _dialogue;

	[Export]
	private bool _facingRight = false;

	public int ID {get; set;} = -1;
	private bool _active = true;
	
	public bool Active 
	{
		get {
			return _active;
		}
		set {
			_active = value;
			GetNode<Label>("Label").Visible = _active;
		}
	}

	private Vector2 _gravity = new Vector2(0,1);
	private Vector2 _vel = new Vector2();
	private bool _onFloor = false;
	private AudioStreamPlayer2D _soundPlayer;
	public Dictionary<AudioData.NPC, AudioStream> NPCSounds {get; set;} = AudioData.LoadedSounds<AudioData.NPC>(AudioData.NPCSoundPaths);
	public override void _Ready()
	{
		base._Ready();
		_soundPlayer = GetNode<AudioStreamPlayer2D>("SoundPlayer");
		GetNode<Sprite>("Sprite").FlipH = _facingRight;
		
	}
	
	public override void _PhysicsProcess(float delta)
	{
		base._PhysicsProcess(delta);
		if (!_onFloor)
		{
			_vel += _gravity;
			Position = new Vector2(Position.x + _vel.x * delta, Position.y + _vel.y * delta);
		}
	}

	public void StartDialogue(float posX)
	{
		if (!Active)
		{
			return;
		}
		GetNode<Sprite>("Sprite").FlipH = posX > Position.x;
		AudioHandler.PlaySound(_soundPlayer, GetRandomInteractSound(), AudioData.SoundBus.Voice);
		DialogueStarted?.Invoke(_dialogue);
		GD.Print("Commencing dialogue with NPC ID ", ID);
	}


	public AudioStream GetRandomInteractSound()
	{
		return (new AudioStream[4] {NPCSounds[AudioData.NPC.Interact1], NPCSounds[AudioData.NPC.Interact2], NPCSounds[AudioData.NPC.Interact3]
			, NPCSounds[AudioData.NPC.Interact4]})
			[new Random().Next(0,4)];
	}

	private void OnNPCBodyEntered(Godot.Object body)
	{
		if (body is StaticBody2D staticBody)
		{
			if (staticBody.Name == "NPCStaticBody")
			{
				return;
			}
		}
		_onFloor = true;
	}

	private void OnDetectAreaBodyEntered(Godot.Object body)
	{
		if (!Active)
		{
			return;
		}
		if (body is PlayerBody player)
		{
			GetNode<AnimationPlayer>("Anim").Play("Start");
		}
	}

}
