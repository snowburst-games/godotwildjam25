using Godot;
using System;
using System.Collections.Generic;

// need to add healpots and manapots. after this need to implement experience and levelling up. then need to implement the worldphasing and the treasure that lets udo it

public class PlayerBody : KinematicBody2D, IActorBody
{
	public delegate void SpiritArtefactActivatedDelegate(bool Spirit, bool flavour);
	public event SpiritArtefactActivatedDelegate SpiritArtefactActivated;
	public delegate void PlayerDiedDelegate();
	public event PlayerDiedDelegate PlayerDied;
	public delegate void PlayerAnnouncedDelegate(HUD.Announcement announcement);
	public event PlayerAnnouncedDelegate PlayerAnnounced;
	public bool Spirit {get; set;} = false; // can only cast spells and see ghosts in spirit form! and in spirit form the world becomes greyyyyy
	public Actor Actor {get; set;}
	public ActorSpellManager SpellManager {get; set;}

	public delegate void TreasureCollectedDelegate(Treasure.TreasureType treasure);
	public event TreasureCollectedDelegate TreasureCollected;

	public List<RayCast2D> CollisionRays {get; set;}
	private AudioStreamPlayer2D _soundPlayer;
	private AudioStreamPlayer2D _spellSoundPlayer;
	private AudioStreamPlayer2D _itemSoundPlayer;
	public Dictionary<AudioData.Player, AudioStream> PlayerSounds {get; set;} = AudioData.LoadedSounds<AudioData.Player>(AudioData.PlayerSoundPaths);
	public Dictionary<AudioData.Spell, AudioStream> SpellSounds {get; set;} = AudioData.LoadedSounds<AudioData.Spell>(AudioData.SpellSoundPaths);

	public override void _Ready()
	{
		CollisionRays = new List<RayCast2D>() {
		GetNode<RayCast2D>("CollideDownRay"),
		GetNode<RayCast2D>("CollideUpRay"),
		GetNode<RayCast2D>("CollideLeftRay"),
		GetNode<RayCast2D>("CollideRightRay")
		};
		_soundPlayer = GetNode<AudioStreamPlayer2D>("SoundPlayer");
		_spellSoundPlayer = GetNode<AudioStreamPlayer2D>("SpellSoundPlayer");
		_itemSoundPlayer = GetNode<AudioStreamPlayer2D>("ItemSoundPlayer");
		// SpellManager = Actor.GetNode<ActorSpellManager>("SpellManager");
		// SpellManager._spellsKnown.Add(ActorSpellManager.Spell.FireBolt);
	}

	private int _currentSoundPriority = 0;
	public void PlaySound(AudioStream stream, AudioData.SoundBus bus, int priority)
	{
		// if (_soundPlayer.Playing && !interrupt)
		// {
		// 	return;
		// }
		if (!_soundPlayer.Playing)
		{
			_currentSoundPriority = 0;
		}
		if (priority < _currentSoundPriority)
		{
			return;
		}

		// GD.Print(_soundPlayer.Playing);
		if (_soundPlayer.Playing == false)
		{
			_soundPlayer.Stream = null;
		}
		if (stream == _soundPlayer.Stream)
		{
			return;
		}
		_currentSoundPriority = priority;
		AudioHandler.PlaySound(_soundPlayer, stream, bus);
	}

	public void PlaySpellSound(AudioStream stream)
	{
		AudioHandler.PlaySound(_spellSoundPlayer, stream, AudioData.SoundBus.Effects);
	}
	public void PlayItemSound(AudioStream stream)
	{
		AudioHandler.PlaySound(_itemSoundPlayer, stream, AudioData.SoundBus.Effects);
	}


	public void StopSound(AudioStream stream)
	{
		if (_soundPlayer.Stream == stream)
			_soundPlayer.Stop();

		// GD.Print("STOPPING SOND");
	}

	public void OnSpawn()
	{
		GetNode<AnimationPlayer>("EffectsAnim").Play("Spawn");
		GetNode<CPUParticles2D>("SpiritParticles").Emitting = false;
		GetNode<CPUParticles2D>("LevelUpParticles").Emitting = false;
		
		PlaySound(PlayerSounds[AudioData.Player.Spawn], AudioData.SoundBus.Effects, 10);
	}

	public AudioStream GetRandomHurtSound()
	{
		return (new AudioStream[3] {PlayerSounds[AudioData.Player.Hurt1], PlayerSounds[AudioData.Player.Hurt2], PlayerSounds[AudioData.Player.Hurt3]})
			[new Random().Next(0,3)];
	}
	public AudioStream GetRandomGulpSound()
	{
		return PlayerSounds[AudioData.Player.Gulp1];
		// return (new AudioStream[2] {PlayerSounds[AudioData.Player.Gulp1], PlayerSounds[AudioData.Player.Gulp2]})
		// 	[new Random().Next(0,2)];
	}

	public void Flip()
	{
		GetNode<RayCast2D>("InteractRay").CastTo = new Vector2(-GetNode<RayCast2D>("InteractRay").CastTo.x, GetNode<RayCast2D>("InteractRay").CastTo.y);

		// _sprite.FlipH = !_sprite.FlipH;
		if (Actor.FacingRight)
		{
			GetNode<Sprite>("Sprite").Offset = new Vector2(-50, 0);
		}
		else
		{
			GetNode<Sprite>("Sprite").Offset = new Vector2(50, 0);		
		}
	}

	public void StartBerserk(float power)
	{
		// not implemented. if to implement would need to put in another class.. refactor..
	}

	public override void _PhysicsProcess(float delta)
	{
		base._PhysicsProcess(delta);
		if (GetNode<RayCast2D>("InteractRay").IsColliding())
		{
					// GD.Print("TEST1");
			if (GetNode<RayCast2D>("InteractRay").GetCollider() is NPC npc)
			{
					// GD.Print("TEST2");
				if (Input.IsActionJustPressed("Interact"))
				{
					// GD.Print("TEST");
					npc.StartDialogue(Position.x*2);
					return;
				}
			}
			if (((Node)GetNode<RayCast2D>("InteractRay").GetCollider()).GetParent() is NPC npc2)
			{
				if (Input.IsActionJustPressed("Interact"))
				{
					// GD.Print("TEST");
					npc2.StartDialogue(Position.x*2);
					return;
				}
			}
		}
	}

	public void PickupTreasure(Treasure.TreasureType treasure)
	{
		switch (treasure)
		{
			case Treasure.TreasureType.Firebolt:
				Actor.GetNode<ActorSpellManager>("SpellManager").AddSpell(ActorSpellManager.Spell.FireBolt);
				break;
			case Treasure.TreasureType.Berserk:
				Actor.GetNode<ActorSpellManager>("SpellManager").AddSpell(ActorSpellManager.Spell.Berserk);
				break;
			case Treasure.TreasureType.Blink:
				Actor.GetNode<ActorSpellManager>("SpellManager").AddSpell(ActorSpellManager.Spell.Blink);
				break;
			case Treasure.TreasureType.Fireball:
				Actor.GetNode<ActorSpellManager>("SpellManager").AddSpell(ActorSpellManager.Spell.Fireball);
				break;
			case Treasure.TreasureType.Heal:
				Actor.GetNode<ActorSpellManager>("SpellManager").AddSpell(ActorSpellManager.Spell.Heal);
				break;
			// case Treasure.TreasureType.HealthPot:
			// 	HealPots += 1;
			// 	// UPDATE THE HUD 
			// 	break;
			// case Treasure.TreasureType.ManaPot:
			// 	ManaPots += 1;
			// 	// UPDATE THE HUD
			// 	break;
			case Treasure.TreasureType.SpiritArtefact:
				Actor.GetNode<ActorSpellManager>("SpellManager").AddSpell(ActorSpellManager.Spell.SpiritArtefact);
				break;
		}
		TreasureCollected?.Invoke(treasure);
		// PlaySound(PlayerSounds[AudioData.Player.GetItemBig], AudioData.SoundBus.Effects, 6);
		PlayItemSound(PlayerSounds[AudioData.Player.GetItemBig]);
	}

	

	public void OnLevelUp()
	{
		// play lvl up anim
		PlayerAnnounced?.Invoke(HUD.Announcement.LevelUp);
		GetNode<AnimationPlayer>("EffectsAnim").Play("LevelUp");
		PlaySound(PlayerSounds[AudioData.Player.LevelUp], AudioData.SoundBus.Effects, 11);
	}

	public void ActivateSpiritArtefact(bool on, bool flavour=true)
	{
		Spirit = on;
		GetNode<CPUParticles2D>("SpiritParticles").Emitting = on;
		SpiritArtefactActivated?.Invoke(on, flavour);
	}

	// private void AddTreasure()
	// {
	// 	Actor.GetNode<ActorSpellManager>("SpellManager").AddSpell(ActorSpellManager.Spell.FireBolt);
	// }

	public void UnsubscribeEvents()
	{
		SpiritArtefactActivated = null;
		TreasureCollected = null;
		PlayerDied = null;
		PlayerAnnounced = null;
	}

	public void OnDeath()
	{
		PlayerDied?.Invoke();
	}

	public void OnHealPotionPickup()
	{
		Actor.GetNode<ActorDamageHandler>("DamageHandler").ModHealthPots(1);//.HealPots += 1;
		TreasureCollected?.Invoke(Treasure.TreasureType.HealthPot);
		// PlaySound(PlayerSounds[AudioData.Player.GetItemSmall], AudioData.SoundBus.Effects, 6);
		PlayItemSound(PlayerSounds[AudioData.Player.GetItemSmall]);
	}

	public void OnManaPotionPickup()
	{
		
		Actor.GetNode<ActorDamageHandler>("DamageHandler").ModManaPots(1);// ManaPots += 1;
		TreasureCollected?.Invoke(Treasure.TreasureType.ManaPot);
		// PlaySound(PlayerSounds[AudioData.Player.GetItemSmall], AudioData.SoundBus.Effects, 6);
		PlayItemSound(PlayerSounds[AudioData.Player.GetItemSmall]);
	}
	public void OnPowerOrbsPickup(float num)
	{
		Actor.GetNode<ActorDamageHandler>("DamageHandler").ModPower(num);	
		TreasureCollected?.Invoke(Treasure.TreasureType.PowerOrbs);
		// PlaySound(PlayerSounds[AudioData.Player.GetItemSmall], AudioData.SoundBus.Effects, 6);
		PlayItemSound(PlayerSounds[AudioData.Player.GetItemSmall]);
	}

	public void OnExperienceOrbsPickup(float num)
	{
		Actor.ExperienceManager.UpdateExperience(num);
		TreasureCollected?.Invoke(Treasure.TreasureType.ExperienceOrbs);
		// PlaySound(PlayerSounds[AudioData.Player.GetItemSmall], AudioData.SoundBus.Effects, 6);
		PlayItemSound(PlayerSounds[AudioData.Player.GetItemSmall]);
	}


}
