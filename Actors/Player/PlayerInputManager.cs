using Godot;
using System;

public class PlayerInputManager : IActorInputManager
{
	public Actor Actor {get; set;}

	public Vector2 Target {get; set;}

	public bool MouseEnabled {get; set;} = true;
	public Vector2 PlayerTarget {get; set;}
	public delegate void BlinkHintedDelegate(Vector2 target, float distance, bool visible);
	public event BlinkHintedDelegate BlinkHinted;
	public PlayerInputManager()
	{

	}

	public PlayerInputManager(Actor actor)
	{
		this.Actor = actor;
	}

	public void Update(float delta)
	{

		if (Actor.CurrentActionState == Actor.ActionState.Dying)
		{
			return;
		}

		Target = Actor.GetGlobalMousePosition();


		BlinkHint(Target);


		Actor.TryRunLeft = Input.IsActionPressed("P1Left");
		Actor.TryRunRight = Input.IsActionPressed("P1Right");

		Actor.TryJump = Actor.GetNode<Timer>("Body/JumpBufferTimer").TimeLeft > 0;
		
		if (Actor.Gravity == 0)
		{
			Actor.TryMoveDown = Input.IsActionPressed("P1Down");
			Actor.TryMoveUp = Input.IsActionPressed("P1Up");
		}
		else
		{
			if (Input.IsActionPressed("Jump"))
			{
				Actor.GetNode<Timer>("Body/JumpBufferTimer").Start();
			}
		}
		Actor.TryCast = Input.IsActionPressed("Cast");

		if (!MouseEnabled && Actor.TryCast)
		{
			Actor.TryCast = false;
			// GD.Print("Mouse disabled?");
		}
		// GD.Print(Actor.GetNode<ActorSpellManager>("SpellManager").CurrentSpell.ToString());
		if ((Actor.GetNode<ActorSpellManager>("SpellManager").CurrentSpell == ActorSpellManager.Spell.Empty || 
			Actor.GetNode<ActorSpellManager>("SpellManager").CurrentSpell == ActorSpellManager.Spell.SpiritArtefact) && Actor.TryCast)
		{
			Actor.TryCast = false;
		}

		Actor.GetNode<ActorSpellManager>("SpellManager").HotKeyCastSpell = ActorSpellManager.Spell.Empty;
		// Actor.GetNode<ActorSpellManager>("SpellManager").HotKeyCastSpell = ActorSpellManager.Spell.Empty;

		if (Input.IsActionPressed("FireBoltSpell"))
		{
			TrySelectedSpellCast(ActorSpellManager.Spell.FireBolt);
		}
		if (Input.IsActionPressed("FireballSpell"))
		{
			TrySelectedSpellCast(ActorSpellManager.Spell.Fireball);
		}
		if (Input.IsActionPressed("HealSpell"))
		{
			TrySelectedSpellCast(ActorSpellManager.Spell.Heal);
		}
		if (Input.IsActionPressed("BerserkSpell"))
		{
			TrySelectedSpellCast(ActorSpellManager.Spell.Berserk);
		}
		if (Input.IsActionPressed("BlinkSpell"))
		{
			TrySelectedSpellCast(ActorSpellManager.Spell.Blink);
		}
		if (Input.IsActionPressed("ArtefactSpell"))
		{
			// TrySelectedSpellCast(ActorSpellManager.Spell.SpiritArtefact);
			OnArtefactUsed();
		}

	}


	public void BlinkHint(Vector2 target, float distance = 400f)
	{
		if (Actor.GetNode<ActorSpellManager>("SpellManager").CurrentSpell != ActorSpellManager.Spell.Blink)
		{
			BlinkHinted?.Invoke(target, distance, false);
			return;
		}

		BlinkHinted?.Invoke(target, distance, true);
	}

	public void OnArtefactUsed()
	{
		// TrySelectedSpellCast(ActorSpellManager.Spell.SpiritArtefact);
		Actor.GetNode<ActorSpellManager>("SpellManager").ArtefactCastSpell = ActorSpellManager.Spell.SpiritArtefact;
		Actor.TryCast = true;
	}

	// private ActorSpellManager.Spell _prevSpell = ActorSpellManager.Spell.Empty;

	public void TrySelectedSpellCast(ActorSpellManager.Spell spell)
	{
		if (!Actor.GetNode<ActorSpellManager>("SpellManager").GetKnownSpells().Contains(spell))
		{
			return;
		}
		Actor.GetNode<ActorSpellManager>("SpellManager").HotKeyCastSpell = spell;

		// // _prevSpell = Actor.GetNode<ActorSpellManager>("SpellManager").CurrentSpell;
		// Actor.GetNode<ActorSpellManager>("SpellManager").SetActiveSpell(spell, true);


		Actor.TryCast = true;
	}
	
	public bool CanCastSpirit(ActorSpellManager.Spell spell)
	{
		if (spell != ActorSpellManager.Spell.SpiritArtefact)
		{
			if (!Actor.GetNode<PlayerBody>("Body").Spirit)
			{
				return false;
			}
		}
		return true;
	}

	public bool CanAffordToCastSpell(ActorSpellManager.Spell spell)
	{
		return Actor.ActorStats["Mana"] >=  Actor.GetNode<ActorSpellManager>("SpellManager").GetSpellCost(spell);// SpellCosts[spell];//Actor.GetNode<ActorSpellManager>("SpellManager").SpellCosts[ Actor.GetNode<ActorSpellManager>("SpellManager")]
	}
}
