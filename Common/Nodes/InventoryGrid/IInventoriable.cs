using Godot;
using System;

public interface IInventoriable
{
	string ItemName
	{
		get;
		set;
	}

	InventoryItem InventoryItem
	{
		get;
		set;
	}

	void Init(InventoryItem inventoryItem);

	void Use();

	float Health
	{
		get;
		set;
	}

	event InventoryItem.ItemUsedDelegate ItemUsed;

}
