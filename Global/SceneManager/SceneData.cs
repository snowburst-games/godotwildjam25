// Scene data: contains references to all Stage scenes within the game - for simplicity so we don't have to update path in each script.

using System;

public class SceneData
{
    public enum Stage
    {
        MainMenu,
        World
    }

    public enum Scene
    {
        // NetState,
        // Lobby,
        // Player,
        // Creature
        MonsterBullet,
        NPC,
        ExperienceOrbs,
        HealthPotion,
        ManaPotion,
        PowerOrbs,
        BerserkTreasure,
        BlinkTreasure,
        FireballTreasure,
        FireboltTreasure,
        HealTreasure,
        SpiritArtefact
    }

    public readonly System.Collections.Generic.Dictionary<Stage,string> Stages 
        = new System.Collections.Generic.Dictionary<Stage, string>()
    {
        {Stage.MainMenu, "res://Stages/MainMenu/StageMainMenu.tscn"},
        {Stage.World, "res://Stages/World/StageWorld.tscn"}
    };

    public static System.Collections.Generic.Dictionary<Scene,string> Scenes 
        = new System.Collections.Generic.Dictionary<Scene, string>()
    {
        // {Scene.NetState, "res://Global/NetState/NetState.tscn"},
        // {Scene.Lobby, "res://Utils/Network/Lobby/Lobby.tscn"},
        // {Scene.Player, "res://Entities/Characters/Player/Player.tscn"},
        // {Scene.Creature, "res://Entities/Creature/Creature.tscn"},
        {Scene.MonsterBullet, "res://Props/Bullets/MonsterBullet/MonsterBullet.tscn"},
        {Scene.NPC, "res://Actors/NPC/NPC.tscn"},
        {Scene.ExperienceOrbs, "res://Props/Treasures/ExperienceOrbs/ExperienceOrbs.tscn"},
        {Scene.HealthPotion, "res://Props/Treasures/HealPotion/HealPotion.tscn"},
        {Scene.ManaPotion, "res://Props/Treasures/ManaPotion/ManaPotion.tscn"},
        {Scene.PowerOrbs, "res://Props/Treasures/PowerOrbs/PowerOrbs.tscn"},
        {Scene.BerserkTreasure, "res://Props/Treasures/BerserkTreasure/BerserkTreasure.tscn"},
        {Scene.BlinkTreasure, "res://Props/Treasures/BlinkTreasure/BlinkTreasure.tscn"},
        {Scene.FireballTreasure, "res://Props/Treasures/FireballTreasure/FireballTreasure.tscn"},
        {Scene.FireboltTreasure, "res://Props/Treasures/FireboltTreasure/FireboltTreasure.tscn"},
        {Scene.HealTreasure, "res://Props/Treasures/HealTreasure/HealTreasure.tscn"},
        {Scene.SpiritArtefact, "res://Props/Treasures/SpiritArtefactTreasure/SpiritArtefactTreasure.tscn"},
    };
}
