using Godot;
using System;

public class MonsterBullet : Area2D
{

	public float Damage {get; set;}
	public bool Berserk {get; set;} = false;
	public MonsterBody Originator {get; set;}

	public void Start()
	{
		
		GetNode<AnimationPlayer>("Anim").Play("Start");
	}
	private void OnAnimAnimationFinished(String animName)
	{
		if (animName == "Start")
		{
			QueueFree();
		}
	}
	private void OnMonsterBulletBodyEntered(Godot.Object body)
	{
		if (Berserk)
		{
			if (body is MonsterBody monster)
			{
				if (monster == Originator || monster.Level == MonsterBody.PowerLevel.Boss)
				{
					return;
				}
				// if (monster.Actor.CurrentActionState == Actor.ActionState.Dying)
				// {
				// 	return;
				// }
				monster.Actor.GetNode<ActorDamageHandler>("DamageHandler").TakeDamage(Damage);
				// player.TakeDamage(_damage);
			}
			return;
		}
		if (body is PlayerBody player)
		{
			// if (player.Actor.CurrentActionState == Actor.ActionState.Dying)
			// {
			// 	return;
			// }
			player.Actor.GetNode<ActorDamageHandler>("DamageHandler").TakeDamage(Damage);
			// player.TakeDamage(_damage);
		}
	}
}




