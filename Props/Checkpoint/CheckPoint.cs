using Godot;
using System;
using System.Collections.Generic;

public class CheckPoint : Area2D
{
	public delegate void CheckPointReachedDelegate(CheckPoint checkpoint);
	public event CheckPointReachedDelegate CheckPointReached;
	private AudioStreamPlayer2D _soundPlayer;
	public Dictionary<AudioData.Checkpoint, AudioStream> CheckpointSounds {get; set;} = AudioData.LoadedSounds<AudioData.Checkpoint>(AudioData.CheckPointSoundPaths);

	public override void _Ready()
	{
		base._Ready();
		_soundPlayer = GetNode<AudioStreamPlayer2D>("SoundPlayer");
	}

	private void OnBodyEntered(Godot.Object body)
	{
		if (body is PlayerBody player && GetNode<Timer>("Timer").TimeLeft == 0)
		{
			CheckPointReached?.Invoke(this);
		}
	}

	public void StartAnim()
	{
		AudioHandler.PlaySound(_soundPlayer, CheckpointSounds[AudioData.Checkpoint.Save], AudioData.SoundBus.Effects);
		// GetNode<AudioStreamPlayer2D>("SoundPlayer").Stream = (AudioStream) GD.Load("res://Props/Spells/SpiritArtefact/CastSpellGeneric.ogg");
		// GetNode<AudioStreamPlayer2D>("SoundPlayer").Play();
		GetNode<AnimationPlayer>("Anim").Play("Start");
	}

	private void OnBodyExited(Godot.Object body)
	{
		// Replace with function body.
	}

}

