using Godot;
using System;
using System.Collections.Generic;

public interface ISpellEffect
{
	void Start(Vector2 target, float power, float speed, List<WorldSpellManager.Effect> spellEffect, bool sourcePlayer, ActorSpellManager.Spell spell);
	void Init(float power);

	Vector2 Position {set; get;}
	float Cooldown {get; set;}
}
