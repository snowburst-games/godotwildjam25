using Godot;
using System;
using System.Collections.Generic;

public class SpellDamage : Area2D, ISpellEffect
{
	private bool _started = false;
	private Vector2 _target = new Vector2(-10000, -10000);
	private float _power = 0;
	private float _speed = 100;
	private Vector2 _velocity = new Vector2(0,0);
	private Vector2 _direction;

	public float Cooldown {get; set;} = 0.2f;
	private List<WorldSpellManager.Effect> _spellEffects;
	private bool _sourcePlayer = true;
	private AudioStreamPlayer2D _soundPlayer;
	public Dictionary<AudioData.Spell, AudioStream> SpellSounds {get; set;} = AudioData.LoadedSounds<AudioData.Spell>(AudioData.SpellSoundPaths);

	public override void _Ready()
	{
		if (HasNode("SoundPlayer"))
		{
			_soundPlayer = GetNode<AudioStreamPlayer2D>("SoundPlayer");
		}
	}

	public void Init(float power)
	{
		
	}
	public void Start(Vector2 target, float power, float speed,  List<WorldSpellManager.Effect> spellEffects, bool sourcePlayer, ActorSpellManager.Spell spell)
	{
		_sourcePlayer = sourcePlayer;
		if (_spellImpactSounds.ContainsKey(spell))
		{
			_spellSound = _spellImpactSounds[spell];
		}
		// if (!_sourcePlayer)
		// {
		// 	SetCollisionMaskBit(1, false);
		// }
		// else
		// {
		// 	SetCollisionMaskBit(0, false);
		// }
		_started = true;
		_target = target;
		_power = power;
		_speed = speed;
		_spellEffects = spellEffects;
		_direction = (_target - Position).Normalized();
		Position += _direction * 20f;
		LookAt(_target);
	}

	private float _distanceTravelled = 0;

	public override void _PhysicsProcess(float delta)
	{
		base._PhysicsProcess(delta);

		if (!_started)
		{
			return;
		}
		// GD.Print("source player? ", _sourcePlayer);
		// float xVel = Mathf.Lerp(_velocity.x, _speed * (_target - Position).Normalized().x, 1f);
		// float yVel = Mathf.Lerp(_velocity.y, _speed * (_target - Position).Normalized().y, 1f);

		// _velocity = new Vector2(xVel, yVel);

		Position += _direction*_speed*delta;
		_distanceTravelled += _direction.Length()*_speed*delta;
		
		if (_distanceTravelled >= 1000)
		{
			Die();
		}

		// Actor.Velocity = new Vector2(Mathf.Lerp(Actor.Velocity.x, Actor.Speed * Actor.MoveDir.x, 0.15f), Actor.YVel + (Actor.MoveDir.y * Actor.Speed));

		
	}
	private void OnSpellBodyEntered(Godot.Object body)
	{
		if (!_started)
		{
			return;
		}
		// GD.Print("source player? ", _sourcePlayer);
		if (body is PlayerBody p && !_sourcePlayer)
		{
			if (p.Actor.CurrentActionState == Actor.ActionState.Dying)
			{
				return;
			}
			
			foreach (WorldSpellManager.Effect effect in _spellEffects)
			{
				DoSpellEffect(effect, p);
			}

			Die();
			return;
		}
		if (body is MonsterBody m && _sourcePlayer)
		{
			if (m.Actor.CurrentActionState == Actor.ActionState.Dying)
			{
				return;
			}
			// GD.Print("TAKE DAMAGE MONSTER!");

			// if (_spellEffects.Contains)
			foreach (WorldSpellManager.Effect effect in _spellEffects)
			{
				DoSpellEffect(effect, m);
			}
	
			Die();
			return;
		}
		
		if (!(body is IActorBody))
		{
			Die();
		}
	}

	private AudioData.Spell _spellSound = AudioData.Spell.FireboltImpact;

	private Dictionary<ActorSpellManager.Spell, AudioData.Spell> _spellImpactSounds = new Dictionary<ActorSpellManager.Spell, AudioData.Spell>()
	{
		{ActorSpellManager.Spell.Berserk, AudioData.Spell.BerserkImpact},
		{ActorSpellManager.Spell.Fireball, AudioData.Spell.FireballImpact},
		{ActorSpellManager.Spell.FireBolt, AudioData.Spell.FireboltImpact}
	};

	private void DoSpellEffect(WorldSpellManager.Effect effect, IActorBody actorBody)
	{
		switch (effect)
		{
			case WorldSpellManager.Effect.Damage:
				actorBody.Actor.GetNode<ActorDamageHandler>("DamageHandler").TakeDamage(_power);
				break;
			case WorldSpellManager.Effect.Berserk:
				actorBody.StartBerserk(GetBerserkDuration(_power));// //actorBody.Actor.GetNode<ActorSpellManager>("SpellManager").GetBerserkDuration());
				break;
		}
	}

	public float GetBerserkDuration(float power)
	{
		return power/2f - 10;
	}

	private async void Die()
	{
		SetPhysicsProcess(false);
		GetNode<AnimationPlayer>("Anim").Play("Die");
		if (_soundPlayer != null)
		{
			AudioHandler.PlaySound(_soundPlayer, SpellSounds[_spellSound], AudioData.SoundBus.Effects);
		}

		await ToSignal (GetNode<AnimationPlayer>("Anim"), "animation_finished");

		QueueFree();
	}

}

