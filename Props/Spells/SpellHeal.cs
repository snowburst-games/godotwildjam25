using Godot;
using System;
using System.Collections.Generic;

public class SpellHeal : Area2D, ISpellEffect
{

	public delegate void LookedForCasterDelegate(SpellHeal spellHeal);
	public event LookedForCasterDelegate LookedForCaster;
	public Vector2 AnimTarget {get; set;}
	private Vector2 _target;

	private List<WorldSpellManager.Effect> _spellEffects;
	private float _power = 0;
	private bool _started = false;

	public float Cooldown {get; set;} = 0.2f;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		
	}

	public void Init(float power)
	{

		// _power = power;
	}

	public void Start(Vector2 target, float power, float speed, List<WorldSpellManager.Effect> spellEffects, bool sourcePlayer, ActorSpellManager.Spell spell)
	{
		_spellEffects = spellEffects;
		// _started = true;
		_target = target;
		_power = power;
		// if (_spellEffects.Contains(WorldSpellManager.Effect.Blink))
		// {
		// 	Cooldown /= power;
		// }
		// _speed = speed;
		// _direction = (_target - Position).Normalized();
		// Position += _direction * 20f;
		// LookAt(_target);
	}


	public override void _PhysicsProcess(float delta)
	{
		base._PhysicsProcess(delta);

		LookedForCaster?.Invoke(this);

		Position = AnimTarget;

		// if (!_started)
		// {
		// 	return;
		// }		
	}

	private void OnSpellBodyEntered(object body)
	{
		if (_started)
		{
			return;
		}

		if (body is PlayerBody p)
		{
			_started = true;
			foreach (WorldSpellManager.Effect effect in _spellEffects)
			{
				DoSpellEffect(effect, p);
			}
			Die();
			return;
		}
	}

	// 		// if (_spellEffects.Contains)
	// 		foreach (WorldSpellManager.Effect effect in _spellEffects)
	// 		{
	// 			DoSpellEffect(effect, m);
	// 		}

	// 		Die();
	// 		return;
	// 	}
		
	// 	else
	// 	{
	// 		Die();
	// 	}
	// }

	private void DoSpellEffect(WorldSpellManager.Effect effect, PlayerBody p)
	{
		switch (effect)
		{
			case WorldSpellManager.Effect.Heal:
				p.Actor.GetNode<ActorDamageHandler>("DamageHandler").ModHealth(_power);// TakeDamage(-_power);
				break;
			case WorldSpellManager.Effect.Blink:
				p.Actor.Blink(_target);
				break;
			case WorldSpellManager.Effect.Spirit:
				p.ActivateSpiritArtefact(!p.Spirit);
				break;
			
			
		}
	}

	private async void Die()
	{
		// SetPhysicsProcess(false);
		GetNode<AnimationPlayer>("Anim").Play("Die");

		await ToSignal (GetNode<AnimationPlayer>("Anim"), "animation_finished");

		QueueFree();
	}
}

