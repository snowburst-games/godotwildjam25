using Godot;
using System;

public class ExperienceOrbs : Area2D, IPickupable
{
	// public delegate void LookedForPlayerDelegate(ExperienceOrbs orbs);
	// public event LookedForPlayerDelegate LookedForPlayer;
	// private bool _started = false;
	// public Vector2 Target = new Vector2(-10000, -10000);
	// private float _speed = 500;
	// private Vector2 _velocity = new Vector2(0,0);
	// private Vector2 _direction;
	public float ExperienceValue {get; set;} = 0;

	public PickupBehaviour PickupBehaviour {get; set;}


	public AnimationPlayer Anim {get; set;}
	// public ExperienceDropped

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{


		// Start();
	}

	public void Init() // runs before ready
	{
		Anim = GetNode<AnimationPlayer>("Anim");
		PickupBehaviour = new PickupBehaviour(this);
		PickupBehaviour.Start();
		// GD.Print("Running init");
	}

	// public void Start()
	// {
	// 	_started = true;
	// 	GetNode<AnimationPlayer>("Anim").Play("Start");
	// 	// ExperienceValue = experiencevalue;
	// 	GetNode<CPUParticles2D>("CPUParticles2D").Amount = (int)Math.Ceiling(ExperienceValue)*2;
	// 	_direction = (Target - Position).Normalized();
	// 	Position += _direction * 20f;
	// 	LookAt(Target);
	// }

	public override void _PhysicsProcess(float delta)
	{
		base._PhysicsProcess(delta);
		if (PickupBehaviour == null)
		{
			return;
		}
		PickupBehaviour.Update(delta);
		// if (!_started)
		// {
		// 	return;
		// }

		// LookedForPlayer?.Invoke(this);
		// _direction = (Target - Position).Normalized();
		// LookAt(Target);

		// if (Position.DistanceTo(Target) < 150)
		// {
		// 	Position += _direction*_speed*delta;
		// }
	}

	public void SetPickupPaused(bool paused)
	{
		PickupBehaviour.Paused = paused;
		// GD.Print("Pausing behaviour");
	}

	private void OnExperienceOrbsBodyEntered(Godot.Object body)
	{
		if (PickupBehaviour == null)
		{
			// GD.Print("Pickup is null so not activating");
			return;
		}
		if (PickupBehaviour.Paused)
		{
			// GD.Print("Pickup is paused so not activating");
			return;
		}
		if (body is PlayerBody player)
		{
			player.OnExperienceOrbsPickup(ExperienceValue);
			PickupBehaviour.Die();
		}
	}

	// private async void Die()
	// {
	// 	GetNode<AnimationPlayer>("Anim").Play("Die");

	// 	await ToSignal(GetNode<AnimationPlayer>("Anim"), "animation_finished");

	// 	QueueFree();
	// }

}
