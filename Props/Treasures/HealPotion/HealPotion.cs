using Godot;
using System;

public class HealPotion : Area2D, IPickupable
{
	public PickupBehaviour PickupBehaviour {get; set;}

	public Timer IdleTimer {get; set;}
	public AnimationPlayer Anim {get; set;}
	public override void _Ready()
	{
		IdleTimer = GetNode<Timer>("Timer");
		IdleTimer.WaitTime = Math.Max(0.01f,new Random().Next(0,20)/40f);
		IdleTimer.Start();
	}

	private void OnTimerTimeout()
	{
		GetNode<AnimationPlayer>("AnimIdle").Play("Idle");
	}


	public void Init() // runs before ready
	{
		Anim = GetNode<AnimationPlayer>("Anim");
		PickupBehaviour = new PickupBehaviour(this);
		PickupBehaviour.Speed = 1000;
		PickupBehaviour.AttractDistance = 100;
		PickupBehaviour.Start();
	}

	public void SetPickupPaused(bool paused)
	{
		PickupBehaviour.Paused = paused;
	}
	public override void _PhysicsProcess(float delta)
	{
		base._PhysicsProcess(delta);

		if (PickupBehaviour == null)
		{
			return;
		}
		PickupBehaviour.Update(delta);
	}
	private void OnPotionBodyEntered(object body)
	{
		if (PickupBehaviour == null)
		{
			// GD.Print("Pickup is null so not activating");
			return;
		}
		if (PickupBehaviour.Paused)
		{
			// GD.Print("Pickup is paused so not activating");
			return;
		}
		if (body is PlayerBody player)
		{
			player.OnHealPotionPickup();
			PickupBehaviour.Die();
		}
	}

}

