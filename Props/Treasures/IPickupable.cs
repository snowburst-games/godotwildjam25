using Godot;
using System;

public interface IPickupable
{
	AnimationPlayer Anim {get; set;}
	Vector2 Position {get; set;}

	void LookAt(Vector2 target);

	void QueueFree();
	void Init();
	PickupBehaviour PickupBehaviour {get; set;}

	void SetPickupPaused(bool paused);
}
