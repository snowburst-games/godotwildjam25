using Godot;
using System;

public class PickupBehaviour : Reference
{
	public PickupBehaviour()
	{

	}

	private IPickupable _pickupable;

	public PickupBehaviour(IPickupable pickupable)
	{
		_pickupable = pickupable;
	}


	public delegate void LookedForPlayerDelegate(PickupBehaviour pickupbehaviour);
	public event LookedForPlayerDelegate LookedForPlayer;
	private bool _started = false;
	public bool Paused {get; set;} = false;
	public Vector2 Target = new Vector2(-10000, -10000);
	public float Speed {get; set;} = 500;
	public float AttractDistance {get; set;} = 150;
	private Vector2 _velocity = new Vector2(0,0);
	private Vector2 _direction;
	// public float ExperienceValue {get; set;} = 0;

	public enum Item { HealthPot, ManaPot, ExperienceOrbs, PowerOrbs}

	public void Start()
	{
		_started = true;
		_pickupable.Anim.Play("Start");
		// _pickupable.Anim.
		// GetNode<CPUParticles2D>("CPUParticles2D").Amount = (int)Math.Ceiling(ExperienceValue)*2;
		_direction = (Target - _pickupable.Position).Normalized();
		_pickupable.Position += _direction * 20f;
		// _pickupable.LookAt(Target);
	}

	public void Update(float delta)
	{
		// GD.Print(Paused);
		if (!_started || Paused)
		{
			return;
		}

		LookedForPlayer?.Invoke(this);
		_direction = (Target - _pickupable.Position).Normalized();
		// _pickupable.LookAt(Target);

		if (_pickupable.Position.DistanceTo(Target) < AttractDistance)
		{
			_pickupable.Position += _direction*Speed*delta;
		}
	}

	public async void Die()
	{
		_pickupable.Anim.Play("Die");

		await ToSignal(_pickupable.Anim, "animation_finished");

		_pickupable.QueueFree();
	}
}
