using Godot;
using System;

public class PowerOrbs : Area2D, IPickupable
{
	public PickupBehaviour PickupBehaviour {get; set;}

	public float PowerValue {get; set;} = 0;
	public AnimationPlayer Anim {get; set;}
	public override void _Ready()
	{

		// if (PowerValue == 0)
		// {
		// 	Init(1);
		// }
	}

	public void Init()
	{

	}

	public void Init(float powervalue) // runs before ready
	{
		PowerValue = powervalue;
		Anim = GetNode<AnimationPlayer>("Anim");
		PickupBehaviour = new PickupBehaviour(this);
		PickupBehaviour.Start();
	}

	public void SetPickupPaused(bool paused)
	{
		PickupBehaviour.Paused = paused;
	}
	public override void _PhysicsProcess(float delta)
	{
		base._PhysicsProcess(delta);
		if (PickupBehaviour == null)
		{
			return;
		}
		PickupBehaviour.Update(delta);
	}

	private void OnBodyEntered(object body)
	{
		if (PickupBehaviour == null)
		{
			// GD.Print("Pickup is null so not activating");
			return;
		}
		if (PickupBehaviour.Paused)
		{
			// GD.Print("Pickup is paused so not activating");
			return;
		}
		if (PickupBehaviour == null)
		{
			return;
		}
		if (body is PlayerBody player)
		{
			player.OnPowerOrbsPickup(PowerValue);
			// GD.Print("power modified by num", PowerValue);
			PickupBehaviour.Die();
		}
	}
}


