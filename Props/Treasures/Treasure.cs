using Godot;
using System;

public class Treasure : Area2D
{
	public enum TreasureType {
		Firebolt,
		Fireball,
		Blink,
		Berserk,
		ManaPot,
		HealthPot,
		Heal,
		SpiritArtefact,
		ExperienceOrbs,
		PowerOrbs
	}

	private bool _collected = false;

	[Export]
	private TreasureType _treasureType;
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";

	public void SetTreasureType(TreasureType treasureType)
	{
		_treasureType = treasureType;
	}

	public TreasureType GetTreasureType()
	{
		return _treasureType;
	}

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		
	}

	private void OnTreasureBodyEntered(Godot.Object body)
	{
		if (_collected)
		{
			return;
		}
		if (body is PlayerBody p)
		{
			p.PickupTreasure(_treasureType);
			_collected = true;
			Die();
		}
	}


	private async void Die()
	{
		GetNode<AnimationPlayer>("Anim").Play("Die");
		await ToSignal(GetNode<AnimationPlayer>("Anim"), "animation_finished");

		QueueFree();
	}

}
