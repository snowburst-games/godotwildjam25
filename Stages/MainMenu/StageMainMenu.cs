// Stage menu: simple menu script connecting local and networked game signals to our scene manager.
using Godot;
using System;
using System.Collections.Generic;

public class StageMainMenu : Stage
{
	private AudioStreamPlayer _musicPlayer;
	private Dictionary<AudioData.MainMenuSounds, AudioStream> _mainMenuSounds = AudioData.LoadedSounds<AudioData.MainMenuSounds>(AudioData.MainMenuSoundPaths);
	private PictureStory _pictureStory;
	private Panel _popAbout;
	// If 2D positional sound is required then add a AudioStreamPlayer2D node as a child and:
	// AudioStreamPlayer2D soundPlayer = (AudioStreamPlayer2D) GetNode("SoundPlayer");
	// // Play sound
	// AudioHandler.PlaySound(soundPlayer, playerSounds[AudioData.PlayerSounds.Test], AudioData.SoundBus.Effects);

	public override void _Ready()
	{
		base._Ready();
		_popAbout = GetNode<Panel>("PopAbout");
		_musicPlayer = GetNode<AudioStreamPlayer>("MusicPlayer");
		_pictureStory = GetNode<PictureStory>("PictureStory");
		AudioHandler.PlaySound(_musicPlayer, _mainMenuSounds[AudioData.MainMenuSounds.Music], AudioData.SoundBus.Music);
		_popAbout.Visible = false;
		// Load settings at start if they exist. This should run at startup so if another scene is set at startup put this there.
		// GameSettings.Instance.LoadFromJSON();

	}
	private void Intro()
	{
		_pictureStory.Reset();
		_pictureStory.PictureStoryFinished+=this.OnIntroFinished;
		_pictureStory.FadeOutOnFinish = false;
		_pictureStory.VisibleOnFinish = true;
		_pictureStory.AddScreen("res://Stages/MainMenu/PictureStory/Intro_screen.png", 
		@"20th of September 1745, a Sunday
Lenore had returned from her year-long journey to find her home town derelict. Everyone she ever knew was gone...");
		_pictureStory.Start();
	}

	private void OnIntroFinished()
	{
		SceneManager.SimpleChangeScene(SceneData.Stage.World, new Dictionary<string, object>() {{"Difficulty", _selectedDifficulty}});
	}

	private void OnBtnPlayPressed()
	{
		// Intro();
		GetNode<Panel>("PopAbout").Visible = true;
		GetNode<Control>("PopAbout/CntDifficulty").Visible = true;
		GetNode<Control>("PopAbout/CntAbout").Visible = false;
	}

	private int _selectedDifficulty = 0;

	private void OnBtnDifficultyPressed(int difficulty) // baby 0 hard 3
	{
		_selectedDifficulty = difficulty;
		Intro();
	}

	private void OnBtnAboutPressed()
	{
		GetNode<Panel>("PopAbout").Visible = true;
		GetNode<Control>("PopAbout/CntAbout").Visible = true;
		GetNode<Control>("PopAbout/CntDifficulty").Visible = false;
	}

	private void OnBtnHowToPressed()
	{
		GetNode<Panel>("PopAbout").Visible = true;
		GetNode<Control>("PopAbout/CntAbout").Visible = false;
		GetNode<Control>("PopAbout/CntDifficulty").Visible = false;
	}

	private void OnBtnBackPressed()
	{
		GetNode<Panel>("PopAbout").Visible = false;
	}

	public override void _Input(InputEvent ev)
	{
		if (_popAbout.Visible && ev is InputEventMouseButton evMouseButton && ev.IsPressed())
		{
			if (! (evMouseButton.Position.x > _popAbout.RectGlobalPosition.x && evMouseButton.Position.x < _popAbout.RectSize.x + _popAbout.RectGlobalPosition.x
			&& evMouseButton.Position.y > _popAbout.RectGlobalPosition.y && evMouseButton.Position.y < _popAbout.RectSize.y + _popAbout.RectGlobalPosition.y) )
			{
				GD.Print("CLICKED OUTSIDE ABOUT");
				OnBtnBackPressed();
			}
		}
	}

	// private void _on_BtnLocalGame_pressed()
	// {   
	// 	SceneManager.SimpleChangeScene(SceneData.Stage.Game);  
	// 	// SceneManager.AnimatedChangeScene(SceneData.Stage.Game);   
	// 	// SceneManager.SimpleChangeScene(SceneData.Stage.Game, new Dictionary<string, object>() {
	// 	// 		{"gameConnectedState", StageGame.Connected.Local}
	// 	// 	 });
	// }

	// private void 


	// private void _on_BtnNetworkedGame_pressed()
	// {
	// 	SceneManager.SimpleChangeScene(SceneData.Stage.Multiplayer);
	// }
	
	// private void _on_BtnScores_pressed()
	// {
	// 	SceneManager.SimpleChangeScene(SceneData.Stage.Scores);
	// }

	// private void _on_BtnOptions_pressed()
	// {
	// 	SceneManager.SimpleChangeScene(SceneData.Stage.Options);
	// }

	// private void OnBtnOnePlayerPressed()
	// {
	// 	SceneManager.SimpleChangeScene(SceneData.Stage.World, new Dictionary<string, object>() {
	// 			{"GameMode", 1}
	// 		 });
	// }


	// private void OnBtnTwoPlayersPressed()
	// {
	// 	SceneManager.SimpleChangeScene(SceneData.Stage.World, new Dictionary<string, object>() {
	// 			{"GameMode", 2}
	// 		 });
	// }


	private void _on_BtnQuit_pressed()
	{
		// ToggleMute();
		// OS.WindowFullscreen = false;
		//OS.window_fullscreen = !OS.window_fullscreen
		// SceneManager.AnimatedChangeScene(SceneData.Stage.IntensiveTest, persist:true, sharedData:new Dictionary<string, object>() {{"ID", 1}});
		GetTree().Quit();
	}

	private void ToggleMute()
	{
		bool muted = AudioServer.IsBusMute(AudioServer.GetBusIndex("Master"));
		AudioServer.SetBusMute(AudioServer.GetBusIndex("Master"), !muted);
		string label = "Unmute";
		if (muted)
		{
			label = "Mute";
		}
		GetNode<Button>("BtnToggleMute").Text = label;
	}
}


