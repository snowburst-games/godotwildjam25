using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class CheckpointData
{
	private Dictionary<string, float> _playerStats;
	private List<ActorSpellManager.Spell> _playerKnownSpells;
	Dictionary<ActorSpellManager.Spell, int> _playerSpellLevels;
	private Vector2 _playerPos;
	private Dictionary<DialogueControl.Dialogue, bool> _dialogueHistory;// = new Dictionary<DialogueControl.Dialogue, bool>();

	private List<Tuple<Vector2, MonsterBody.PowerLevel>> _allMonsterData;
	private List<Tuple<Vector2, Treasure.TreasureType>> _allTreasureData;
	private List<Tuple<Vector2, bool>> _allPotionData;

	private List<Tuple<Vector2, float>> _allPowerOrbsData;
	private List<Tuple<Vector2, float>> _allExperienceOrbsData;
	private List<Vector2> _spiritArtefactData = new List<Vector2>();

	public CheckpointData()
	{

	}
	public void SetPlayerData(Dictionary<string, float> playerStats,  List<ActorSpellManager.Spell> playerKnownSpells, 
		Dictionary<ActorSpellManager.Spell, int> playerSpellLevels, Vector2 playerPos)
	{
		_playerStats = playerStats;
		_playerKnownSpells = playerKnownSpells.ToList();
		_playerSpellLevels = new Dictionary<ActorSpellManager.Spell, int>();
		foreach (KeyValuePair<ActorSpellManager.Spell, int> kv in playerSpellLevels)
		{
			_playerSpellLevels.Add(kv.Key, kv.Value);
		}
		_playerPos = playerPos;
	}

	public void SetPlayerFromData(Actor player)
	{
		player.ActorStats = _playerStats;
		player.GetNode<ActorDamageHandler>("DamageHandler").HealPots = (int) _playerStats["HealPots"];
		player.GetNode<ActorDamageHandler>("DamageHandler").ManaPots = (int) _playerStats["ManaPots"];

		player.ExperienceManager.SetExperience(_playerStats["Experience"]);
		player.ExperienceManager.Level = (int) _playerStats["Level"];        
		player.ExperienceManager.Skillpoints = (int) _playerStats["SkillPoints"];
		player.GetNode<ActorSpellManager>("SpellManager").SetKnownSpells(_playerKnownSpells.ToList());
		player.GetNode<ActorSpellManager>("SpellManager").SetSpellLevels(_playerSpellLevels);
		player.GetNode<ActorSpellManager>("SpellManager").SetBlinkCost();;
		player.Position = _playerPos;

		player.ExperienceManager.OnStatsChanged();
	}

	public void SetMonsterData(List<Tuple<Vector2, MonsterBody.PowerLevel>> allMonsterData)
	{
		_allMonsterData = allMonsterData;
	}

	public List<Tuple<Vector2, MonsterBody.PowerLevel>> GetMonsterData()
	{
		return _allMonsterData;
	}
	public void SetTreasureData(List<Tuple<Vector2, Treasure.TreasureType>> allTreasureData)
	{
		_allTreasureData = allTreasureData;
	}

	public void SetArtefact(Vector2 pos)
	{
		_spiritArtefactData = new List<Vector2>() {pos};
	}

	public List<Vector2> GetArtefactData()
	{
		return _spiritArtefactData.ToList();
	}

	public List<Tuple<Vector2, Treasure.TreasureType>> GetTreasureData()
	{
		return _allTreasureData;
	}

	public void SetPotionData(List<Tuple<Vector2, bool>> allPotionData)
	{
		_allPotionData = allPotionData.ToList();
	}

	public List<Tuple<Vector2, bool>> GetPotionData()
	{
		return _allPotionData.ToList();
	}

	public void SetPowerOrbsData(List<Tuple<Vector2, float>> allPowerOrbsData)
	{
		_allPowerOrbsData = allPowerOrbsData.ToList();
	}
	public List<Tuple<Vector2, float>> GetPowerOrbsData()
	{
		return _allPowerOrbsData.ToList();
	}
	public void SetExpOrbsData(List<Tuple<Vector2, float>> allExpOrbsData)
	{
		_allExperienceOrbsData = allExpOrbsData.ToList();
	}
	public List<Tuple<Vector2, float>> GetExpOrbsData()
	{
		return _allExperienceOrbsData.ToList();
	}

	public void SetDialogueHistory(Dictionary<DialogueControl.Dialogue, bool> dialogueHistory)
	{
		_dialogueHistory = dialogueHistory;
	}

	public Dictionary<DialogueControl.Dialogue, bool> GetDialogueHistory()
	{
		return _dialogueHistory.ToDictionary(entry => entry.Key,
											   entry => entry.Value);
	}

	
}
