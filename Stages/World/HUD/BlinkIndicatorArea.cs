using Godot;
using System;

public class BlinkIndicatorArea : Area2D
{
	private Vector2 _originalPos;
	private Vector2 _targetPos;
	private Vector2 _direction;
	private float _distance;
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		
	}

	public void Init(Vector2 originalPos, Vector2 targetPos, float distance)
	{
		_originalPos = originalPos;
		// Position = originalPos;
		_direction = (targetPos - originalPos).Normalized();
		_targetPos = targetPos;
		// if (_distance )
		// _distance = distance;
	}

	public override void _PhysicsProcess(float delta)
	{
		base._PhysicsProcess(delta);
		
	}

	

	public void UpdatePosition()
	{
		if (_distance <= 0)
		{
			return;
		}
		Position = _originalPos + (_direction*_distance);
	}

	private void OnBodyEntered(Godot.Object body)
	{
		// Position -= _direction*_distance;
		_distance -= 75;
		// UpdatePosition();
	}

}

