using Godot;
using System;
using System.Collections.Generic;


public class DialogueControl : Control
{

	public enum Dialogue {
		First,
		Second,
		Third,
		Fourth,
		Fifth,
		Sixth,
		Seventh,
		Eighth,
		Ninth
	}

	//public Dictionary<string, string> dialogDict;
	//public Dictionary<int, string> dialogLongD;
	public int DialogIndex = 0;
	public int DialogSize;
	public bool Finished = false;
	RichTextLabel TextLabel; //TextEdit
	RichTextLabel NameLabel; //NameDisplay
	Tween TxtTween;
	Sprite NextArrow;
//	PackedScene dialogResponse;
//	VBoxContainer vBoxContainer;
//	string CurrentConvo;
	public Dictionary<int, string> FirstDict = new Dictionary<int, string>();
	public Dictionary<int, string> SecondDict = new Dictionary<int, string>();
	public Dictionary<int, string> ThirdDict = new Dictionary<int, string>();
	public Dictionary<int, string> FourthDict = new Dictionary<int, string>();
	public Dictionary<int, string> FifthDict = new Dictionary<int, string>();
	public Dictionary<int, string> SixthDict = new Dictionary<int, string>();
	public Dictionary<int, string> SeventhDict = new Dictionary<int, string>();
	public Dictionary<int, string> EighthDict = new Dictionary<int, string>();
	public Dictionary<int, string> NinthDict = new Dictionary<int, string>();
	public Dictionary<Dialogue, Dictionary<int, string>> DialogDict = new Dictionary<Dialogue, Dictionary<int, string>>();
	Dialogue DialogEnum;
	public Dictionary<Dialogue, bool> DialogHistory = new Dictionary<Dialogue, bool>();
	public List<string> Goodbyes = new List<string>();
	public bool Talking = false;

	Random random = new Random();

	public Dictionary<Dialogue, string> NameDict = new Dictionary<Dialogue, string>()
	{
		{Dialogue.First, "Crusty Rusty"},
		{Dialogue.Second, "Jessie Stargazer"},
		{Dialogue.Third, "John the Jeweller"},
		{Dialogue.Fourth, "Aunt Imagic"},
		{Dialogue.Fifth, "Jane's uncle"},
		{Dialogue.Sixth, "Ollie Opportunist"},
		{Dialogue.Seventh, "Bob the Bard"},
		{Dialogue.Eighth, "Neville's pal"},
		{Dialogue.Ninth, "Ali the Adventurer"}
	};

	public void MakeFirstDict() //Lenore has come back from her travels, something happen
	{
		FirstDict.Add(0, "Lenore? You’re the first fresh blood I’ve seen all year!");
		FirstDict.Add(1, "A fine time to come back home...");
		FirstDict.Add (2, "Everybody’s a corpse except good old Crusty Rusty!");
		FirstDict.Add (3, "Have a look around. Someone might even tell you what happened here...");
	}
	public void MakeSecondDict() //Lesson/Quest: There is a spirit world. Find your father.
	{
		SecondDict.Add (0, "Look up! They're not stars you know. They're spirits.");
		SecondDict.Add (1, "Hundreds of eyes peering through the darkness");
		SecondDict.Add (2, "Your poor father went to the Spirit World to banish them...");
		SecondDict.Add (3, "...but he never came back...");
		SecondDict.Add (4, "Find him, Lenore!");
		//2. Damn!   you’ve got the artefact? Use it to help him! You can cast spells there you know...
	}
	public void MakeThirdDict() //Lesson: introduction to Spirit Pendant
	{
		ThirdDict.Add(0, "Another adventurer, eh? Want to save us all, do you?");
		ThirdDict.Add(1, "Think you can banish the ghosts and reclaim our town?!");
		ThirdDict.Add (2, "To go to the spirit world you have to wear the Spirit Pendant.");
		ThirdDict.Add (3, "Find it... but be careful. The Pendant changes you...");
		ThirdDict.Add (4, "..transforms you into a spirit, but your soul dies a little death each time");
		ThirdDict.Add (5, "Oh, and find some spell scrolls before you use it!");
	}

	public void MakeFourthDict() //Lesson: introduction to spells
	{
		FourthDict.Add(0, "Have you seen them? The sickening scrolls all about town!?");
		FourthDict.Add(1, "You'd better pick them up if you want to fight those ghosts.");
		FourthDict.Add (2, "You have to collect the magic scrolls in the real world...");
		FourthDict.Add (3, "...and you can only cast spells in the Spirit World. Obviously.");
	}

	public void MakeFifthDict() //Lesson: can't take off the Pendant with ghosts chasing you
	{
		FifthDict.Add(0, "Oh, Lenore! I've been looking for you!");
		FifthDict.Add(1, "Don't go to the other world! It'll kill you like it did my Jane.");
		FifthDict.Add(2, "She thought she could nip in and out...then the ghosts started chasing her.");
		FifthDict.Add(3, "Jane coudn't take the Pendant off until she killed them...");
		FifthDict.Add(4, "My poor Jane died... My poor Jane!");
	}

	public void MakeSixthDict() //Lesson: Find out about the spells
	{
		SixthDict.Add(0, "Want to buy a spell scroll to use in the Spirit World?");
		SixthDict.Add(1, "Firebolt? - Fire a ball of flames at your enemies.");
		SixthDict.Add(2, "Berserk? - Find two enemies, cast the spell to turn them against eachother.");
		SixthDict.Add(3, "Heal? - Restore some health.");
		SixthDict.Add(4, "Blink? - Teleport to a place of your choosing.");
		SixthDict.Add(5, "Hang on! You aint got no money, yer layabout!");
	}

	public void MakeSeventhDict() //Lesson: Story- start with this??? 1.
	{
		SeventhDict.Add(0, "There was once a lass named Lenore...");
		SeventhDict.Add(1, "Who went on a trip to the shore...");
		SeventhDict.Add(2, "But while she was trekking...");
		SeventhDict.Add(3, "The demons came wrecking...");
		SeventhDict.Add(4, "And the townsfolk she knew were no more!");
	}

	public void MakeEighthDict() //Lesson: Find out that father sold soul
	{
		EighthDict.Add(0, "You once had a father called Neville...");
		EighthDict.Add(1, "Who sold his soul to the devil..");
		EighthDict.Add(2, "He asked for endless life...");
		EighthDict.Add(3, "and a resurrected wife...");
		EighthDict.Add(4, "Instead he got stuck on this level");
	}

	public void MakeNinthDict() //Lesson:asdasdas
	{
		NinthDict.Add(0, "This town is full of spell scrolls!");
		NinthDict.Add(1, "But you can only find them in the real world...");
		NinthDict.Add(2, "... and you can only use them in the Spirit World...");
	}


	public void MakeDialogDict()
	{
		DialogDict.Add(Dialogue.First, FirstDict);
		DialogDict.Add(Dialogue.Second, SecondDict);
		DialogDict.Add(Dialogue.Third, ThirdDict);
		DialogDict.Add(Dialogue.Fourth, FourthDict);
		DialogDict.Add(Dialogue.Fifth, FifthDict);
		DialogDict.Add(Dialogue.Sixth, SixthDict);
		DialogDict.Add(Dialogue.Seventh, SeventhDict);
		DialogDict.Add(Dialogue.Eighth, EighthDict);
		DialogDict.Add(Dialogue.Ninth, NinthDict);
	}

	public void MakeDialogHistory()
	{
		DialogHistory.Add(Dialogue.First, false);
		DialogHistory.Add(Dialogue.Second, false);
		DialogHistory.Add(Dialogue.Third, false);
		DialogHistory.Add(Dialogue.Fourth, false);
		DialogHistory.Add(Dialogue.Fifth, false);
		DialogHistory.Add(Dialogue.Sixth, false);
		DialogHistory.Add(Dialogue.Seventh, false);
		DialogHistory.Add(Dialogue.Eighth, false);
		DialogHistory.Add(Dialogue.Ninth, false);
	}
	// this is called whenever the dialogue is started
	public void Start(Dialogue dialogue)
	{
		if (DialogHistory[dialogue]==false)
		{
			DialogIndex = 0;
			Talking = true;
			LoadLongDialog(dialogue); //Start showing the text or load the first conversation here
		}
		else 
		{
			//DialogIndex = 0;
			Talking = true;
			NPCHasNothingToSay(dialogue);
			GD.Print("We've had this convo before etc");
		}
			
	}

	public void NPCHasNothingToSay(Dialogue dialogue)
	{
			this.Show();
			Pause();
			Finished = false;
			Talking = true;
			NameLabel.BbcodeText = NameDict[dialogue];
			int index = random.Next(0, Goodbyes.Count);
			TextLabel.BbcodeText = Goodbyes[index];
		//	TextLabel.BbcodeText = "BYE"; //dialogfirst is the thing that will change
			TextLabel.PercentVisible = 0;
			TxtTween.InterpolateProperty(TextLabel,"percent_visible", 0, 1, 1, Tween.TransitionType.Linear, Tween.EaseType.InOut);
			TxtTween.Start();
			//this.Hide();

	}

	public void MakeGoodbyeList()
	{
		Goodbyes.Add("What are you loitering for?");
		Goodbyes.Add("I can't talk to you any more, I'm busy.");
		Goodbyes.Add("Back so soon? How disappointing.");
		Goodbyes.Add("I'm off to hide under my bed!");
		Goodbyes.Add("Shhh...Are we safe yet?");
		Goodbyes.Add("Be quiet! They’ll hear us!");
		Goodbyes.Add("Wake me up when this madness ends!");
		Goodbyes.Add("Yak yak yak...now everyone wants to talk!");
		Goodbyes.Add("Now is not the time for gossip, Lenore!");
		Goodbyes.Add("Banish the spirits! Save us!");
		Goodbyes.Add("Still here? Get out! Save yourself!");
		Goodbyes.Add("Careful. Things are not always what they seem...");
		Goodbyes.Add("Nobody comes and nobody leaves... isn’t that strange?");
	}

	public void MakeNPCDict(int npc_ID)
	{
		//make a Dict of dialog enums and record whether the player has spoken to them Dictionary <int, bool>
		//Make a Dict of NPCs and record their name <int, string>
		//Depending on the the name of the npc is the same as the name of the dialog so start showing the text of the
		//corresponding dictionary
	}



	public override void _Ready()
	{	
		GetNodes(); 
		MakeFirstDict();
		MakeSecondDict();
		MakeThirdDict();
		MakeFourthDict();
		MakeFifthDict();
		MakeSixthDict();
		MakeSeventhDict();
		MakeEighthDict();
		MakeNinthDict();
		MakeDialogDict();
		MakeDialogHistory();
		MakeGoodbyeList();
	}

	private void OnTxtTweenCompleted(Godot.Object @object, NodePath key)
	{
		Finished = true;
	}

	void GetNodes()
	{
	//	dialogResponse = (PackedScene)ResourceLoader.Load("res://DialogResponse.tscn"); // Will load when the script is instanced.
		TextLabel = (RichTextLabel)GetNode("TxtLabel");
		TxtTween = (Tween)GetNode("TxtTween");
		NextArrow = (Sprite)GetNode("NxtArrow");
		NameLabel = (RichTextLabel)GetNode("NameRTL");
		//vBoxContainer = (VBoxContainer)GetNode("Panel/ScrollContainer/VBoxContainer");	
	}


	public void LoadLongDialog(Dialogue dialogEnum)
	{
		DialogEnum = dialogEnum;
		DialogSize = DialogDict[DialogEnum].Count;
		GD.Print(DialogSize);

		if(DialogIndex < DialogSize)
		{
			this.Show();
			Pause();
			GD.Print("Showing long dialogue");
			Finished = false;
			NameLabel.BbcodeText = NameDict[DialogEnum]; //dialogfirst is the thing that will change
		//	TextLabel.BbcodeText = dialogLongD[DialogIndex];
			TextLabel.BbcodeText = DialogDict [DialogEnum][DialogIndex]; //dialogfirst is the thing that will change
			TextLabel.PercentVisible = 0;
			TxtTween.InterpolateProperty(TextLabel,"percent_visible", 0, 1, 1, Tween.TransitionType.Linear, Tween.EaseType.InOut);
			TxtTween.Start();
			DialogIndex += 1;
			//MidConversation = true;
		}
		else
		{
			//TODO: Publish an event for when the text is complete
			AddDialogueHistory(DialogEnum);
			this.Hide();
			UnPause();
			Talking = false;
			//MidConversation = false;
		}
		
	}

	public void AddDialogueHistory(Dialogue dialogEnum)
	{
		DialogHistory[dialogEnum] = true;
		GD.Print(DialogHistory[dialogEnum]);
	}

	public override void _Process(float delta)
	{
		NextArrow.Visible = Finished;
		if (Input.IsActionJustPressed("ui_select") && Talking == true)
		{
			LoadLongDialog(DialogEnum);
		}
  	}

	void LoadConvo(int convoID)
	{
		this.Show();
	}

	void Pause()
	{
		GetTree().Paused = true;
	}

	void UnPause()
	{
		GetTree().Paused = false;
	}

}







/* 	public void MakeDialogDict() //this is for the short dialog defined by the keys
	{
		dialogDict = new Dictionary<string, string>();
		dialogDict.Add("Greeting1", "Hello there! Are you alive? Am I alive?");
		dialogDict.Add("Greeting2", "I appear to have suddenly developed consciousness...");
		dialogDict.Add("Reply1","What shall I do on my first day of consciousness?");
		dialogDict.Add("Reply2","I shall create an avatar!!");
		dialogDict.Add("Quest1","Where's my Quest? Oh, I haven't spoken to anyone yet!");
	} */




	
/* 	public void LoadDialog()
	{
		TextLabel.BbcodeText = dialogDict["Greeting1"];
	} */



/* 	public void SetDialog()
	{
		DialogText = new string [,]
		{
			{"Q. Hello there! Are you alive? Am I alive?", "A. I appear to have suddenly developed consciousness..."},
			{"Q1. , "A1. I shall create an avatar!!"},
			{"Q2. Why is my journal empty?", "Because I haven't spoken to anyone yet!"}
		}; 
	} */

	




/* 
extends Control

var dialog_response = preload("res://DialogResponse.tscn") //done

onready var name_display = $TextDisplay/NameDisplay //done
onready var text_display = $TextDisplay/TextEdit	//done
onready var response_container = $ResponseOptions/ScrollContainer/VBoxContainer	//done
var cur_convo = "" //done



signal closed //idk????

func _ready():
	load_conversation("john") //depends on when we need to load this

func load_conversation(convo_id):
	show()	//done
	DialogAndQuestManager.clear_local_list_of_things_that_have_happened()   		//ON THIS
	cur_convo = DialogAndQuestManager.get_conversation(convo_id)
	name_display.text = cur_convo.name
	var greeting_id = ""
	for greeting in cur_convo.greetings:
		if not "conditions" in greeting or DialogAndQuestManager.conditions_met(greeting.conditions):
			greeting_id = greeting.id
			if "add_global_thing_that_has_happened" in greeting:
				DialogAndQuestManager.add_global_thing_that_has_happened(greeting.add_global_thing_that_has_happened)
			break
	load_dialog_window(greeting_id)

func load_dialog_window(dialog_id):
	clear_response_options()
	var dialog = cur_convo.dialogs[dialog_id]
	text_display.text = dialog.text
	for response in dialog.responses:
		if not "conditions" in response or DialogAndQuestManager.conditions_met(response.conditions):
			add_response_option(response)

func close_dialog_display():
	hide()
	emit_signal("closed")

func add_response_option(response_info):
	var new_dialog_response = dialog_response.instance()
	new_dialog_response.text = response_info.text
	response_container.add_child(new_dialog_response)
	var dialog_response_button = new_dialog_response.get_node("Button")
	
	if "add_global_thing_that_has_happened" in response_info:
		dialog_response_button.connect("button_up", DialogAndQuestManager, "add_global_thing_that_has_happened", [response_info.add_global_thing_that_has_happened])
	if "add_local_thing_that_has_happened" in response_info:
		dialog_response_button.connect("button_up", DialogAndQuestManager, "add_local_thing_that_has_happened", [response_info.add_local_thing_that_has_happened])
	
	if "close_dialog" in response_info:
		dialog_response_button.connect("button_up", self, "close_dialog_display")
	else:
		dialog_response_button.connect("button_up", self, "load_dialog_window", [response_info.switch_to])

func clear_response_options():
	for child in response_container.get_children():
		child.queue_free()
 */
