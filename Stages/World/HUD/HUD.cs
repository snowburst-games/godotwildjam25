using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class HUD : CanvasLayer
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";
	public delegate void ArtefactUsedDelegate();
	public event ArtefactUsedDelegate ArtefactUsed;
	public delegate void SpellSelectedDelegate(ActorSpellManager.Spell spell);
	public event SpellSelectedDelegate SpellSelected;
	public delegate void ItemUsedDelegate(PickupBehaviour.Item item);
	public event ItemUsedDelegate ItemUsed;

	private Dictionary<ActorExperienceManager.StatThatUpdates, float> _latestUpdatedStats = new Dictionary<ActorExperienceManager.StatThatUpdates, float>();

	private Dictionary<ActorSpellManager.Spell, Button> _spellButtons;

	public enum Announcement {
		LevelUp
	}
	public Dictionary<Announcement, string> Announcements {get; set;} = new Dictionary<Announcement, string>()
	{
		{Announcement.LevelUp, "Your hold over the spirit world grows stronger! Use your wisdom to improve your magic.\n(Click the button on the top right to improve your spells!)"}
	};

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		_spellButtons = new Dictionary<ActorSpellManager.Spell, Button>()
		{
			{ActorSpellManager.Spell.FireBolt, GetNode<Button>("PnlSpellBtns/SpellBtns/BtnFirebolt")},
			{ActorSpellManager.Spell.Fireball, GetNode<Button>("PnlSpellBtns/SpellBtns/BtnFireball")},
			{ActorSpellManager.Spell.Heal, GetNode<Button>("PnlSpellBtns/SpellBtns/BtnHeal")},
			{ActorSpellManager.Spell.Berserk, GetNode<Button>("PnlSpellBtns/SpellBtns/BtnBerserk")},
			{ActorSpellManager.Spell.Blink, GetNode<Button>("PnlSpellBtns/SpellBtns/BtnBlink")},
			{ActorSpellManager.Spell.SpiritArtefact, GetNode<Button>("PnlSpellBtns/SpellBtns/BtnSpiritArtefact")}
		};
		HideAllSpellButtons();
	}

	public void PlayAnim(string anim)
	{
		if (GetNode<AnimationPlayer>("Anim").CurrentAnimation != anim)
		{
			GetNode<AnimationPlayer>("Anim").Play(anim);
		}
	}

	public void ShowSpellButton(ActorSpellManager.Spell spell)
	{
		_spellButtons[spell].Visible = true;
		// foreach (Button btn in _spellButtons)
		// {
		// 	if ()
		// }
		// if (_spellButtons.Values.Where(x => x.Visible == false).Count() == 1)
		// {
		// 	SelectSpell(spell);
		// }
		// SelectAnyButArtefact();

		
	}

	// public void SelectAnyButArtefact()
	// {
	// 	if (_lastSpellSelected != ActorSpellManager.Spell.Empty)
	// 	{
	// 		SelectSpell(_lastSpellSelected);
	// 		return;
	// 	}

	// 	int visibleCount = 0;
	// 	ActorSpellManager.Spell nextSpell = ActorSpellManager.Spell.Empty;
	// 	foreach (KeyValuePair<ActorSpellManager.Spell, Button> kv in _spellButtons)
	// 	{
	// 		if (kv.Key == ActorSpellManager.Spell.SpiritArtefact)
	// 		{
	// 			continue;
	// 		}
	// 		if (kv.Value.Visible == true)
	// 		{
	// 			visibleCount += 1;
	// 			nextSpell = kv.Key;
	// 		}
			
	// 	}
	// 	if (visibleCount == 1)
	// 	{
	// 		SelectSpell(nextSpell);
	// 	}
	// }

	public void HideAllSpellButtons()
	{
		foreach (Button btn in _spellButtons.Values)
		{
			btn.Visible = false;
		}
	}

	public void EnableAllSpellButtons()
	{
		foreach (Node n in GetNode("PnlSpellBtns/SpellBtns").GetChildren())
		{
			if (n is Button btn)
			{
				btn.Disabled = false;
			}
		}
	}

	public void OnStatsUpdated(Dictionary<ActorExperienceManager.StatThatUpdates, float> statsThatUpdate)
	{
		_latestUpdatedStats = statsThatUpdate;
		// _statsLabels[StatsLabel.Health].Text = "Health: " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.Health] + 
		// 	" / " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.MaxHealth];
		// 	_statsLabels[StatsLabel.Mana].Text = "Mana: " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.Mana] + 
		// 	" / " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.MaxMana];
		// _statsLabels[StatsLabel.Power].Text = "Power: " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.Power];
		// _statsLabels[StatsLabel.Level].Text = "Level: " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.Level];
		// _statsLabels[StatsLabel.TotalSpirit].Text = "Total Spirit: " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.Experience];
		// _statsLabels[StatsLabel.SpiritNeeded].Text = "Spirit Needed For Level " + (statsThatUpdate[ActorExperienceManager.StatThatUpdates.Level] + 1) +
		// 	": " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.ExperienceNeeded];
		// _statsLabels[StatsLabel.WisdomPoints].Text = "Wisdom Points: " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.SkillPoints];

		// _spellPowerLabels[SpellPowerLabel.FireBolt].Text = "Fire Bolt (mana cost " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.FireBoltCost] + 
		// 	"): " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.FireBolt];
		// _spellPowerLabels[SpellPowerLabel.Fireball].Text = "Fireball (mana cost " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.FireballCost] + 
		// 	"): " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.Fireball];
		// _spellPowerLabels[SpellPowerLabel.Heal].Text = "Heal (mana cost " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.HealCost] + 
		// 	"): " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.Heal];
		// _spellPowerLabels[SpellPowerLabel.Blink].Text = "Blink (mana cost " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.BlinkCost] + 
		// 	"): " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.Blink];
		// _spellPowerLabels[SpellPowerLabel.Berserk].Text = "Berserk (mana cost " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.BerserkCost] + 
		// 	"): " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.Berserk];
		GetNode<Button>("PnlSpellBtns/SpellBtns/BtnFirebolt").HintTooltip = "Throw a quick bolt of fire for " 
			+ statsThatUpdate[ActorExperienceManager.StatThatUpdates.FireBoltDamage] + " damage. Cost: "
			+ statsThatUpdate[ActorExperienceManager.StatThatUpdates.FireBoltCost] + ". (Hotkey: 1)";
		GetNode<Button>("PnlSpellBtns/SpellBtns/BtnFireball").HintTooltip = "Unleash a mighty ball of fire for " 
			+ statsThatUpdate[ActorExperienceManager.StatThatUpdates.FireballDamage] + " damage. Cost: "
			+ statsThatUpdate[ActorExperienceManager.StatThatUpdates.FireballCost] + ". (Hotkey: 2)";
		GetNode<Button>("PnlSpellBtns/SpellBtns/BtnBerserk").HintTooltip = "Watch as your enemies turn on one another for " 
			+ statsThatUpdate[ActorExperienceManager.StatThatUpdates.BerserkTime] + " seconds. Cost: "
			+ statsThatUpdate[ActorExperienceManager.StatThatUpdates.BerserkCost] + ". (Hotkey: 4)";
		GetNode<Button>("PnlSpellBtns/SpellBtns/BtnBlink").HintTooltip = "Close your eyes and you're there! Cost: "
			+ statsThatUpdate[ActorExperienceManager.StatThatUpdates.BlinkCost] + ". (Hotkey: 5)";
		GetNode<Button>("PnlSpellBtns/SpellBtns/BtnHeal").HintTooltip = "Heal thyself. Does " 
			+ statsThatUpdate[ActorExperienceManager.StatThatUpdates.HealHealing] + " healing. Cost: "
			+ statsThatUpdate[ActorExperienceManager.StatThatUpdates.HealCost] + ". (Hotkey: 3)";
//HUD/CntIndicators/HealthBar/LblHealth
		GetNode<ProgressBar>("CntIndicators/HealthBar").Value = (statsThatUpdate[ActorExperienceManager.StatThatUpdates.Health]
			/ statsThatUpdate[ActorExperienceManager.StatThatUpdates.MaxHealth]) * 100f;
		GetNode<Label>("CntIndicators/HealthBar/LblHealth").Text = Math.Max(1, Math.Ceiling(statsThatUpdate[ActorExperienceManager.StatThatUpdates.Health])) + " / " +
			Math.Ceiling(statsThatUpdate[ActorExperienceManager.StatThatUpdates.MaxHealth]);
		GetNode<ProgressBar>("CntIndicators/ManaBar").Value = (statsThatUpdate[ActorExperienceManager.StatThatUpdates.Mana]
			/ statsThatUpdate[ActorExperienceManager.StatThatUpdates.MaxMana]) * 100f;
		GetNode<Label>("CntIndicators/ManaBar/LblMana").Text = Math.Ceiling(statsThatUpdate[ActorExperienceManager.StatThatUpdates.Mana]) + " / " +
			Math.Ceiling(statsThatUpdate[ActorExperienceManager.StatThatUpdates.MaxMana]);
			
		GetNode<ProgressBar>("CntIndicators/ExperienceBar").Value = Math.Ceiling(statsThatUpdate[ActorExperienceManager.StatThatUpdates.PercentXPToNextLevel]*100f);
		GetNode<Label>("CntIndicators/ExperienceBar/LblLevel").Text = "Level: " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.Level];

		GetNode<Button>("PnlSpellBtns/BtnHealthPot").Disabled = false;
		GetNode<Button>("PnlSpellBtns/BtnManaPot").Disabled = false;
		GetNode<Label>("PnlSpellBtns/BtnHealthPot/Label").Text = statsThatUpdate[ActorExperienceManager.StatThatUpdates.HealthPots].ToString();
		GetNode<Label>("PnlSpellBtns/BtnManaPot/Label").Text = statsThatUpdate[ActorExperienceManager.StatThatUpdates.ManaPots].ToString();
		if (statsThatUpdate[ActorExperienceManager.StatThatUpdates.HealthPots] == 0)
		{
			GetNode<Button>("PnlSpellBtns/BtnHealthPot").Disabled = true;
		}
		if (statsThatUpdate[ActorExperienceManager.StatThatUpdates.ManaPots] == 0)
		{
			GetNode<Button>("PnlSpellBtns/BtnManaPot").Disabled = true;
		}
		//value*100f


	}

	private bool _bossDied = false;

	public void OnBossTookDamage(float health, float maxHealth)
	{
		if (_bossDied)
		{
			return;
		}
		GetNode<ProgressBar>("CntIndicators/BossHealthBar").Visible = true;

		GetNode<ProgressBar>("CntIndicators/BossHealthBar").Value = (health / maxHealth) * 100f;
		GetNode<Label>("CntIndicators/BossHealthBar/LblHealth").Text = Math.Ceiling(health) + " / " + Math.Ceiling(maxHealth);

		// if (health == 0)
		// {
		// 	GetNode<ProgressBar>("CntIndicators/BossHealthBar").Visible = false; // we end the game after boss dies anyway, right?
		// }
	}

	public void OnBossDied()
	{
		_bossDied = true;
		GetNode<ProgressBar>("CntIndicators/BossHealthBar").Visible = false; // we end the game after boss dies anyway, right?
	}

	public void OnPlayerDied()
	{
		GetNode<ProgressBar>("CntIndicators/BossHealthBar").Visible = false;
	}

	public void OnBtnFireboltPressed()
	{
		SelectSpell(ActorSpellManager.Spell.FireBolt);
		// EnableAllSpellButtons();
		// _spellButtons[ActorSpellManager.Spell.FireBolt].Disabled = true;
		// SpellSelected?.Invoke(ActorSpellManager.Spell.FireBolt);
		// GetNode<Button>("SpellBtns/BtnFirebolt").Disabled = true;
	}

	public void MakeSpellButtonGlow(ActorSpellManager.Spell spell)
	{
		foreach (Button b in _spellButtons.Values)
		{
			b.GetNode<Sprite>("Sprite").Modulate = new Color(1,1,1);
		}
		_spellButtons[spell].GetNode<Sprite>("Sprite").Modulate = new Color(2,2,2);
	}


	private void OnBtnFireballPressed()
	{
		SelectSpell(ActorSpellManager.Spell.Fireball);
		// EnableAllSpellButtons();
		// _spellButtons[ActorSpellManager.Spell.Fireball].Disabled = true;
		// SpellSelected?.Invoke(ActorSpellManager.Spell.Fireball);
		// GetNode<Button>("SpellBtns/BtnFireball").Disabled = true;
	}

	private void OnBtnHealPressed()
	{
		SelectSpell(ActorSpellManager.Spell.Heal);
		// EnableAllSpellButtons();
		// _spellButtons[ActorSpellManager.Spell.Heal].Disabled = true;
		// SpellSelected?.Invoke(ActorSpellManager.Spell.Heal);
	}

	private void OnBtnBerserkPressed()
	{
		SelectSpell(ActorSpellManager.Spell.Berserk);
	}


	private void OnBtnBlinkPressed()
	{
		SelectSpell(ActorSpellManager.Spell.Blink);
	}



	private void OnBtnSpiritArtefactPressed()
	{
		ArtefactUsed?.Invoke();
		// SelectSpell(ActorSpellManager.Spell.SpiritArtefact);

	}


	private void OnBtnManaPotPressed()
	{
		ItemUsed?.Invoke(PickupBehaviour.Item.ManaPot);
	}


	private void OnBtnHealthPotPressed()
	{
		ItemUsed?.Invoke(PickupBehaviour.Item.HealthPot);
	}

	private ActorSpellManager.Spell _lastSpellSelected = ActorSpellManager.Spell.Empty;

	private void SelectSpell(ActorSpellManager.Spell spell)
	{
		EnableAllSpellButtons();
		_spellButtons[spell].Disabled = true;
		MakeSpellButtonGlow(spell);
		SpellSelected?.Invoke(spell);

		if (spell != ActorSpellManager.Spell.SpiritArtefact)
		{
			_lastSpellSelected = spell;
		}
	}

	public void StartSpellCooldown(ActorSpellManager.Spell spell, float cooldown)
	{
		_spellButtons[spell].GetNode<Timer>("Timer").WaitTime = cooldown;
		_spellButtons[spell].GetNode<Timer>("Timer").Start();
	}

	public override void _Process(float delta)
	{
		base._Process(delta);
		foreach (Button btn in _spellButtons.Values)
		{
			btn.GetNode<Label>("Label").Text = "";
			btn.Modulate = new Color(1,1,1);
			// if (_lastSpellSelected != ActorSpellManager.Spell.Empty)
			// {
			// 	MakeSpellButtonGlow(_lastSpellSelected);
			// }
			if (btn.GetNode<Timer>("Timer").TimeLeft > 0)
			{
			// 	btn.GetNode<Label>("Label").Text = Math.Round(btn.GetNode<Timer>("Timer").TimeLeft, 2).ToString();
			btn.Modulate = new Color(1+btn.GetNode<Timer>("Timer").TimeLeft*5,1+btn.GetNode<Timer>("Timer").TimeLeft*5,1+btn.GetNode<Timer>("Timer").TimeLeft*10);
			}
			
		}

		if (Input.IsActionJustPressed("HealthPotion"))
		{
			if (!GetNode<Button>("PnlSpellBtns/BtnHealthPot").Disabled)
			{
				OnBtnHealthPotPressed();
			}
		}
		if (Input.IsActionJustPressed("ManaPotion"))
		{
			if (!GetNode<Button>("PnlSpellBtns/BtnManaPot").Disabled)
			{
				OnBtnManaPotPressed();
			}
		}
		if (Input.IsActionJustPressed("Menu"))
		{
			OnBtnMenuPressed();
		}
		if (Input.IsActionJustPressed("Character"))
		{
			OnBtnCharacterPressed();
		}
	}


	private void OnBtnCharacterPressed()
	{
		GetNode<PnlCharacter>("PnlCharacter").Start();
	}

	private void OnBtnMenuPressed()
	{
		GetNode<PnlMenu>("PnlMenu").Start();
	}

}


