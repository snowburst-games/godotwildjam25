using Godot;
using System;
using System.Collections.Generic;

// todo
// need to update all labels every time a change occurs - event from health changed / level changed / xp changed / power changed
// FINISH CONNECTING THE PLUS BUTTONS TO SPELLPOWERINVOKED
// make sure all the spells increase in power appropriately after skill points are spent

public class PnlCharacter : Panel
{

	public delegate void SpellPlusPressedDelegate(SpellPowerLabel spellPower);
	public event SpellPlusPressedDelegate SpellPlusPressed;

	public enum StatsLabel {
		Health, Mana, Power, Level, TotalSpirit, SpiritNeeded, WisdomPoints
	}
	private Dictionary<ActorExperienceManager.StatThatUpdates, float> _latestUpdatedStats = new Dictionary<ActorExperienceManager.StatThatUpdates, float>();
	public enum SpellPowerLabel {
		FireBolt, Fireball, Heal, Blink, Berserk
	}

	private Dictionary<StatsLabel, Label> _statsLabels;
	private Dictionary<SpellPowerLabel, Label> _spellPowerLabels;
	// private Dictionary<ActorSpellManager.Spell, SpellPowerLabel> 
	private List<Button> _allSpellPowerPlusBtns = new List<Button>();
	private float _deltaTime = 0;

	// public int CurrentSkillPoints {get; set;} = 0; // THIS NEEDS TO UPDATE WHENEVER THE CHARACTER LEVELS UP AND ALSO WHEN SPELLPLUSPRESSED IS INVOKED (AT THE TARGET)

	public override void _Ready()
	{
		Visible = false;

		_statsLabels = new Dictionary<StatsLabel, Label>()
		{
			{StatsLabel.Health, GetNode<Label>("PnlStats/LblHealth")},
			{StatsLabel.Mana, GetNode<Label>("PnlStats/LblMana")},
			{StatsLabel.Power, GetNode<Label>("PnlStats/LblPower")},
			{StatsLabel.Level, GetNode<Label>("PnlStats/LblLevel")},
			{StatsLabel.TotalSpirit, GetNode<Label>("PnlStats/LblSpirit")},
			{StatsLabel.SpiritNeeded, GetNode<Label>("PnlStats/LblSpiritNext")},
			{StatsLabel.WisdomPoints, GetNode<Label>("PnlStats/LblWisdom")}
		};
		_spellPowerLabels = new Dictionary<SpellPowerLabel, Label>()
		{
			{SpellPowerLabel.FireBolt, GetNode<Label>("PnlSkills/LblFireBolt")},
			{SpellPowerLabel.Fireball, GetNode<Label>("PnlSkills/LblFireball")},
			{SpellPowerLabel.Heal, GetNode<Label>("PnlSkills/LblHeal")},
			{SpellPowerLabel.Blink, GetNode<Label>("PnlSkills/LblBlink")},
			{SpellPowerLabel.Berserk, GetNode<Label>("PnlSkills/LblBerserk")}
		};

		foreach (SpellPowerLabel spellPower in _spellPowerLabels.Keys)
		{
			_allSpellPowerPlusBtns.Add(GetSpellPowerButton(spellPower));
		}
	}

	public Button GetSpellPowerButton(SpellPowerLabel spellPower)
	{
		return _spellPowerLabels[spellPower].GetNode<Button>("BtnPlus");
	}

	// public SpellPowerLabel
	

	private void OnBtnPlusFireBoltPressed()
	{
		SpellPlusPressed?.Invoke(SpellPowerLabel.FireBolt);
		DoSpellPowerPlusVisible();
	}
	private void OnBtnPlusFireballPressed()
	{
		SpellPlusPressed?.Invoke(SpellPowerLabel.Fireball);
		DoSpellPowerPlusVisible();
	}
	private void OnBtnPlusHealPressed()
	{
		SpellPlusPressed?.Invoke(SpellPowerLabel.Heal);
		DoSpellPowerPlusVisible();
	}
	private void OnBtnPlusBerserkPressed()
	{
		SpellPlusPressed?.Invoke(SpellPowerLabel.Berserk);
		DoSpellPowerPlusVisible();
	}
	private void OnBtnPlusBlinkPressed()
	{
		SpellPlusPressed?.Invoke(SpellPowerLabel.Blink);
		DoSpellPowerPlusVisible();
	}

	public void DoSpellPowerPlusVisible()
	{
		if (!_latestUpdatedStats.ContainsKey(ActorExperienceManager.StatThatUpdates.SkillPoints))
		{
			GD.Print("Error! Stats in HUD didn't update in time!");
			return;
		}
		// foreach (SpellPowerLabel spellPower in _spellPowerLabels.Keys)
		foreach (Button b in _allSpellPowerPlusBtns)
		{
			// Button b = _spellPowerLabels[spellPower].GetNode<Button>("BtnPlus");

			if (_latestUpdatedStats[ActorExperienceManager.StatThatUpdates.SkillPoints] == 0)
			{
				b.Visible = false;
			}
			else
			{
				foreach (SpellPowerLabel spellPower in _spellPowerLabels.Keys)
				{
					// GD.Print(b.GetParent());
					// GD.Print(_spellPowerLabels[spellPower]);
					if (b.GetParent() == _spellPowerLabels[spellPower])
					{
						// GD.Print(_latestUpdatedStats[_spellPowerLabelLevels[spellPower]]);
						if (_latestUpdatedStats[_spellPowerLabelLevels[spellPower]] > 0)
						{
							b.Visible = true;
						}
						else
						{
							b.Visible = false;
						}
						if (spellPower == SpellPowerLabel.Blink && _latestUpdatedStats[_spellPowerLabelLevels[spellPower]] >= 6)
						{
							b.Visible = false;
						}
					}
				}


				// if (b.GetParent() == _spellPowerLabels[SpellPowerLabel.FireBolt] && _latestUpdatedStats[ActorExperienceManager.StatThatUpdates.FireBolt] > 0)
				// {
				// 	b.Visible = true;
				// }
				// else
				// {
				// 	b.Visible = false;
				// }
				
			}
		}
	}

		private Dictionary<SpellPowerLabel, ActorExperienceManager.StatThatUpdates> _spellPowerLabelLevels = 
			new Dictionary<SpellPowerLabel, ActorExperienceManager.StatThatUpdates>()
		{
			{SpellPowerLabel.FireBolt, ActorExperienceManager.StatThatUpdates.FireBolt},
			{SpellPowerLabel.Fireball, ActorExperienceManager.StatThatUpdates.Fireball},
			{SpellPowerLabel.Heal, ActorExperienceManager.StatThatUpdates.Heal},
			{SpellPowerLabel.Berserk, ActorExperienceManager.StatThatUpdates.Berserk},
			{SpellPowerLabel.Blink, ActorExperienceManager.StatThatUpdates.Blink}
		};


		// foreach (Button b in _allSpellPowerPlusBtns)
		// 	{
		// 		if (_latestUpdatedStats[ActorExperienceManager.StatThatUpdates.SkillPoints] == 0)
		// 		{
		// 			b.Visible = false;
		// 		}
		// 		else
		// 		{
		// 			b.Visible = true;
		// 		}
		// 	}
	

	public void Start()
	{
		_deltaTime = 0;
		GetTree().Paused = true;
		Visible = true;
		DoSpellPowerPlusVisible();
	}
	
	private void OnBtnReturnPressed()
	{
		GetTree().Paused = false;
		Visible = false;

	}

	public override void _Process(float delta)
	{
		_deltaTime += delta;
		if (Input.IsActionJustPressed("Character") && _deltaTime > 0.1f)
		{
			OnBtnReturnPressed();
		}
	}
	
	public void OnStatsUpdated(Dictionary<ActorExperienceManager.StatThatUpdates, float> statsThatUpdate)
	{
		_latestUpdatedStats = statsThatUpdate;
		_statsLabels[StatsLabel.Health].Text = "Health: " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.Health] + 
			" / " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.MaxHealth];
			_statsLabels[StatsLabel.Mana].Text = "Mana: " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.Mana] + 
			" / " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.MaxMana];
		_statsLabels[StatsLabel.Power].Text = "Power: " + Math.Ceiling(statsThatUpdate[ActorExperienceManager.StatThatUpdates.Power]);
		_statsLabels[StatsLabel.Level].Text = "Level: " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.Level];
		_statsLabels[StatsLabel.TotalSpirit].Text = "Total Spirit: " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.Experience];
		_statsLabels[StatsLabel.SpiritNeeded].Text = "Spirit Needed For Level " + (statsThatUpdate[ActorExperienceManager.StatThatUpdates.Level] + 1) +
			": " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.ExperienceNeeded];
		_statsLabels[StatsLabel.WisdomPoints].Text = "Wisdom Points: " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.SkillPoints];

		_spellPowerLabels[SpellPowerLabel.FireBolt].Text = "Fire Bolt. Mana cost: " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.FireBoltCost] + 
			". Damage: " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.FireBoltDamage] + ". Current Spell Level: "
			 + statsThatUpdate[ActorExperienceManager.StatThatUpdates.FireBolt];
		_spellPowerLabels[SpellPowerLabel.Fireball].Text = "Fireball. Mana cost: " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.FireballCost] + 
			". Damage: " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.FireballDamage] + ". Current Spell Level: "
			 + statsThatUpdate[ActorExperienceManager.StatThatUpdates.Fireball];
		_spellPowerLabels[SpellPowerLabel.Heal].Text = "Heal. Mana cost: " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.HealCost] + 
			". Healing: " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.HealHealing] + ". Current Spell Level: "
			 + statsThatUpdate[ActorExperienceManager.StatThatUpdates.Heal];
		_spellPowerLabels[SpellPowerLabel.Blink].Text = "Blink. Mana cost: " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.BlinkCost] +
			 ". Current Spell Level: " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.Blink] + "\n(Higher level reduces blink mana cost).";
		_spellPowerLabels[SpellPowerLabel.Berserk].Text = "Berserk. Mana cost: " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.BerserkCost] + 
			". Duration: " + statsThatUpdate[ActorExperienceManager.StatThatUpdates.BerserkTime] + ". Current Spell Level: "
			 + statsThatUpdate[ActorExperienceManager.StatThatUpdates.Berserk];
	}
}

			// {StatThatUpdates.FireBoltDamage, (float)Math.Ceiling(Actor.GetNode<ActorSpellManager>("SpellManager").GetOverallPower(ActorSpellManager.Spell.FireBolt))},
			// {StatThatUpdates.FireballDamage, (float)Math.Ceiling(Actor.GetNode<ActorSpellManager>("SpellManager").GetOverallPower(ActorSpellManager.Spell.Fireball))}	,
			// {StatThatUpdates.HealHealing, (float)Math.Ceiling(Actor.GetNode<ActorSpellManager>("SpellManager").GetOverallPower(ActorSpellManager.Spell.Heal))}	,
			// {StatThatUpdates.BerserkTime , (float)Math.Ceiling(Actor.GetNode<ActorSpellManager>("SpellManager").GetOverallPower(ActorSpellManager.Spell.Berserk))}	
