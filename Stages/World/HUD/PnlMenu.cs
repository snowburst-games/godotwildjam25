using Godot;
using System;

public class PnlMenu : Panel
{

	public delegate void BtnQuitPressedDelegate();
	public event BtnQuitPressedDelegate BtnQuitPressed;
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";
	private float _deltaTime = 0;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		Visible = false;
	}

	public void Start()
	{
		GetTree().Paused = true;
		Visible = true;
		_deltaTime = 0;
	}

	private void OnBtnQuitPressed()
	{
		BtnQuitPressed?.Invoke();
	}


	private void OnBtnReturnPressed()
	{
		GetTree().Paused = false;
		Visible = false;

	}

	public override void _Process(float delta)
	{
		_deltaTime += delta;
		if (Input.IsActionJustPressed("Menu") && _deltaTime > 0.1f)
		{
			OnBtnReturnPressed();
		}
	}
}

