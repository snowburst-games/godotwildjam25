using Godot;
using System;

public class TextTrigger : Area2D
{
	
	public delegate void TextTriggerFiredDelegate(string text);
	public event TextTriggerFiredDelegate TextTriggerFired;

	public override void _Ready()
	{
		
	}

	[Export]
	public string Text {get; set;} = "";

	[Export]
	public bool OneShot {get; set;} = true;

	private void OnBodyEntered(Godot.Object body)
	{
		if (body is PlayerBody player)
		{
			TextTriggerFired?.Invoke(Text);
			if (OneShot)
			{
				TextTriggerFired = null;
			}
		}
	}	



}

