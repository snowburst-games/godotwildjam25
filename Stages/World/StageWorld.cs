using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class StageWorld : Stage
{

	private List<Actor> _monsters = new List<Actor>();
	private WorldTreasureHandler _worldTreasureHandler = new WorldTreasureHandler();
	private Actor _player;
	// private List<IPickupable> _potions = new List<IPickupable>();
	private PictureStory _pictureStory;
	private LblEventInfo _lblEventInfo;
	private AudioStreamPlayer2D _soundPlayer;
	private AudioStreamPlayer _musicPlayer;
	public Dictionary<AudioData.World, AudioStream> WorldSounds {get; set;} = AudioData.LoadedSounds<AudioData.World>(AudioData.WorldSoundPaths);

	public override void _Ready()
	{
		base._Ready();
		
		if (this.SharedData != null)
		{
			if (this.SharedData["Difficulty"] is int i)
			{
				SetDifficulty(i);
			}
			else
			{
				GD.Print("WARNING: DIFFICULTY INTEGER NOT IN SHARED DATA!");
			}
		}
		else
		{
			GD.Print("WARNING: NO SHARED DATA! ARE WE RUNNING THIS FROM THE MAIN MENU?");
		}


		_soundPlayer = GetNode<AudioStreamPlayer2D>("SoundPlayer");
		_musicPlayer = GetNode<AudioStreamPlayer>("MusicPlayer");
		AudioHandler.PlaySound(_musicPlayer, WorldSounds[AudioData.World.Music], AudioData.SoundBus.Music);

		GetNode<WorldSpellManager>("WorldSpellManager").SpellCast+=this.OnSpellCast;
		GetNode<PnlMenu>("HUD/PnlMenu").BtnQuitPressed+=this.QuitToMenu;
		_pictureStory = GetNode<PictureStory>("HUD/PictureStory");
		_pictureStory.Hide();
		_lblEventInfo = GetNode<LblEventInfo>("HUD/LblEventInfo");
		// _lblEventInfo.QueueText("You are lost and alone. Explore the area and talk to the remaining villagers!", 1);
		
		_player = AddPlayer();
		_worldTreasureHandler.SetPlayer(_player);
		foreach (Node n in GetNode("MonsterSpawnPoints1").GetChildren())
		{
			if (n is Position2D position2D)
			{
				// Actor m = AddMonster(position2D.Position/2);
				// m.GetNode<MonsterBody>("Body").SetPower(MonsterBody.PowerLevel.One);
				// _monsters.Add(m);
				SpawnMonster1(position2D.Position/2);
			}
		}
		foreach (Node n in GetNode("MonsterSpawnPoints2").GetChildren())
		{
			if (n is Position2D position2D)
			{
				// Actor m = AddMonster(position2D.Position/2);
				// m.GetNode<MonsterBody>("Body").SetPower(MonsterBody.PowerLevel.Two);
				// _monsters.Add(m);
				SpawnMonster2(position2D.Position/2);
			}
		}
		foreach (Node n in GetNode("MonsterSpawnPoints3").GetChildren())
		{
			if (n is Position2D position2D)
			{
				// Actor m = AddMonster(position2D.Position/2);
				// m.GetNode<MonsterBody>("Body").SetPower(MonsterBody.PowerLevel.Three);
				// _monsters.Add(m);
				SpawnMonster3(position2D.Position/2);
			}
		}
		foreach (Node n in GetNode("MonsterSpawnPointsBoss").GetChildren())
		{
			if (n is Position2D position2D)
			{
				// Actor m = AddMonster(position2D.Position/2);
				// m.GetNode<MonsterBody>("Body").SetPower(MonsterBody.PowerLevel.Boss);
				// m.GetNode<MonsterBody>("Body").TookDamage+=GetNode<HUD>("HUD").OnBossTookDamage;
				// m.GetNode<MonsterBody>("Body").TookDamage+=this.OnBossTookDamage;
				// m.GetNode<MonsterBody>("Body").BossEngaged+=this.OnBossEngaged;
				// _monsters.Add(m);
				SpawnMonsterBoss(position2D.Position/2);
			}
		}
		foreach (Node n in GetNode("NPCs").GetChildren())
		{
			if (n is NPC npc)
			{
				InitNPC(npc);
			}
		}
		foreach (Node n in GetNode("Potions").GetChildren())
		{
			if (n is IPickupable p)
			{
				// _potions.Add(p);
				p.Init();
				p.PickupBehaviour.LookedForPlayer+=this.OnLookedForPlayer;
			}
		}
		foreach (Node n in GetNode("Treasures").GetChildren())
		{
			if (n is Treasure treasure)
			{
				//
			}
		}
		foreach (PowerOrbs pOrbs in GetNode("PowerOrbs").GetChildren())
		{
			pOrbs.Init(1);
			pOrbs.PickupBehaviour.LookedForPlayer+=this.OnLookedForPlayer;
		}
		foreach (ExperienceOrbs expOrbs in GetNode("SpiritOrbs").GetChildren())
		{
			expOrbs.ExperienceValue = 1;
			expOrbs.Init();
			expOrbs.PickupBehaviour.LookedForPlayer+=this.OnLookedForPlayer;
		}
		foreach (Node n in GetNode("CheckPoints").GetChildren())
		{
			if (n is CheckPoint checkPoint)
			{
				checkPoint.CheckPointReached+=this.OnCheckPointSave;
			}
		}
		foreach (TextTrigger t in GetNode("TextTriggers").GetChildren())
		{
			t.TextTriggerFired+=this.ExternalEventText;
		}

		OnSpiritArtefactActivated(false, false);
		OnCheckPointSave();

		// OnPlayerDied();
		// EndGame();
	}

	private void EndGame()
	{
		_pictureStory.Reset();
		_pictureStory.PictureStoryFinished+=this.OnEndGameStoryCompleted;
		_pictureStory.StayPausedOnFinish = true;
		_pictureStory.FadeOutOnFinish = false;
		_pictureStory.VisibleOnFinish = true;
		_pictureStory.AddScreen("res://Stages/World/PictureStory/victory.png", 
		@"Your flames engulf the spirit,
and he frees a final roar,
then he whispers...
and you hear it...
“I’m your father, dear Lenore!”");
		_pictureStory.Start();
	}

	private void OnEndGameStoryCompleted()
	{
		SceneManager.SimpleChangeScene(SceneData.Stage.MainMenu);
	}

	private void SetDifficulty(int difficulty)
	{
		switch (difficulty)
		{
			case 0:
				GameDifficulty = 0.1f;
				break;
			case 1:
				GameDifficulty = 0.3f;
				break;
			case 2:
				GameDifficulty = 0.7f;
				break;
			case 3:
				GameDifficulty = 1f;
				break;
			case 4:
				GameDifficulty = 1.5f;
				break;
			
		}
	}

	public void QuitToMenu()
	{
		SceneManager.SimpleChangeScene(SceneData.Stage.MainMenu);
	}

	public void InitNPC(NPC npc)
	{
		npc.DialogueStarted+= GetNode<DialogueControl>("HUD/DialogueControl").Start;
	}

	public Actor AddPlayer()
	{
		Actor actor = GetNode<ActorFactory>("ActorFactory").CreatePlayer();
		actor.Name = "Player";
		AddActorCommon(actor);
		actor.Position = GetNode<Position2D>("PlayerSpawnPoint").Position/2;
		actor.GetNode<ActorSpellManager>("SpellManager").Actor = actor;
		actor.GetNode<ActorSpellManager>("SpellManager").SpellAdded+=GetNode<HUD>("HUD").ShowSpellButton;
		actor.GetNode<ActorSpellManager>("SpellManager").SpellCast+=GetNode<WorldSpellManager>("WorldSpellManager").OnSpellCast;
		actor.GetNode<ActorSpellManager>("SpellManager").SpellFizzled+=this.OnSpellFizzled;
		actor.GetNode<ActorSpellManager>("SpellManager").SpellData = GetNode<WorldSpellManager>("WorldSpellManager").SpellData;
		((PlayerInputManager) actor.GetInputManager()).BlinkHinted+=this.OnPlayerBlinkHintedStart;
		actor.GetNode<PlayerBody>("Body").SpiritArtefactActivated+=this.OnSpiritArtefactActivated;
		actor.GetNode<PlayerBody>("Body").TreasureCollected+=this.OnTreasureCollected;
		actor.GetNode<PlayerBody>("Body").PlayerDied+=this.OnPlayerDied;
		actor.GetNode<PlayerBody>("Body").PlayerAnnounced+=this.OnPlayerAnnounced;
		actor.ExperienceManager.StatsUpdated+=GetNode<HUD>("HUD").OnStatsUpdated;
		actor.ExperienceManager.StatsUpdated+=GetNode<PnlCharacter>("HUD/PnlCharacter").OnStatsUpdated;
		GetNode<PnlCharacter>("HUD/PnlCharacter").SpellPlusPressed+=actor.ExperienceManager.OnSpellPlusPressed;
		// actor.GetNode<ActorSpellManager>("SpellManager").AddSpell(ActorSpellManager.Spell.FireBolt); // teswting
		// actor.GetNode<ActorSpellManager>("SpellManager").AddSpell(ActorSpellManager.Spell.Fireball); // teswting
		// actor.GetNode<ActorSpellManager>("SpellManager").AddSpell(ActorSpellManager.Spell.SpiritArtefact); // testing
		// actor.GetNode<ActorSpellManager>("SpellManager").AddSpell(ActorSpellManager.Spell.Heal); // testing
		// actor.GetNode<ActorSpellManager>("SpellManager").AddSpell(ActorSpellManager.Spell.Berserk); // testing
		// actor.GetNode<ActorSpellManager>("SpellManager").AddSpell(ActorSpellManager.Spell.Blink); // testing
		GetNode<HUD>("HUD").ArtefactUsed+=((PlayerInputManager)actor.GetInputManager()).OnArtefactUsed;
		GetNode<HUD>("HUD").SpellSelected+=actor.GetNode<ActorSpellManager>("SpellManager").SetActiveSpell;
		GetNode<HUD>("HUD").ItemUsed+=actor.GetNode<ActorDamageHandler>("DamageHandler").OnItemUsed;
		// GetNode<HUD>("HUD").OnBtnFireboltPressed();
		// actor.TriedToBlink+=this.OnTriedToBlink;
		return actor;
	}
	
	public void OnSpellFizzled(bool notSpirit, bool artefact)
	{
		if (artefact)
		{
			_lblEventInfo.QueueText("Fizzled! The hostile spirits will not allow your escape!", 1);
			return;
		}
		if (notSpirit)
		{
			_lblEventInfo.QueueText("Fizzled! Must be in spirit form...", 1);
		}
		else
		{
			_lblEventInfo.QueueText("Fizzled! Not enough mana!", 1);
		}
	}

	public void OnPlayerAnnounced(HUD.Announcement announcement)
	{
		_lblEventInfo.QueueText(GetNode<HUD>("HUD").Announcements[announcement], 1);
		if (announcement == HUD.Announcement.LevelUp)
		{
			GetNode<HUD>("HUD").PlayAnim("CharacterAlert");
		}
	}

	public void ExternalEventText(string text)
	{
		_lblEventInfo.QueueText(text, 0.5f);
	}

	public void OnTreasureCollected(Treasure.TreasureType treasure)
	{
		switch (treasure)
		{
			case Treasure.TreasureType.Berserk:
				_lblEventInfo.QueueText("Turn your foes against each other!", 0.5f);
				break;
			case Treasure.TreasureType.Blink:
				_lblEventInfo.QueueText("Get around obstacles and escape from enemies!", 0.5f);
				break;
			case Treasure.TreasureType.ExperienceOrbs:
				_lblEventInfo.QueueText("You absorb spirit essence...", 0.5f);
				break;
			case Treasure.TreasureType.Fireball:
				_lblEventInfo.QueueText("Purge them with fire!", 0.5f);
				break;
			case Treasure.TreasureType.Firebolt:
				_lblEventInfo.QueueText("Simple yet reliable!", 0.5f);
				break;
			case Treasure.TreasureType.Heal:
				_lblEventInfo.QueueText("Bind your wounds and go once more!", 0.5f);
				break;
			case Treasure.TreasureType.HealthPot:
				_lblEventInfo.QueueText("Contains the essence of life. Restores health.", 0.5f);
				break;
			case Treasure.TreasureType.ManaPot:
				_lblEventInfo.QueueText("Contains the essence of magic. Restores mana.", 0.5f);
				break;
			case Treasure.TreasureType.PowerOrbs:
				_lblEventInfo.QueueText("Your power grows...", 0.5f);
				break;
			case Treasure.TreasureType.SpiritArtefact:
				_lblEventInfo.QueueText("The fabled Spirit Pendant! Change yourself, change the world.\n(Click pendant icon to use...) ", 0.5f);
				break;
		}
	}

	public void SetPauseNode(Node node, bool pause) // horrible method
	{
		node.SetProcess(!pause);
		node.SetProcessInput(!pause);
		node.SetProcessInternal(!pause);
		node.SetProcessUnhandledInput(!pause);
		node.SetProcessUnhandledKeyInput(!pause);
		
		// for performance
		if (node is NPC nPC)
		{
			nPC.Active = !pause;
		}
		if (node is Actor actor)
		{
			actor.SetActive(!pause);//Active = !pause;
		}
		if (node.HasNode("Body"))
		{
			if (node.GetNode("Body") is MonsterBody m)
			{
				m.GetNode<CollisionShape2D>("Shape").Disabled = pause;
				m.GetNode<Area2D>("DetectionArea").Monitoring = !pause;
				m.GetNode<Area2D>("AttackArea").Monitoring = !pause;
			}
		}
		else
		{
			foreach (Node n in node.GetChildren())
			{
				if (n is CPUParticles2D particles)
				{
					particles.Emitting = !pause;
				}
			}
		}
		// end for performance

		if (node is IPickupable pickupable)
		{
			pickupable.SetPickupPaused(pause);
		}
	
		if (node is Area2D area)
		{

			

			foreach (Node n in area.GetChildren())
			{
				
				if (n is CollisionShape2D shape2D)
				{
					shape2D.SetDeferred("disabled", pause);
				}
				if (n is CollisionPolygon2D polygon2D)
				{
					polygon2D.SetDeferred("disabled", pause);
				}
				if (n is StaticBody2D staticBody)
				{
					foreach (Node staticChild in staticBody.GetChildren())
					{
						if (staticChild is CollisionShape2D staticChildShape2D)
						{
							staticChildShape2D.SetDeferred("disabled", pause);
						}
						if (staticChild is CollisionPolygon2D staticChildPoly2D)
						{
							staticChildPoly2D.SetDeferred("disabled", pause);
						}
					}
				}
			}
		}
	}

	public void SetPauseScene(Node rootNode, bool pause) // doesnt work for collision shapes...
	{
		SetPauseNode(rootNode, pause);
		foreach (Node n in rootNode.GetChildren())
		{
			SetPauseScene(n, pause);
		}
	}

	public void OnSpiritArtefactActivated(bool spirit, bool flavour= true)
	{
		if (spirit)
		{
			if (flavour)
				_lblEventInfo.QueueText("You become a spirit, and you see the world transform before your very eyes!", 1);
			GetNode<AnimationPlayer>("HUD/SpiritOverlay/Anim").Play("Start");
			GetNode<AnimationPlayer>("SpiritWorldAnim").Play("EnterSpiritWorld");
			foreach (Actor monster in _monsters)
			{
				// monster.Visible = true;
				monster.GetNode<MonsterBody>("Body").InSpiritWorld = true;
			}
			if (HasNode("SpiritArtefact/SpiritArtefactTreasure"))
			{
				SetPauseNode(GetNode<Treasure>("SpiritArtefact/SpiritArtefactTreasure"), true);
			}
			foreach (Node n in GetNode("Treasures").GetChildren())
			{
				SetPauseNode(n, true);
			}
			foreach (Node n in GetNode("NPCs").GetChildren())
			{
				SetPauseNode(n, true);
			}
			foreach (Node n in GetNode("Potions").GetChildren())
			{
				SetPauseNode(n, false);
			}
			foreach (Node n in GetNode("PowerOrbs").GetChildren())
			{
				SetPauseNode(n, false);
			}
			foreach (Node n in GetNode("SpiritOrbs").GetChildren())
			{
				SetPauseNode(n, false);
			}
			_player.ManaRegenMultiplier = 1;

		}
		else
		{
			if (flavour)
				_lblEventInfo.QueueText("Sigh. Back to humanity and its mundane trappings...", 1);
			foreach (Actor monster in _monsters)
			{
				// monster.Visible = false;
				monster.GetNode<MonsterBody>("Body").InSpiritWorld = false;
			}
			// hide "spirit spells".. OR.. SPELLFIZZLES
			// SetPauseScene(GetNode("Treasures"), true);
			if (HasNode("SpiritArtefact/SpiritArtefactTreasure"))
			{
				SetPauseNode(GetNode<Treasure>("SpiritArtefact/SpiritArtefactTreasure"), false);
			}
			foreach (Node n in GetNode("Treasures").GetChildren())
			{
				SetPauseNode(n, false);
			}
			foreach (Node n in GetNode("NPCs").GetChildren())
			{
				SetPauseNode(n, false);
			}
			foreach (Node n in GetNode("Potions").GetChildren())
			{
				SetPauseNode(n, true);
			}
			foreach (Node n in GetNode("PowerOrbs").GetChildren())
			{
				SetPauseNode(n, true);
			}
			foreach (Node n in GetNode("SpiritOrbs").GetChildren())
			{
				SetPauseNode(n, true);
			}
			_player.ManaRegenMultiplier = 4;

			GetNode<ProgressBar>("HUD/CntIndicators/BossHealthBar").Visible = false;
			GetNode<AnimationPlayer>("SpiritWorldAnim").PlayBackwards("EnterSpiritWorld");
			GetNode<AnimationPlayer>("HUD/SpiritOverlay/Anim").Play("End");
		}

		// GetNode<HUD>("HUD").SelectAnyButArtefact();
	}

	public void SetAllToPhysicalWorld()
	{
		foreach (Actor monster in _monsters)
		{
			// monster.Visible = false;
			monster.GetNode<MonsterBody>("Body").InSpiritWorld = false;
		}
		// hide "spirit spells".. OR.. SPELLFIZZLES
		// SetPauseScene(GetNode("Treasures"), true);
		foreach (Node n in GetNode("Treasures").GetChildren())
		{
			SetPauseNode(n, true);
		}
		foreach (Node n in GetNode("NPCs").GetChildren())
		{
			SetPauseNode(n, false);
		}
		foreach (Node n in GetNode("Potions").GetChildren())
		{
			SetPauseNode(n, true);
		}
		foreach (Node n in GetNode("PowerOrbs").GetChildren())
		{
			SetPauseNode(n, true);
		}
		foreach (Node n in GetNode("SpiritOrbs").GetChildren())
		{
			SetPauseNode(n, true);
		}
	}

	// public void OnCheckEngaged(ActorSpellManager spellManager)
	// {
	// 	spellManager.EngagedByMonsters = EngagedByMonsters();
	// }

	public override void _PhysicsProcess(float delta)
	{
		base._PhysicsProcess(delta);
		_player.GetNode<ActorSpellManager>("SpellManager").EngagedByMonsters = EngagedByMonsters();

		foreach (Actor actor in _monsters)
		{
			// GD.Print(actor.Position.DistanceTo(_player.Position));
			if (actor.Position.DistanceTo(_player.Position) > 1000)
			{
				SetPauseNode(actor, true);
				// if (actor.GetInputManager() is MonsterInputManager monsterInputManager)
				// {
				// 	monsterInputManager.SetAIState(MonsterInputManager.AIState.Idle);
				// }
			}
			else
			{
				SetPauseNode(actor, false);
			}
		}
		// foreach (Area2D area in GetNode("Potions").GetChildren())
		// {
		// 	if (area.Position.DistanceTo(_player.Position) > 1000)
		// 	{
		// 		SetPauseNode(area, true);
				
		// 	}
		// 	else
		// 	{
		// 		SetPauseNode(area, false);
		// 	}
			
		// }
		// foreach (Area2D area in GetNode("PowerOrbs").GetChildren())
		// {
		// 	if (area.Position.DistanceTo(_player.Position) > 1000)
		// 	{
		// 		SetPauseNode(area, true);
		// 	}
		// 	else
		// 	{
		// 		SetPauseNode(area, false);
		// 	}
		// }
		// foreach (Area2D area in GetNode("SpiritOrbs").GetChildren())
		// {
		// 	if (area.Position.DistanceTo(_player.Position) > 1000)
		// 	{
		// 		SetPauseNode(area, true);
		// 	}
		// 	else
		// 	{
		// 		SetPauseNode(area, false);
		// 	}
		// }
		
	}

	

	public bool EngagedByMonsters()
	{
		foreach (Actor m in _monsters)
		{
			if (m.GetInputManager() is MonsterInputManager monsterInputManager)
			{
				if (monsterInputManager.GetAIState() == MonsterInputManager.AIState.Pursue || monsterInputManager.GetAIState() == MonsterInputManager.AIState.Attack)
				{
					// GD.Print ( monsterInputManager.GetAIState().ToString());
					return true;
				}
			}
		}
		return false;
	}

	public void OnBossEngaged()
	{
		_lblEventInfo.QueueText("What is this monstrosity?! Destroy it!", 1);
		GetNode<WorldSpellManager>("WorldSpellManager").ArtefactDisabled = true;
	}

	public void OnBossTookDamage(float health, float maxHealth)
	{
		if (health <= 0)
		{
			GetNode<WorldSpellManager>("WorldSpellManager").ArtefactDisabled = false;
		}
	}

	public void OnPlayerBlinkHintedStart(Vector2 target, float distance, bool visible) // this is run whenever the mouse is moved and blinkselected
	{
		GetNode<BlinkIndicatorArea>("BlinkIndicatorArea").Visible = visible;
		Vector2 tar = _player.Position*2+ ((target/2 - _player.Position).Normalized()* distance);
		if (_player.Position.y*2 - tar.y > 250)
		{
			distance += 250 - (_player.Position.y*2 - tar.y);
		}
		if (_player.Position.y*2 - tar.y < -250)
		{
			distance += (_player.Position.y*2 - tar.y) + 250;
		}
		OnPlayerBlinkHinted(_player.Position*2, (target/2 - _player.Position).Normalized(), distance);
	}

	public async void OnPlayerBlinkHinted(Vector2 origin, Vector2 direction, float distance)
	{

		Vector2 newPos = origin + (direction * distance);
		Vector2 mapCoords = GetNode<TileMap>("TileMap").WorldToMap(newPos);
		int cell = GetNode<TileMap>("TileMap").GetCell((int)mapCoords.x, (int)mapCoords.y);
		if (cell == -1)
		{
			GetNode<BlinkIndicatorArea>("BlinkIndicatorArea").Position = newPos;
		}
		else
		{
			OnPlayerBlinkHinted(origin, direction, distance-25);
		}


		await ToSignal (GetTree(), "idle_frame");

	}

	public async void OnTriedToBlink(Actor actor, int ID, Vector2 targetPos, float distance, bool throughwalls)
	{
		//needs changing if more than player can blink
		if (actor.GetNode<IActorBody>("Body") is MonsterBody)
		{
			Vector2 originalPos = actor.Position;
			Vector2 dirToTarget = ((targetPos/2f) - actor.Position).Normalized();
			actor.Position += dirToTarget * distance;

			await ToSignal (GetTree(), "idle_frame");

			if (!throughwalls)
			{
				foreach(RayCast2D ray in actor.GetNode<IActorBody>("Body").CollisionRays)
				{
					if (ray.IsColliding())
					{
						actor.Position = originalPos;
						if (distance <= 0)
						{
							return;
						}
						OnTriedToBlink(actor, _player.ID, targetPos, distance-75, throughwalls);
						return;
					}
				}
			}
		}
		else
		{
			actor.Position = GetNode<BlinkIndicatorArea>("BlinkIndicatorArea").Position/2; // this is the new line
			CorrectPlayerBlink();
		}
		actor.Blinked();
	}

	public async void CorrectPlayerBlink(int randIncrement = 1)
	{
		foreach(RayCast2D ray in _player.GetNode<IActorBody>("Body").CollisionRays)
		{
			if (ray.IsColliding())
			{
				_player.Position += new Vector2(new Random().Next(-randIncrement,randIncrement+1), new Random().Next(-randIncrement,randIncrement+1));
				GD.Print("Correcting blink due to collision");
				await ToSignal (GetTree(), "idle_frame");
				CorrectPlayerBlink(randIncrement+1);
				return;
			}
		}
	}

	public Actor AddMonster(Vector2 pos)
	{
		Actor actor = GetNode<ActorFactory>("ActorFactory").CreateMonster(pos);
		// actor.Modulate = new Color(1,0,0,0.5f);
		// actor.OriginalColor = actor.Modulate;
		actor.Name = "Monster";
		AddActorCommon(actor);
		actor.GetNode<MonsterBody>("Body").Died+=this.OnMonsterDied;
		actor.GetNode<MonsterBody>("Body").DiedAnimCompleted+=this.OnMonsterDiedPostAnim;
		actor.GetNode<MonsterBody>("Body").SpellCast+=GetNode<WorldSpellManager>("WorldSpellManager").OnSpellCast;
		if (actor.GetInputManager() is MonsterInputManager monsterInput)
		{
			monsterInput.LookedForPlayer+=this.OnMonsterLookedForPlayer;
			monsterInput.FiredBullet+=this.OnFiredBullet;
			monsterInput.GhostSpawned+=this.OnGhostSpawned;
		}
		return actor;
	}

	public void OnGhostSpawned(int monsterID)
	{
		Vector2 position = _monsters.Find(x => x.ID == monsterID).Position;
		Actor m = AddMonster(position);
		m.GetNode<MonsterBody>("Body").SetPower(MonsterBody.PowerLevel.One, GameDifficulty);
		m.GetNode<MonsterBody>("Body").InSpiritWorld = true;
		m.GetNode<AnimationPlayer>("Body/BlinkAnim").Play("Blink");
		m.GetNode<MonsterBody>("Body").PlaySound(m.GetNode<MonsterBody>("Body").GhostSounds[AudioData.Ghost.Spawn], AudioData.SoundBus.Voice,
			6);
		_monsters.Add(m);
	}

	public void OnPlayerDied()
	{
		GetNode<HUD>("HUD").OnPlayerDied(); // hides the boss health bar
		_pictureStory.Reset();
		_pictureStory.AddScreen("res://Stages/World/PictureStory/defeat.png", 
		@"You are dead. But are you really dead? Is not death the first step towards eternal life?
		(You have lost your progress since the last checkpoint).");

		_pictureStory.Start();
		_pictureStory.PictureStoryFinished+=this.OnPlayerDiedStoryFinished;
	}

	public void OnPlayerDiedStoryFinished()
	{
		// _player.SetPhysicsProcess(true);
		// _player.Visible = true;
		// _player.GetNode<CollisionShape2D>("Body/Shape").Disabled = false;
		_player.GetNode<PlayerBody>("Body").RotationDegrees = 0;
		_player.GetNode<PlayerBody>("Body").Scale = new Vector2(1,1);
		_player.GetNode<PlayerBody>("Body").OnSpawn();
		
		_player.SetActionState(Actor.ActionState.Idle);
		_player.DiedOnce = false;
		OnCheckPointLoad();
	}

	public void OnMonsterDiedPostAnim(MonsterBody.PowerLevel level)
	{
		if (level == MonsterBody.PowerLevel.Boss)
		{
			GetNode<HUD>("HUD").OnBossDied();
			EndGame();
		}
	}

	public void OnMonsterDied(float _experienceDropped, Vector2 origin, MonsterBody.PowerLevel level, float power)
	{
		ExperienceOrbs orbs = (ExperienceOrbs) GD.Load<PackedScene>(SceneData.Scenes[SceneData.Scene.ExperienceOrbs]).Instance();
		orbs.Init();
		orbs.PickupBehaviour.LookedForPlayer+=this.OnLookedForPlayer;
		// orbs.LookedForPlayer+=this.OnLookedForPlayer;
		orbs.Position = origin*2;
		orbs.PickupBehaviour.Target = _player.Position*2;
		// orbs.Target = _player.Position*2;
		orbs.ExperienceValue = _experienceDropped;
		GetNode("SpiritOrbs").CallDeferred( "add_child", orbs);
		orbs.SetDeferred( "owner", GetNode("SpiritOrbs"));

		if (level != MonsterBody.PowerLevel.One)
		{
			PowerOrbs pOrbs = (PowerOrbs) GD.Load<PackedScene>(SceneData.Scenes[SceneData.Scene.PowerOrbs]).Instance();
			pOrbs.Init(power/5f);
			pOrbs.PickupBehaviour.LookedForPlayer+=this.OnLookedForPlayer;
			// orbs.LookedForPlayer+=this.OnLookedForPlayer;
			pOrbs.Position = origin*2;
			pOrbs.PickupBehaviour.Target = _player.Position*2;
			GetNode("PowerOrbs").CallDeferred( "add_child", pOrbs);
			pOrbs.SetDeferred( "owner", GetNode("PowerOrbs"));
		}

		if (new Random().Next(0,100) > 90)
		{
			GenerateRandomPotion(origin*2).PickupBehaviour.Paused = false;
		}

		// if (level == MonsterBody.PowerLevel.Boss)
		// {
		// 	GetNode<HUD>("HUD").OnBossDied();
		// 	EndGame();
		// }
		// orbs.Start(_experienceDropped);
	}


	public void GeneratePowerOrbs(Vector2 origin, float powerValue)
	{
		PowerOrbs pOrbs = (PowerOrbs) GD.Load<PackedScene>(SceneData.Scenes[SceneData.Scene.PowerOrbs]).Instance();
		pOrbs.Init(powerValue/5f);
		pOrbs.PickupBehaviour.LookedForPlayer+=this.OnLookedForPlayer;
		// orbs.LookedForPlayer+=this.OnLookedForPlayer;
		pOrbs.Position = origin;
		pOrbs.PickupBehaviour.Target = _player.Position*2;
		GetNode("PowerOrbs").CallDeferred( "add_child", pOrbs);
		pOrbs.SetDeferred( "owner", GetNode("PowerOrbs"));
		pOrbs.PickupBehaviour.Paused = true;
	}

	

	public void GenerateExpOrbs(Vector2 origin, float expValue)
	{
		ExperienceOrbs orbs = (ExperienceOrbs) GD.Load<PackedScene>(SceneData.Scenes[SceneData.Scene.ExperienceOrbs]).Instance();
		orbs.Init();
		orbs.PickupBehaviour.LookedForPlayer+=this.OnLookedForPlayer;
		// orbs.LookedForPlayer+=this.OnLookedForPlayer;
		orbs.Position = origin;
		orbs.PickupBehaviour.Target = _player.Position*2;
		// orbs.Target = _player.Position*2;
		orbs.ExperienceValue = expValue;
		GetNode("SpiritOrbs").CallDeferred( "add_child", orbs);
		orbs.SetDeferred( "owner", GetNode("SpiritOrbs"));
		orbs.PickupBehaviour.Paused = true;
		
	}

	public IPickupable GenerateRandomPotion(Vector2 origin)
	{
		// IPickupable potion;
		if (new Random().Next(0,2) == 0)
		{
			return GeneratePotion(origin, true);
			// potion = (IPickupable) GD.Load<PackedScene>(SceneData.Scenes[SceneData.Scene.HealthPotion]).Instance();
			// potion.Init();
			// potion.PickupBehaviour.LookedForPlayer+=this.OnLookedForPlayer;
			// potion.Position = origin*2;
			// potion.PickupBehaviour.Target = _player.Position*2;

		}
		else
		{
			return GeneratePotion(origin, false);
			// potion = (IPickupable) GD.Load<PackedScene>(SceneData.Scenes[SceneData.Scene.ManaPotion]).Instance();
			// potion.Init();
			// potion.PickupBehaviour.LookedForPlayer+=this.OnLookedForPlayer;
			// potion.Position = origin*2;
			// potion.PickupBehaviour.Target = _player.Position*2;
		}
		// potion.Init();
		// potion.PickupBehaviour.LookedForPlayer+=this.OnLookedForPlayer;
		// potion.Position = origin*2;
		// potion.PickupBehaviour.Target = _player.Position*2;
		// GetNode("Potions").CallDeferred( "add_child", (Node)potion);
		// ((Node)potion).SetDeferred( "owner", GetNode("Potions"));
		
	}

	public IPickupable GeneratePotion(Vector2 origin, bool heal)
	{
		string path = SceneData.Scenes[SceneData.Scene.HealthPotion];
		if (!heal)
		{
			path = SceneData.Scenes[SceneData.Scene.ManaPotion];
		}
		IPickupable potion = (IPickupable) GD.Load<PackedScene>(path).Instance();
		potion.Init();
		potion.PickupBehaviour.LookedForPlayer+=this.OnLookedForPlayer;
		potion.Position = origin;
		potion.PickupBehaviour.Target = _player.Position*2;
		GetNode("Potions").CallDeferred( "add_child", (Node)potion);
		((Node)potion).SetDeferred( "owner", GetNode("Potions"));
		potion.PickupBehaviour.Paused = true;
		return potion;
	}

	public void AddActorCommon(Actor actor)
	{
		
		if (actor.GetNode("Body") is PlayerBody)
		{
			AddChild(actor);
			actor.Owner = this;
		}
		else
		{
			GetNode("Monsters").AddChild(actor);
			actor.Owner = GetNode("Monsters");
		}
		actor.GetNode<ActorDamageHandler>("DamageHandler").Actor = actor;
		actor.Disposed+=this.OnActorDisposed;
		actor.TriedToBlink+=this.OnTriedToBlink;

		// actor.GetNode<ActorDamageHandler>("DamageHandler").ActorStatChanged+=actor.OnActorStatChanged;
	}

	public void OnActorDisposed(Actor actor)
	{
		if (_monsters.Contains(actor))
		{
			_monsters.Remove(actor);
		}
		actor.QueueFree();
	}

	public void OnFiredBullet(int actorID, Vector2 origin, bool berserk)
	{
		if (! IsPlayer(actorID))
		{
			MonsterBullet b = (MonsterBullet) GD.Load<PackedScene>(SceneData.Scenes[SceneData.Scene.MonsterBullet]).Instance();
			b.Damage = _monsters.Find(x => x.ID == actorID).ActorStats["Power"];
			AddChild(b);
			b.Owner = this;
			b.Originator = _monsters.Find(x => x.ID == actorID).GetNode<MonsterBody>("Body");
			b.Position = origin*2;
			b.Berserk = berserk;
			b.Start();
		}
	}

	public void OnSpellCast(ISpellEffect spellEffect, ActorSpellManager.Spell spell, bool sourcePlayer)
	{
		if (spellEffect is SpellHeal spellHeal)
		{
			spellHeal.LookedForCaster+=this.OnLookedForCaster;
		}
		// GD.Print(spellEffect.Cooldown);
		if (sourcePlayer)
		{
			GetNode<HUD>("HUD").StartSpellCooldown(spell, spellEffect.Cooldown);
		}

	}

	public void OnLookedForCaster(SpellHeal spellHeal)
	{
		spellHeal.AnimTarget=_player.Position*2;
	}

	public void OnLookedForPlayer(PickupBehaviour pickupbehaviour)//ExperienceOrbs orbs)
	{
		if (_player.CurrentActionState == Actor.ActionState.Dying)
		{
			return;
		}
		pickupbehaviour.Target = _player.Position*2;
		// orbs.Target = _player.Position*2;
	}

	public bool IsPlayer(int actorID)
	{
		return _player.ID == actorID;
	}

	public void OnMonsterLookedForPlayer(int monsterID, bool berserk, bool playerOnly)
	{
		if (berserk)
		{
			Actor monster = _monsters.Find(x => x.ID == monsterID);
			List<Actor> sortedMonsters = _monsters.OrderBy(o => monster.Position.DistanceTo(o.Position)).ToList();
			if (sortedMonsters.Count <= 1) // this is the last monster in the list 
			{
				monster.GetNode<MonsterBody>("Body").Berserk = false;
				return;
			}
			if (monster.Position.DistanceTo(sortedMonsters[1].Position) > 200) // the next monster in the list is too far
			{
				monster.GetNode<MonsterBody>("Body").Berserk = false;
				return;
			}
			if (playerOnly)
			{
				monster.GetInputManager().PlayerTarget = sortedMonsters[1].Position;
				return;
			}
			monster.GetInputManager().PlayerTarget = monster.GetInputManager().Target = sortedMonsters[1].Position;
			
			return;
			// List<Order> SortedList = objListOrder.OrderBy(o=>o.OrderDate).ToList();
		}
		if (_player == null)
		{
			return;
		}
		if (_player.CurrentActionState != Actor.ActionState.Dying)
		{
			((MonsterInputManager) _monsters.Find(x => x.ID == monsterID).GetInputManager()).PlayerTarget = _player.Position;
			if (!playerOnly)
			{
				((MonsterInputManager) _monsters.Find(x => x.ID == monsterID).GetInputManager()).Target = _player.Position;
			}
		}
		else
		{
			((MonsterInputManager) _monsters.Find(x => x.ID == monsterID).GetInputManager()).PlayerTarget = new Vector2(-10000, -10000);
			if (!playerOnly)
			{
				((MonsterInputManager) _monsters.Find(x => x.ID == monsterID).GetInputManager()).Target = new Vector2(-10000, -10000);
			}
		}
	}

	private void OnPnlSpellBtnsMouseEntered()
	{
		_player.GetInputManager().MouseEnabled=false;
		// GD.Print(_player.GetInputManager().MouseEnabled);
	}


	private void OnPnlSpellBtnsMouseExited()
	{
		_player.GetInputManager().MouseEnabled=true;
	}

	public void PlayWorldSound(AudioData.World soundData)
	{
		if (_soundPlayer.Playing)
		{
			if (_soundPlayer.Stream == WorldSounds[soundData])
			{
				GD.Print("not playing world sound");
				return;
			}
		}
		GD.Print("plauing world sound");
		AudioHandler.PlaySound(_soundPlayer, WorldSounds[soundData], AudioData.SoundBus.Effects);
	}

	public void OnCheckPointSave(CheckPoint checkPoint = null)
	{
		// dont save the checkpoint if we 
		foreach (Actor m in _monsters)
		{
			if (m.GetInputManager() is MonsterInputManager monsterInputManager)
			{
				if (monsterInputManager.GetAIState() == MonsterInputManager.AIState.Pursue || monsterInputManager.GetAIState() == MonsterInputManager.AIState.Attack)
				{
					return;
				}
			}
		}
		if (checkPoint != null)
		{
			checkPoint.StartAnim();
			checkPoint.GetNode<Timer>("Timer").Start();
			_lblEventInfo.QueueText("The spirits of this place protect you. You will return if defeated.", 1);
		}

			// {StatThatUpdates.Fireball, GetSpellLevel(ActorSpellManager.Spell.Fireball)},
			// {StatThatUpdates.FireBolt, GetSpellLevel(ActorSpellManager.Spell.FireBolt)},
			// {StatThatUpdates.Berserk, GetSpellLevel(ActorSpellManager.Spell.Berserk)},
			// {StatThatUpdates.Blink, GetSpellLevel(ActorSpellManager.Spell.Blink)},
			// {StatThatUpdates.Heal, GetSpellLevel(ActorSpellManager.Spell.Heal)},
		Dictionary<string, float> playerStats = new Dictionary<string, float>();
		foreach (KeyValuePair<string, float> kv in _player.ActorStats)
		{
			playerStats.Add(kv.Key, kv.Value);
		}
		playerStats["HealPots"] = _player.GetNode<ActorDamageHandler>("DamageHandler").HealPots;
		playerStats["ManaPots"] = _player.GetNode<ActorDamageHandler>("DamageHandler").ManaPots;
		playerStats["Experience"] = _player.ExperienceManager.GetUpdatedStats()[ActorExperienceManager.StatThatUpdates.Experience];
		playerStats["Level"] = _player.ExperienceManager.GetUpdatedStats()[ActorExperienceManager.StatThatUpdates.Level];
		playerStats["SkillPoints"] = _player.ExperienceManager.GetUpdatedStats()[ActorExperienceManager.StatThatUpdates.SkillPoints];


		Dictionary<ActorSpellManager.Spell, int> playerSpellLevels = new Dictionary<ActorSpellManager.Spell, int>()
		{
			{ActorSpellManager.Spell.FireBolt, _player.ExperienceManager.GetSpellLevel(ActorSpellManager.Spell.FireBolt)},
			{ActorSpellManager.Spell.Fireball, _player.ExperienceManager.GetSpellLevel(ActorSpellManager.Spell.Fireball)},
			{ActorSpellManager.Spell.Heal, _player.ExperienceManager.GetSpellLevel(ActorSpellManager.Spell.Heal)},
			{ActorSpellManager.Spell.Berserk, _player.ExperienceManager.GetSpellLevel(ActorSpellManager.Spell.Berserk)},
			{ActorSpellManager.Spell.Blink, _player.ExperienceManager.GetSpellLevel(ActorSpellManager.Spell.Blink)}
		};

		List<ActorSpellManager.Spell> playerKnownSpells = _player.GetNode<ActorSpellManager>("SpellManager").GetKnownSpells().ToList();

		Vector2 playerPos = _player.Position;

		List<Tuple<Vector2, MonsterBody.PowerLevel>> allMonsterData = new List<Tuple<Vector2, MonsterBody.PowerLevel>>();
		foreach (Actor m in GetNode("Monsters").GetChildren())
		{
			Tuple<Vector2, MonsterBody.PowerLevel> monsterData = new Tuple<Vector2, MonsterBody.PowerLevel>(m.OriginalPos, m.GetNode<MonsterBody>("Body").Level);
			allMonsterData.Add(monsterData);
		}

		List<Tuple<Vector2, Treasure.TreasureType>> allTreasureData = new List<Tuple<Vector2, Treasure.TreasureType>>();
		foreach (Treasure t in GetNode("Treasures").GetChildren())
		{
			Tuple<Vector2, Treasure.TreasureType> treasureData = new Tuple<Vector2, Treasure.TreasureType>(t.Position, t.GetTreasureType());
			allTreasureData.Add(treasureData);
		}


		List<Tuple<Vector2, bool>> allPotionData = new List<Tuple<Vector2, bool>>();
		foreach (Area2D potion in GetNode("Potions").GetChildren())
		{
			Tuple<Vector2, bool> potionData = new Tuple<Vector2, bool>(potion.Position, potion is HealPotion ? true : false);
			allPotionData.Add(potionData);
		}

		List<Tuple<Vector2, float>> allPowerOrbsData = new List<Tuple<Vector2, float>>();
		foreach (PowerOrbs orbs in GetNode("PowerOrbs").GetChildren())
		{
			Tuple<Vector2, float> powerorbsData = new Tuple<Vector2, float>(orbs.Position, orbs.PowerValue);
			allPowerOrbsData.Add(powerorbsData);
		}
		List<Tuple<Vector2, float>> allExpOrbsData = new List<Tuple<Vector2, float>>();
		foreach (ExperienceOrbs orbs in GetNode("SpiritOrbs").GetChildren())
		{
			Tuple<Vector2, float> spiritOrbsData = new Tuple<Vector2, float>(orbs.Position, orbs.ExperienceValue);
			allExpOrbsData.Add(spiritOrbsData);
		}

		_lastCheckpointData = new CheckpointData();
		foreach (Treasure t in GetNode<Node2D>("SpiritArtefact").GetChildren())
		{
			_lastCheckpointData.SetArtefact(t.Position);
		}
		_lastCheckpointData.SetPlayerData(playerStats, playerKnownSpells, playerSpellLevels, playerPos);
		_lastCheckpointData.SetMonsterData(allMonsterData);	
		_lastCheckpointData.SetDialogueHistory(GetNode<DialogueControl>("HUD/DialogueControl").DialogHistory.ToDictionary(entry => entry.Key,
											   entry => entry.Value));
		_lastCheckpointData.SetTreasureData(allTreasureData);
		_lastCheckpointData.SetPotionData(allPotionData);
		_lastCheckpointData.SetPowerOrbsData(allPowerOrbsData);
		_lastCheckpointData.SetExpOrbsData(allExpOrbsData);

	}

	private CheckpointData _lastCheckpointData;

	public void OnCheckPointLoad()
	{
		if (_lastCheckpointData == null)
		{
			GD.Print("ERROR checkpoint is null");
			return;
		}
		_lblEventInfo.QueueText("You return to the land of the living. There is still much to be done...", 1);
		_lastCheckpointData.SetPlayerFromData(_player);

		foreach (Actor m in GetNode("Monsters").GetChildren())
		{
			m.DisposeActor();
		}

		foreach (Tuple<Vector2, MonsterBody.PowerLevel> monsterData in _lastCheckpointData.GetMonsterData())
		{
			if (monsterData.Item2 == MonsterBody.PowerLevel.One)
			{
				SpawnMonster1(monsterData.Item1);
			}
			else if (monsterData.Item2 == MonsterBody.PowerLevel.Two)
			{
				SpawnMonster2(monsterData.Item1);
			}
			else if (monsterData.Item2 == MonsterBody.PowerLevel.Three)
			{
				SpawnMonster3(monsterData.Item1);
			}
			else if (monsterData.Item2 == MonsterBody.PowerLevel.Boss)
			{
				SpawnMonsterBoss(monsterData.Item1);
			}
		}
		foreach (Treasure t in GetNode("Treasures").GetChildren())
		{
			t.QueueFree();
		}

		foreach (Tuple<Vector2, Treasure.TreasureType> treasureData in _lastCheckpointData.GetTreasureData())
		{
			string scnPath = SceneData.Scenes[SceneData.Scene.BerserkTreasure];
			switch (treasureData.Item2)
			{
				case Treasure.TreasureType.Berserk:
					scnPath = SceneData.Scenes[SceneData.Scene.BerserkTreasure];
					break;
				case Treasure.TreasureType.Blink:
					scnPath = SceneData.Scenes[SceneData.Scene.BlinkTreasure];
					break;
				case Treasure.TreasureType.Heal:
					scnPath = SceneData.Scenes[SceneData.Scene.HealTreasure];
					break;
				case Treasure.TreasureType.Fireball:
					scnPath = SceneData.Scenes[SceneData.Scene.FireballTreasure];
					break;
				case Treasure.TreasureType.Firebolt:
					scnPath = SceneData.Scenes[SceneData.Scene.FireboltTreasure];
					break;
				case Treasure.TreasureType.SpiritArtefact:
					scnPath = SceneData.Scenes[SceneData.Scene.SpiritArtefact];
					break;
			}

			PackedScene treasureScn = (PackedScene) GD.Load(scnPath);
			Treasure t = (Treasure)treasureScn.Instance();
			t.SetTreasureType(treasureData.Item2);
			t.Position = treasureData.Item1;
			GetNode("Treasures").AddChild(t);
		}
		
		foreach (Area2D potion in GetNode("Potions").GetChildren())
		{
			potion.QueueFree();
		}

		foreach (Tuple<Vector2, bool> potionData in _lastCheckpointData.GetPotionData())
		{
			GeneratePotion(potionData.Item1, potionData.Item2);
		}
		foreach (PowerOrbs orbs in GetNode("PowerOrbs").GetChildren())
		{
			orbs.QueueFree();
		}

		foreach (Tuple<Vector2, float> powerOrbsData in _lastCheckpointData.GetPowerOrbsData())
		{
			GeneratePowerOrbs(powerOrbsData.Item1, powerOrbsData.Item2*5);
		}
		foreach (ExperienceOrbs orbs in GetNode("SpiritOrbs").GetChildren())
		{
			orbs.QueueFree();
		}

		foreach (Tuple<Vector2, float> expOrbsData in _lastCheckpointData.GetExpOrbsData())
		{
			GenerateExpOrbs(expOrbsData.Item1, expOrbsData.Item2);
		}

		foreach (Treasure t in GetNode<Node2D>("SpiritArtefact").GetChildren())
		{
			t.QueueFree();
		}

		foreach (Vector2 artefactPos in _lastCheckpointData.GetArtefactData())
		{
			PackedScene treasureScn = (PackedScene) GD.Load(SceneData.Scenes[SceneData.Scene.SpiritArtefact]);
			Treasure t = (Treasure)treasureScn.Instance();
			t.SetTreasureType(Treasure.TreasureType.SpiritArtefact);
			t.Position = artefactPos;
			GetNode("SpiritArtefact").AddChild(t);
		}
		// foreach (ExperienceOrbs orbs in GetNode("SpiritOrbs").GetChildren())
		// {
		// 	// SetPauseNode
		// 	orbs.PickupBehaviour.Paused = true;
		// 	GD.Print("pausing pickup behaviour for exp orbs");
		// }
		
		GetNode<DialogueControl>("HUD/DialogueControl").DialogHistory = _lastCheckpointData.GetDialogueHistory();

		if (_player.GetNode<PlayerBody>("Body").Spirit)
		{
			_player.GetNode<PlayerBody>("Body").ActivateSpiritArtefact(false, false);
		}
		// OnSpiritArtefactActivated(false);
		// SetAllToPhysicalWorld();
		GetNode<HUD>("HUD").HideAllSpellButtons();
		GetNode<HUD>("HUD").EnableAllSpellButtons();
		foreach (ActorSpellManager.Spell spell in _player.GetNode<ActorSpellManager>("SpellManager").GetKnownSpells())
		{
			GetNode<HUD>("HUD").ShowSpellButton(spell);
		}
		GetNode<WorldSpellManager>("WorldSpellManager").ArtefactDisabled = false;
		_player.GetNode<ActorSpellManager>("SpellManager").SetActiveSpell(ActorSpellManager.Spell.Empty);

	}

	// public override void _Input(InputEvent ev) // testing only
	// {
	// 	base._Input(ev);

	// 	if (ev.IsActionPressed("TestSave"))
	// 	{
	// 		// OnCheckPointSave();
	// 	}
	// 	if (ev.IsActionPressed("TestLoad"))
	// 	{
	// 		OnCheckPointLoad();
	// 	}
	// }

	public float GameDifficulty {get; set;} = 1; // 0.2, 0.5, 1, 1.5
	public Actor SpawnMonsterCommon(Vector2 pos, MonsterBody.PowerLevel level)
	{
		Actor m = AddMonster(pos);
		m.GetNode<MonsterBody>("Body").SetPower(level, GameDifficulty);
		_monsters.Add(m);
		return m;
	}

	public void SpawnMonster1(Vector2 pos)
	{
		SpawnMonsterCommon(pos, MonsterBody.PowerLevel.One);
	}
	public void SpawnMonster2(Vector2 pos)
	{
		SpawnMonsterCommon(pos, MonsterBody.PowerLevel.Two);
	}
	public void SpawnMonster3(Vector2 pos)
	{
		SpawnMonsterCommon(pos, MonsterBody.PowerLevel.Three);
	}
	public void SpawnMonsterBoss(Vector2 pos)
	{
		Actor m = SpawnMonsterCommon(pos, MonsterBody.PowerLevel.Boss);
		m.GetNode<MonsterBody>("Body").TookDamage+=GetNode<HUD>("HUD").OnBossTookDamage;
		m.GetNode<MonsterBody>("Body").TookDamage+=this.OnBossTookDamage;
		m.GetNode<MonsterBody>("Body").BossEngaged+=this.OnBossEngaged;
	}
}

		// 	if (n is Position2D position2D)
		// 	{
		// 		Actor m = AddMonster(position2D.Position/2);
		// 		m.GetNode<MonsterBody>("Body").SetPower(MonsterBody.PowerLevel.Boss);
		// 		m.GetNode<MonsterBody>("Body").TookDamage+=GetNode<HUD>("HUD").OnBossTookDamage;
		// 		m.GetNode<MonsterBody>("Body").TookDamage+=this.OnBossTookDamage;
		// 		m.GetNode<MonsterBody>("Body").BossEngaged+=this.OnBossEngaged;
		// 		_monsters.Add(m);
		// 	}
		// }

			// foreach (Node n in GetNode("Treasures").GetChildren())
			// {
			// 	SetPauseNode(n, true);
			// }
			// foreach (Node n in GetNode("NPCs").GetChildren())
			// {
			// 	SetPauseNode(n, false);
			// }
			// foreach (Node n in GetNode("Potions").GetChildren())
			// {
			// 	SetPauseNode(n, true);
			// }
			// foreach (Node n in GetNode("PowerOrbs").GetChildren())
			// {
			// 	SetPauseNode(n, true);
			// }
			// foreach (Node n in GetNode("SpiritOrbs").GetChildren())
			// {
			// 	SetPauseNode(n, true);
			// }
// var packed_scene = PackedScene.new()
// packed_scene.pack(get_tree().get_current_scene())
// ResourceSaver.save("res://my_scene.tscn", packed_scene)
// # Load the PackedScene resource
// var packed_scene = load("res://my_scene.tscn")
// # Instance the scene
// var my_scene = packed_scene.instance()
// add_child(my_scene)
