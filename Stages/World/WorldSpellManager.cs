// just add the other spells now

using Godot;
using System;
using System.Collections.Generic;

public class WorldSpellManager : Node2D
{

	public delegate void SpellCastDelegate(ISpellEffect spefx, ActorSpellManager.Spell spell, bool sourcePlayer);
	public event SpellCastDelegate SpellCast;

	public enum SpellStat {
		Speed,
		BasePower,
		BaseCooldown

	}
	public bool ArtefactDisabled {get; set;} // IF WE NEED TO GO BACK TO THE REAL WORLD AFTER BOSS DIES, RE-ENABLE THIS ONCE BOSS IS DEAD

	public enum Effect {
		Damage,
		Heal,
		Berserk,
		Blink,
		Spirit
	}

	private Dictionary<ActorSpellManager.Spell, PackedScene> _spellScenes = new Dictionary<ActorSpellManager.Spell, PackedScene>()
	{
		{ActorSpellManager.Spell.FireBolt, (PackedScene)GD.Load("res://Props/Spells/Firebolt/SpellFireBolt.tscn")},
		{ActorSpellManager.Spell.Fireball, (PackedScene)GD.Load("res://Props/Spells/Fireball/SpellFireball.tscn")},
		{ActorSpellManager.Spell.Heal, (PackedScene)GD.Load("res://Props/Spells/Heal/SpellHeal.tscn")},
		{ActorSpellManager.Spell.Berserk, (PackedScene)GD.Load("res://Props/Spells/Berserk/SpellBerserk.tscn")},
		{ActorSpellManager.Spell.Blink, (PackedScene)GD.Load("res://Props/Spells/Blink/SpellBlink.tscn")},
		{ActorSpellManager.Spell.SpiritArtefact, (PackedScene)GD.Load("res://Props/Spells/SpiritArtefact/SpellSpiritArtefact.tscn")},
		
	};

	private Dictionary<ActorSpellManager.Spell, List<Effect>> _spellEffects = new Dictionary<ActorSpellManager.Spell, List<Effect>>()
	{
		{ActorSpellManager.Spell.FireBolt, new List<Effect>() {Effect.Damage}},
		{ActorSpellManager.Spell.Fireball, new List<Effect>() {Effect.Damage}},
		{ActorSpellManager.Spell.Heal, new List<Effect>() {Effect.Heal}},
		{ActorSpellManager.Spell.Berserk, new List<Effect>() {Effect.Berserk}},
		{ActorSpellManager.Spell.Blink, new List<Effect>() {Effect.Blink}},
		{ActorSpellManager.Spell.SpiritArtefact, new List<Effect>() {Effect.Spirit}},
	};


	public Dictionary<ActorSpellManager.Spell, Dictionary<SpellStat, float>> SpellData = new Dictionary<ActorSpellManager.Spell, Dictionary<SpellStat, float>>()
	{
		{ActorSpellManager.Spell.FireBolt, new Dictionary<SpellStat, float>()
			{
				{SpellStat.Speed, 500},
				{SpellStat.BasePower, 1}, // 5
				{SpellStat.BaseCooldown, 0.5f}
			}},
		{ActorSpellManager.Spell.Fireball, new Dictionary<SpellStat, float>()
			{
				{SpellStat.Speed, 400},
				{SpellStat.BasePower, 8}, // 15
				{SpellStat.BaseCooldown, 0.5f}
			}},	
		{ActorSpellManager.Spell.Heal, new Dictionary<SpellStat, float>()
			{
				{SpellStat.Speed, 350},
				{SpellStat.BasePower, 1},
				{SpellStat.BaseCooldown, 0.5f}
			}},	
		{ActorSpellManager.Spell.Berserk, new Dictionary<SpellStat, float>()
			{
				{SpellStat.Speed, 400},
				{SpellStat.BasePower, 10},
				{SpellStat.BaseCooldown, 0.5f}
			}},	
		{ActorSpellManager.Spell.Blink, new Dictionary<SpellStat, float>()
			{
				{SpellStat.Speed, 400},
				{SpellStat.BasePower, 10},
				{SpellStat.BaseCooldown, 0.5f}
			}},	
		{ActorSpellManager.Spell.SpiritArtefact, new Dictionary<SpellStat, float>()
			{
				{SpellStat.Speed, 400},
				{SpellStat.BasePower, 10},
				{SpellStat.BaseCooldown, 0.5f}
			}},	
	};


	public void OnSpellCast(ActorSpellManager.Spell spell, Vector2 position, float overallPower, Vector2 target, bool sourcePlayer=true)
	{
		if (spell == ActorSpellManager.Spell.SpiritArtefact && ArtefactDisabled)
		{
			return;
		}
		// GD.Print("CFASTED");
		ISpellEffect spefx = (ISpellEffect)_spellScenes[spell].Instance();
		
		spefx.Position = position;
		float cooldown = SpellData[spell][SpellStat.BaseCooldown];
		// if (spell == ActorSpellManager.Spell.Blink)
		// {
		// 	cooldown /= power;
		// }
		spefx.Cooldown = cooldown;
		// spefx.Init(SpellData[spell][SpellStat.BasePower]*(1+spellLevel/10f) + power);
		spefx.Init(overallPower);
		SpellCast?.Invoke(spefx, spell, sourcePlayer);
		AddChild((Node)spefx);
		((Node)spefx).Owner = this;

		// float overallPower = 
		spefx.Start(target, overallPower, SpellData[spell][SpellStat.Speed], _spellEffects[spell], sourcePlayer, spell);

		// spefx.Start(target, SpellData[spell][SpellStat.BasePower]*(1+spellLevel/10f) + power, SpellData[spell][SpellStat.Speed], _spellEffects[spell]);
		// spefx.Position = target;

		// do spell code effect here -> travel in direction of mouse, do damage or whatever. probably do a dict that links to various funcs
	}



	// public float GetFinalPower(ActorSpellManager.Spell spell, int spellLevel, float power)
	// {
	// 	return SpellData[spell][SpellStat.BasePower]*(1+spellLevel/10f) + power;
	// }
	
}
