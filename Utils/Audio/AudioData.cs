// Audio data: contains references to all audio files in the project for convenience
// Contains method for loading required sounds - should load all sounds required for a stage at the start.
using System;
using System.Collections.Generic;
using Godot;

public static class AudioData
{

	public enum SoundBus {
		Voice,
		Effects,
		Music,
		Master
	}

	public static Dictionary<SoundBus, string> SoundBusStrings = new Dictionary<SoundBus, string>()
	{
		{SoundBus.Voice, "Voice"},
		{SoundBus.Effects, "Effects"},
		{SoundBus.Music, "Music"},
		{SoundBus.Master, "Master"}
	};


	public enum ButtonSounds {
		ClickStart
	};


	public enum MainMenuSounds {
		Music
	};

	public enum Ghost {
		Pursue, // ghost6
		Attack1, // ghost2
		Attack2, // ghost2
		Die, // ghostdie
		Hurt1, // not done
		Hurt2, // not done
		Hurt3,
		Spawn

	};

	public enum Player {
		Spawn, // use blink as placeholder
		Hurt1,
		Hurt2,
		Hurt3,
		Die, // not done
		LevelUp,
		Run,
		Jump,
		Land,
		GetItemBig,
		GetItemSmall,
		Gulp1

	};

	public enum NPC {
		Interact1,
		Interact2,
		Interact3,
		Interact4
	}

	public enum Spell {
		BerserkCast,
		BerserkImpact,
		BlinkCast,
		FireballCast,
		// FireballCast2,
		FireballImpact,
		FireboltCast,
		FireboltImpact, // firebolt 2
		HealCast,
		SpiritArtefact, //use generic
		Fizzle
	}

	public enum World{
		Music
	}

	public enum Checkpoint{
		Save
	}

	public static Dictionary<Ghost, string> GhostSoundPaths = new Dictionary<Ghost, string>()
	{
		{Ghost.Attack1, "res://Actors/Monster/Sounds/Ghost4.wav"},
		{Ghost.Attack2, "res://Actors/Monster/Sounds/Ghost4.wav"},
		{Ghost.Pursue, "res://Actors/Monster/Sounds/Ghost6.wav"},
		{Ghost.Die, "res://Actors/Monster/Sounds/GhostDie.wav"},
		{Ghost.Hurt1, "res://Actors/Monster/Sounds/GhostHurt1.wav"},
		{Ghost.Hurt2, "res://Actors/Monster/Sounds/GhostHurt2.wav"},
		{Ghost.Hurt3, "res://Actors/Monster/Sounds/GhostHurt3.wav"},
		{Ghost.Spawn, "res://Props/Spells/Blink/Warp.wav"},
	};
	public static Dictionary<Player, string> PlayerSoundPaths = new Dictionary<Player, string>()
	{
		{Player.Die, "res://Actors/Player/Sounds/LenoreDie.wav"},
		{Player.Hurt1, "res://Actors/Player/Sounds/LenoreHurt1.wav"},
		{Player.Hurt2, "res://Actors/Player/Sounds/LenoreHurt2.wav"},
		{Player.Hurt3, "res://Actors/Player/Sounds/LenoreHurt3.wav"},
		{Player.Jump, "res://Actors/Player/Sounds/JumpStart.ogg"},
		{Player.LevelUp, "res://Actors/Player/Sounds/LevelUp.wav"},
		{Player.Run, "res://Actors/Player/Sounds/RunningGrassv3.ogg"},
		{Player.Spawn, "res://Props/Spells/Blink/Warp.wav"},
		{Player.Land, "res://Actors/Player/Sounds/jumpLandv3.ogg"},
		{Player.GetItemBig, "res://Props/Treasures/GetItem.wav"},
		{Player.GetItemSmall, "res://Props/Treasures/Coin.wav"},
		{Player.Gulp1, "res://Actors/Player/Sounds/Gulp.wav"}
	};
	public static Dictionary<NPC, string> NPCSoundPaths = new Dictionary<NPC, string>()
	{
		{NPC.Interact1, "res://Actors/NPC/Sounds/NPCTalk1.wav"},
		{NPC.Interact2, "res://Actors/NPC/Sounds/NPCTalk2.wav"},
		{NPC.Interact3, "res://Actors/NPC/Sounds/NPCTalk3.wav"},
		{NPC.Interact4, "res://Actors/NPC/Sounds/NPCTalk4.wav"}
	};
	public static Dictionary<Spell, string> SpellSoundPaths = new Dictionary<Spell, string>()
	{
		{Spell.BerserkCast, "res://Props/Spells/Berserk/BerserkSpell2.wav"},
		{Spell.BerserkImpact, "res://Props/Spells/Berserk/BerserkImpact.wav"},
		{Spell.BlinkCast, "res://Props/Spells/Blink/Warp.wav"},
		{Spell.FireballCast, "res://Props/Spells/Fireball/FireballBigwav.wav"},
		{Spell.FireballImpact, "res://Props/Spells/Fireball/FireballImpact.wav"},
		{Spell.FireboltCast, "res://Props/Spells/Firebolt/Fireball.wav"},
		{Spell.FireboltImpact, "res://Props/Spells/Firebolt/Fireball2.wav"},
		{Spell.HealCast, "res://Props/Spells/Heal/HealingSpell.wav"},
		{Spell.SpiritArtefact, "res://Props/Spells/SpiritArtefact/CastSpellGeneric.wav"},
		{Spell.Fizzle, "res://Props/Spells/Fizzle.wav"}
	};

	public static Dictionary<World, string> WorldSoundPaths = new Dictionary<World, string>()
	{

		// {World.Checkpoint, "res://Props/Spells/SpiritArtefact/CastSpellGeneric.ogg"}
		{World.Music, "res://Stages/World/Music/CreepyTown_CrystalGlock.ogg"}
	};
	public static Dictionary<Checkpoint, string> CheckPointSoundPaths = new Dictionary<Checkpoint, string>()
	{

		{Checkpoint.Save, "res://Props/Spells/SpiritArtefact/CastSpellGeneric.ogg"}
	};



	public static Dictionary<ButtonSounds, string> ButtonSoundPaths = new Dictionary<ButtonSounds, string>()
	{
		{ButtonSounds.ClickStart, "res://Common/UI/Buttons/BtnBase/ClickStart.ogg"}
	};

	public static Dictionary<MainMenuSounds, string> MainMenuSoundPaths = new Dictionary<MainMenuSounds, string>()
	{
		{MainMenuSounds.Music, "res://Stages/MainMenu/Music/CreepyTown_GuitarGlock.ogg"}
	};


	// Return a dictionary containing loaded sounds. Useful to do when loading/instancing a scene e.g. player.
	// Usage: Dictionary<AudioData.PlayerSounds, AudioStream> playerSounds = 
	//  AudioData.LoadedSounds<AudioData.PlayerSounds>(AudioData.PlayerSoundPaths);
	// If 2D positional sound is required then add a AudioStreamPlayer2D node as a child and:
	// AudioStreamPlayer2D soundPlayer = (AudioStreamPlayer2D) GetNode("SoundPlayer");
	// // Play sound
	// AudioHandler.PlaySound(soundPlayer, playerSounds[AudioData.PlayerSounds.Test], AudioData.SoundBus.Effects);
	// Change to AudioStreamPlayer for music/global sounds and AudioStreamPlayer3D if working in 3D
	public static Dictionary<T, AudioStream> LoadedSounds<T>(Dictionary<T, string> soundPathsDict)
	{
		Dictionary<T, AudioStream> loadedSoundsDict = new Dictionary<T, AudioStream>();
		foreach (T key in soundPathsDict.Keys)
		{
			loadedSoundsDict[key] = (AudioStream) GD.Load(soundPathsDict[key]);
		}
		return loadedSoundsDict;
	}


}
